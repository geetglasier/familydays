package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class UpdateChoresVM(private val mainRepository: MainRepository):ViewModel() {
    fun updatechores(token:String,chore_id:String,icon_name:String,title:String,is_daily_chore:String,is_complete:String,is_confirmation:String,set_point:String,chore_time:String,date_time:String)=
        liveData(Dispatchers.IO) {
            emit(Resource.loading(data = null))
            try {
                emit(Resource.success(data = mainRepository.updatechoreschild(token,chore_id,icon_name,title,is_daily_chore,is_complete,is_confirmation,set_point,chore_time,date_time)))
            } catch (exception: Exception) {
                exception.printStackTrace()
                emit(Resource.error(data = null, exception = exception))
            }



        }
}
package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

class AddChoresResp(
    @Json(name = "data")
    val `data`: LoginResp.Data?,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean) {
}
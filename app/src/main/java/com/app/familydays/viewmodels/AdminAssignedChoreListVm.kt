package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class AdminAssignedChoreListVm (
    private val mainRepository: MainRepository
) : ViewModel() {

    fun getAdminAssignedChoreList(token: String,load_more:String,date_time: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getAdminAssignedChoreList(token,load_more,date_time,)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }
    fun getChildAssignedChoreList(token: String,load_more:String,to_date: String,from_date: String,status:String,date_time: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getChild_assigned_finishedChoreList(token,load_more,to_date,from_date,status,date_time)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

}
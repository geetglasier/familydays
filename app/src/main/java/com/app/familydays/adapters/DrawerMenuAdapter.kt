package com.app.familydays.adapters

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.ItemDrawerMenuBinding
import com.app.familydays.fragments.ChatlistFragment
import com.app.familydays.fragments.ChatlistFragmentDirections
import com.app.familydays.fragments.EditProfileFragmentDirections
import com.app.familydays.fragments.NotificationListFragmentDirections
import com.app.familydays.global.GlobalClass

data class DrawerMenuItem(
    val image: Int,
    val name: String,
    val destination: Int? = null,
    val args: Bundle? = null,
    val count: String = ""
)


class DrawerMenuAdapter(
        val activity: Activity,
        navController: NavController,
        val onItemClicked: (Int?, Bundle?) -> Unit,
        val onLogoutClicked: () -> Unit
) :
    RecyclerView.Adapter<DrawerMenuAdapter.DrawerMenuViewHolder>() {

    val navCont=navController

    private val itemList = mutableListOf(
        DrawerMenuItem(R.drawable.ic_member, activity.getString(R.string.txt_family_member),R.id.action_global_nav_family_member),
        DrawerMenuItem(R.drawable.ic_chat, activity.getString(R.string.txt_chat)),
        DrawerMenuItem(R.drawable.ic_notification, activity.getString(R.string.txt_notification)),
        DrawerMenuItem(R.drawable.ic_contactus, activity.getString(R.string.txt_contact_help_desk), R.id.action_global_nav_contact_admin ),
        DrawerMenuItem(R.drawable.ic_edit_profile, activity.getString(R.string.txt_edit_profile)),
        DrawerMenuItem(R.drawable.ic_logout, activity.getString(R.string.txt_logout))
    )

    inner class DrawerMenuViewHolder(val binding: ItemDrawerMenuBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            val item = itemList[position]
            binding.item = item
            binding.root.setOnClickListener {

                if(position == 4){
                    navCont.navigate(
                            EditProfileFragmentDirections.actionGlobalNavEditProfile(
                                    GlobalClass.prefs.getValueString("token").toString()

                            )
                    )
                }else if(position==1){
                    navCont.navigate(
                        ChatlistFragmentDirections.actionGlobalNavChatlist(
                            GlobalClass.prefs.getValueString("token").toString()

                        ,"")
                    )
                }else if (position==2){
                    navCont.navigate(
                        NotificationListFragmentDirections.actionGlobalNavNotification()
                    )
                }

                if (position == itemList.lastIndex) onLogoutClicked() else onItemClicked(
                    item.destination, item.args
                )
            }

            if (item.count >"0") {
                binding.tvMsgCount.visibility = View.VISIBLE
            } else {
                binding.tvMsgCount.visibility = View.GONE
            }

            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DrawerMenuViewHolder(
            ItemDrawerMenuBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    override fun onBindViewHolder(holder: DrawerMenuViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount() = itemList.size

    fun setMessageCount(msgCount: Int, notificationCount: Int) {


     /*   if (notificationCount>99&&msgCount>99){

            val count= "99+"
            val msgcount= "99+"
            itemList[1] = itemList[1].copy(count = msgcount)
            itemList[2] = itemList[2].copy(count = count)



        }else{
            itemList[1] = itemList[1].copy(count = msgCount.toString())
            itemList[2] = itemList[2].copy(count = notificationCount.toString())

        }*/

        if(notificationCount>99){
            val count= "99+"
            itemList[2] = itemList[2].copy(count = count)
        }else{

            itemList[2] = itemList[2].copy(count = notificationCount.toString())

        }


        if(msgCount>99){
            val msgcount= "$99+"
            itemList[1] = itemList[1].copy(count = msgcount)
        }else{
            itemList[1] = itemList[1].copy(count = msgCount.toString())
        }


        notifyDataSetChanged()

    }

    fun setNotificationCount() {

    }

}



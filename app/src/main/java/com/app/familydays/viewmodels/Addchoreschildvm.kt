package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class Addchoreschildvm(private val mainRepository: MainRepository): ViewModel() {
    fun addchores(token:String,icon_name:String,title:String,chore_time:String,is_daily_chore:String,is_confirmation:String,cho_iscompleted:String,set_point:String)=
        liveData(Dispatchers.IO) {
            emit(Resource.loading(data = null))
            try {
                emit(Resource.success(data = mainRepository.addchoreschild(token,icon_name,title,chore_time,is_daily_chore,is_confirmation,cho_iscompleted,set_point)))
            } catch (exception: Exception) {
                exception.printStackTrace()
                emit(Resource.error(data = null, exception = exception))
            }



        }
}
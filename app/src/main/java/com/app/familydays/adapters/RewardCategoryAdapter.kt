package com.app.familydays.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.databinding.ItemPopularChoreBinding
import com.app.familydays.databinding.ItemRewardcategoryBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.PopularChoreResp
import com.app.familydays.webservice.responses.RewardCategoryResp
import java.lang.reflect.Array.set

class RewardCategoryAdapter(
    val context: Context,
    val onItemClicked: (RewardCategoryResp.Data) -> Unit
) :
    RecyclerView.Adapter<RewardCategoryAdapter.RewardlistViewHolder>() {
    var rewardcategoryRespList: List<RewardCategoryResp.Data>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class RewardlistViewHolder(val binding: ItemRewardcategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.reward = rewardcategoryRespList?.get(position)

            if (GlobalClass.selectepos == position){
                binding.ivSelected.visibility= View.VISIBLE
            }else{
                binding.ivSelected.visibility= View.INVISIBLE
            }


            binding.root.setOnClickListener {
                GlobalClass.selectepos=position
                onItemClicked(rewardcategoryRespList?.get(position)!!)
            }

            binding.executePendingBindings()

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        RewardlistViewHolder(
            ItemRewardcategoryBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )


    override fun onBindViewHolder(holder: RewardlistViewHolder, position: Int) {
        holder.bind(position)

    }

    override fun getItemCount() = rewardcategoryRespList?.size ?: 0

}
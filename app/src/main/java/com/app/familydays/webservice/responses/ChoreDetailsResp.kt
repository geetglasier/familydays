package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

class ChoreDetailsResp(
    @Json(name ="status")
    val status : Boolean,
    @Json(name ="error")
    val error : Int,
    @Json(name ="message")
    val message : String,
    @Json(name ="data")
    val data : Data) {

    data class Data (

        @Json(name="chore_id") val chore_id : Int,
        @Json(name="title") val title : String,
        @Json(name="set_time") val set_time : String,
        @Json(name="is_daily") val is_daily : Int,
        @Json(name="is_complete") val is_complete : Int,
        @Json(name="is_confirmation") val is_confirmation : Int,
        @Json(name="point") val point : Int,
        @Json(name="minimum_point") val minimum_point : Int,
        @Json(name="maximum_point") val maximum_point : Int,
        @Json(name="icon_name") val icon_name : String,
        @Json(name="icon") val icon : String
    )
}
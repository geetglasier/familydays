package com.app.familydays.fragments

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.app.familydays.R
import com.app.familydays.activities.DashboardActivity
import com.app.familydays.adapters.FamilyMemberListAdapter
import com.app.familydays.adapters.MessageMemberListAdapter
import com.app.familydays.databinding.FragmentChatlistBinding
import com.app.familydays.databinding.FragmentFamilyMemberListBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.viewmodels.FamilyMemberVm
import com.app.familydays.viewmodels.MessagemeberListVM
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.android.viewmodel.ext.android.viewModel


class ChatlistFragment : Fragment() {

    private lateinit var binding: FragmentChatlistBinding
    private val viewModel: MessagemeberListVM by viewModel()
    private lateinit var loadingIndicator: KProgressHUD
    private val args: ChatlistFragmentArgs by navArgs()



    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_chatlist,
                container,
                false
            )

     if(  DashboardFragment.instance.loadingIndicator.isShowing) {
         DashboardFragment.instance.loadingIndicator.dismiss()
     }

        loadingIndicator = getLoadingIndicator(requireContext())


       return binding.root

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadingIndicator.dismiss()

        getFamilyMemberList()

    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun getFamilyMemberList() {
        if (isNetworkConnected(requireContext())) {

            viewModel.getFamilyMemberlistmsg(
                GlobalClass.prefs.getValueString("token").toString(),
           "")
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {


                                    Log.e("username", response.user_info.username)
                                    Log.e("profilepic", response.user_info.profile_url)
                                    /* findNavController().navigate(
                                         ChildProfileFragmentDirections.actionGlobalNavChildProfile(
                                             it.token, it.profileUrl, it.role.toString()
                                         )
                                     )*/
                                    binding.tvFullName.text=response.user_info.username
                                    Glide.with(requireContext())
                                        .load(response.user_info.profile_url)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .into(binding.imgProfile)


                                     //if(response.data)


                                    binding.rvFamilyMember.adapter =
                                            MessageMemberListAdapter(
                                                onItemClicked = {
                                                    findNavController().navigate(ChatFragmentDirections.actionGlobalNavChat(GlobalClass.prefs.getValueString("token").toString(),it.user_id.toString(),it.token.toString(),""))

                                                }
                                            ).apply {
                                                familyMemberList = response.data
                                            }



                                    }

                                }


                            }


                        Status.ERROR -> {
                            resource.exception?.printStackTrace()
                            loadingIndicator.dismiss()
                        }

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }


}
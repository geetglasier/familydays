package com.app.familydays.fragments

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.OnTouchListener
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.app.familydays.R
import com.app.familydays.activities.DashboardActivity
import com.app.familydays.adapters.GetMessagesAdapter
import com.app.familydays.adapters.MessageMemberListAdapter
import com.app.familydays.databinding.FragmentChatBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.viewmodels.EditProfileVm
import com.app.familydays.viewmodels.MessagemeberListVM
import com.app.familydays.viewmodels.SendMessageVM
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_chat.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*


class ChatFragment : Fragment() {

    lateinit var binding: FragmentChatBinding
    private lateinit var loadingIndicator: KProgressHUD

    private val args: ChatFragmentArgs by navArgs()
    private val viewModel: SendMessageVM by viewModel()
    private val viewMessagelist: MessagemeberListVM by viewModel()
    private val viewModelgetuser: EditProfileVm by viewModel()
    private val viewModellist: MessagemeberListVM by viewModel()
    var points :String="0"





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false)
        //loadingIndicator = getLoadingIndicator(requireContext())

        loadingIndicator = getLoadingIndicator(requireContext())
        getFamilyMemberList()
        setHasOptionsMenu(true)
        binding.etMessage.setOnTouchListener(OnTouchListener { v, event ->
            val DRAWABLE_LEFT = 0
            val DRAWABLE_TOP = 1
            val DRAWABLE_RIGHT = 2
            val DRAWABLE_BOTTOM = 3
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= binding.etMessage.getRight() - binding.etMessage.getCompoundDrawables()
                        .get(DRAWABLE_RIGHT).getBounds().width()
                ) {
                    // your action here
                        sendmessage()
                  //  Toast.makeText(requireContext(), "Click on right", Toast.LENGTH_SHORT).show()
                    return@OnTouchListener true
                }
            }
            false
        })

       //
        getmessages()
        getuser()




        return binding.root
    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun getmessages() {
        if (isNetworkConnected(requireContext())) {

            viewMessagelist.getMessages(
                GlobalClass.prefs.getValueString("token").toString(),args.userId
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            resource.data?.let { response ->
                                if (response.status) {
                                  //  loadingIndicator.dismiss()

                                    //if(response.data)


                                    binding.rvMessages.adapter =
                                        GetMessagesAdapter(requireContext()

                                        ).apply {
                                            getmessageList = response.data
                                        }
                                    binding.rvMessages.scrollToPosition(response.data.size-1)

                                  //  loadingIndicator.dismiss()
                                }

                            }


                        }


                        Status.ERROR -> {
                            resource.exception?.printStackTrace()
                           // loadingIndicator.dismiss()
                        }

                        Status.LOADING ->{

                        }

                    //loadingIndicator.show()
                    }
                }

        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun getFamilyMemberList() {
        if (isNetworkConnected(requireContext())) {

            viewModellist.getFamilyMemberlistmsg(
                GlobalClass.prefs.getValueString("token").toString(),
                args.notificationId)
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            resource.data?.let { response ->
                                if (response.status) {
                                    //loadingIndicator.dismiss()


                                    Log.e("username", response.user_info.username)
                                    Log.e("profilepic", response.user_info.profile_url)

                                /* findNavController().navigate(
                                         ChildProfileFragmentDirections.actionGlobalNavChildProfile(
                                             it.token, it.profileUrl, it.role.toString()
                                         )
                                     )*/
                                // loadingIndicator.dismiss()
                                }

                            }


                        }


                        Status.ERROR -> {
                            resource.exception?.printStackTrace()
                           // loadingIndicator.dismiss()
                        }

                        Status.LOADING -> {
                           // loadingIndicator.show()
                        }
                    }
                }

        }
    }


    override fun onPrepareOptionsMenu(menu: Menu) {

        super.onPrepareOptionsMenu(menu)
    }
    @RequiresApi(Build.VERSION_CODES.M)
    fun sendmessage() {
        if (isNetworkConnected(requireContext())) {



            val today = Date()
//            val format = SimpleDateFormat("dd-MM-yyyy hh:mm:ss ")
//            var dateToStr = format.format(today)


            val format = SimpleDateFormat("dd-MM-yyyy hh:mm:ss")
            val dateToStr = format.format(today)
            Log.d("xxx",dateToStr)


            viewModel.sendmessage(
                GlobalClass.prefs.getValueString("token").toString(),
                args.userId,
                et_message.text.toString(),
                dateToStr
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            resource.data?.let { response ->
                                if (response.status) {
                                  //  loadingIndicator.dismiss()


                                    binding.etMessage.text.clear()
                                    binding.etMessage.clearFocus()

                                    getmessages()

                                    binding.rvMessages.adapter =
                                        MessageMemberListAdapter(
                                            onItemClicked = {
                                                findNavController().navigate(ChatFragmentDirections.actionGlobalNavChat(
                                                    GlobalClass.prefs.getValueString("token").toString(),it.user_id.toString(),"",""))

                                            }
                                        ).apply {
                                           // familyMemberList = response.data
                                        }


                                    //loadingIndicator.dismiss()
                                }

                            }


                        }


                        Status.ERROR -> {
                            resource.exception?.printStackTrace()
                          //  loadingIndicator.dismiss()
                        }

                        Status.LOADING ->
                            Log.d("xxx","loading")
                    //loadingIndicator.show()
                    }
                }

        }
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    private fun getuser() {

        if (isNetworkConnected(requireContext())) {
            viewModelgetuser.getUserDetail(
               args.childtoken
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    if (response.error == 200) {


                                        points=response.data.totalPoint.toString()

                                        (context as DashboardActivity).tvpointsab.text= "$points Pts"

                                    }

                                }

                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }

    }

}
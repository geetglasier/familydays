package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

class GetMessagesResp(
    @Json(name = "data")
    val `data`: List<Data>,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean,
    @Json(name = "user_role")
    val user_role: String,
    @Json(name = "total_point")
    val total_point: String) {



    data class Data(
        @Json(name = "message_id")
        val message_id: Int,
        @Json(name = "message")
        val message: String,
        @Json(name = "username")
        val username: String,
        @Json(name = "fullname")
        val fullname:String,
        @Json(name = "token")
        val token: String,
        @Json(name = "date_time")
        val date_time: String,
        @Json(name = "profileurl")
        val profileurl: String)

}
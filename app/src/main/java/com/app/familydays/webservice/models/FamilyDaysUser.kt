package com.app.familydays.webservice.models

import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

data class FamilyDaysUser(
    @Json(name = "birth_date")
    var birthDate: String?="",
    @Json(name = "email")
    var email: String?="",
    @Json(name = "family_name")
    var familyName: String?="",
    @Json(name = "full_name")
    var fullName: String?="",
    @Json(name = "is_admin")
    var isAdmin: Int?=1,
    @Json(name = "phone_no")
    var phoneNo: String?="",
    @Json(name = "profile_url")
    var profileUrl: String?="",
    @Json(name = "role")
    var role: Int?=0,
    @Json(name = "rolename")
    var rolename: String?="",
    @Json(name = "token")
    var token: String?="",
    @Json(name = "total_point")
    var totalPoint: Int?=0,
    @Json(name = "user_id")
    var userId: Int?=0,
    @Json(name = "username")
    var username: String?=""
) {
    fun toMap() =
        ((Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            .adapter(FamilyDaysUser::class.java)
            .toJsonValue(this) as Map<String, String>))

}
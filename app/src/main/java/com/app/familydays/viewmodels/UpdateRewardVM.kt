package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class UpdateRewardVM(private val mainRepository: MainRepository): ViewModel() {
    fun updatereward(token:String,rewards_name:String,reward_id:Int,category_id:Int,category:String,category_icon:String,brand_name:String,brand_icon:String,frame_date:String,point:Int)=
        liveData(Dispatchers.IO) {
            emit(Resource.loading(data = null))
            try {
                emit(Resource.success(data = mainRepository.updatereward(token,rewards_name,reward_id,category_id,category,category_icon,brand_name,brand_icon,frame_date,point)))
            } catch (exception: Exception) {
                exception.printStackTrace()
                emit(Resource.error(data = null, exception = exception))
            }



        }
}
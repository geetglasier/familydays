package com.app.familydays.fragments

import android.app.AlertDialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import com.app.familydays.R
import com.app.familydays.adapters.AssignedChoreAdapter
import com.app.familydays.adapters.FinishedChoreAdapter
import com.app.familydays.databinding.FragmentDashboardBinding
import com.app.familydays.global.AppConstant
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.utils.unAuthorisedUserDialog
import com.app.familydays.viewmodels.AdminAssignedChoreListVm
import com.app.familydays.viewmodels.AdminFinishedChoreListVm
import com.app.familydays.viewmodels.ChoreisCompleteVM
import com.app.familydays.viewmodels.DeleteChoreVM
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import android.content.DialogInterface
import com.app.familydays.activities.DashboardActivity


class DashboardFragment : Fragment() {

    private lateinit var binding: FragmentDashboardBinding
    private val viewModel: AdminFinishedChoreListVm by viewModel()
    private val viewModel1: AdminAssignedChoreListVm by viewModel()
    private val viewModel2: ChoreisCompleteVM by viewModel()
    private val viewModeldelete:DeleteChoreVM by viewModel()
     lateinit var loadingIndicator: KProgressHUD
    lateinit var chores_iscompleted: String

    companion object {
        lateinit var instance: DashboardFragment
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false)
        instance=this


        loadingIndicator = getLoadingIndicator(requireContext())

        binding.llClaims.setOnClickListener(){
            if (!isNetworkConnected(requireContext())){
                Toast.makeText(context, R.string.txt_no_internet, Toast.LENGTH_SHORT).show()
            }else {


                findNavController().navigate(
                    ClaimListFragmentDirections.actionGlobalClaim(
                        "",
                        "",
                        ""
                    )
                )

            }
        }

        binding.llNewChore.setOnClickListener {

            if (!isNetworkConnected(requireContext())){
                Toast.makeText(context, R.string.txt_no_internet, Toast.LENGTH_SHORT).show()
            }else{

            GlobalClass.selectepos=-1

            if (GlobalClass.prefs.getValueString("role")
                    .equals("Father") || GlobalClass.prefs.getValueString("role").equals("Mother")
            ) {
                findNavController().navigate(
                        AddNewChoreFragmentDirections.actionGlobalDialogNewChore(
                                "",
                                ""
                        )
                )
            } else {
                findNavController().navigate(
                        AddchoreschildFragmentDirections.actionGlobalDialogNewChorechild(
                                "",
                                ""
                        )
                )
            }

            setHasOptionsMenu(true)
//            val dialogFrg = AddNewChoreFragment().newInstance("")!!
//            dialogFrg.show(parentFragmentManager,"")

        }
        }
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        getAssignedChoreList()
        getFinishedChoreList()

        setFragmentResultListener("refresh") { _, _ ->

            getAssignedChoreList()
            getFinishedChoreList()

        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {

        menu.findItem(R.id.item_add_family).isVisible = false
        menu.findItem(R.id.item_add_reward).isVisible=false

        super.onPrepareOptionsMenu(menu)
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun getFinishedChoreList() {
        val loadmore="0"

        if (isNetworkConnected(requireContext())) {
            viewModel.getAdminFinishedChoreList(
                    GlobalClass.prefs.getValueString("token").toString(),loadmore
            )
                .observe(
                        viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {


                                    click()

                                    if(response.data.size == 6){
                                        binding.tvFinSeemore.visibility=View.VISIBLE
                                    }else{
                                        binding.tvFinSeemore.visibility=View.GONE

                                    }

                                    if(GlobalClass.prefs.getValueString("role_id") == "4" ||
                                            GlobalClass.prefs.getValueString("role_id") == "5"){
                                        binding.tvTotal.visibility = View.GONE
                                    }else{
                                        if (response.data.isNotEmpty()) {

                                            if(response.choreIncompleteCount > 0){
                                                binding.tvTotal.visibility = View.VISIBLE
                                                if ( response.choreIncompleteCount>99){
                                                    binding.tvTotal.text ="99+"
                                                }else{
                                                    binding.tvTotal.text = response.choreIncompleteCount.toString()
                                                }

                                            }else{
                                                binding.tvTotal.visibility = View.GONE
                                                binding.tvFinSeemore.visibility=View.GONE
                                            }

                                        } else {
                                            binding.tvTotal.visibility = View.GONE

                                        }
                                    }

                                    binding.rvFinishedChores.adapter =
                                            FinishedChoreAdapter("dashboard",
                                                    requireContext(),
                                                    onItemClicked = {},onLongclicked = {

                                                }).apply {
                                                choreRespList = response.data
                                            }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                                .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getAssignedChoreList() {
//        "date_time": 2021-08-11 18:54:53

        val today = Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date",dateToStr)

        if (isNetworkConnected(requireContext())) {
            viewModel1.getAdminAssignedChoreList(
                    GlobalClass.prefs.getValueString("token").toString(),"0",dateToStr
            )
                .observe(
                        viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {


                                    click()

                                    if(response.data.size == 6){
                                        binding.tvAssSeemore.visibility=View.VISIBLE
                                    }else{
                                        binding.tvAssSeemore.visibility=View.GONE

                                    }
                                    if(GlobalClass.prefs.getValueString("role_id") == "4" ||
                                            GlobalClass.prefs.getValueString("role_id") == "5"){
                                        binding.tvAssTotal.visibility = View.GONE
                                    }else{
                                        if (response.data.isNotEmpty()) {
                                            if(response.disapprovedCount > 0){
                                                binding.tvAssTotal.visibility = View.VISIBLE
                                                if (response.disapprovedCount>99){
                                                    binding.tvAssTotal.text ="99+"
                                                }else{
                                                    binding.tvAssTotal.text = response.disapprovedCount.toString()
                                                }

                                            }else{
                                                binding.tvAssTotal.visibility = View.GONE

                                            }

                                        } else {
                                            binding.tvAssTotal.visibility = View.GONE

                                        }
                                    }


                                    binding.rvAssignChores.adapter =
                                            AssignedChoreAdapter(
                                                    requireContext(),
                                                    onItemClicked = {


                                                        findNavController().navigate(
                                                                UpdateChoresFragmentDirections.actionGlobalDialogUpdateChore(
                                                                        it.choreId.toString(),
                                                                        it.iconUrl,
                                                                        it.token,"",""
                                                                ,"","")
                                                        )


                                                    }, onDeleteItemClicked = {
                                                //call delete chore api id= it.choreId
                                                val dialog = AlertDialog.Builder(context)
                                                        .setTitle("Delete Chore")
                                                        .setTitle("Are you sure you want to delete this chore?")
                                                        .setPositiveButton(
                                                                "Ok"
                                                        ) { dialog, whichButton ->

                                                            viewModeldelete.deletechore(it.choreId.toString()).observe(viewLifecycleOwner) { resource ->
                                                                when (resource.status) {
                                                                    Status.SUCCESS -> {
                                                                        loadingIndicator.dismiss()
                                                                        resource.data.let { response ->
                                                                            if (response != null) {
                                                                                if (response.status) {
                                                                                    Toast.makeText(
                                                                                            requireContext(),
                                                                                            response.message,
                                                                                            Toast.LENGTH_SHORT
                                                                                    ).show()

                                                                                    setFragmentResult(
                                                                                            "refresh",
                                                                                            bundleOf("fromdeleteChore" to true)
                                                                                    )
                                                                                    findNavController().navigateUp()
                                                                                }
                                                                            }

                                                                        }

                                                                    }
                                                                    Status.LOADING -> {
                                                                        loadingIndicator.show()
                                                                    }
                                                                    Status.ERROR -> {
                                                                        loadingIndicator.dismiss()
                                                                    }
                                                                }

                                                            }
                                                        }
                                                        .setNegativeButton("Cancel") { dialog, whichButton ->
                                                            dialog.dismiss()
                                                        }
                                                        .show()


                                            },onlongItemClicked = {}).apply {
                                                choreRespList = response.data
                                            }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                                .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun iscompletedApiCall(choreiscompleted: String, choreid: String) {

        val today = Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date",dateToStr)

       viewModel2.chore_iscomplete(
                GlobalClass.prefs.getValueString("token").toString(),
                choreid,
                choreiscompleted,
                 dateToStr

        )
            .observe(viewLifecycleOwner) { resources ->
                when (resources.status) {
                    Status.SUCCESS -> {
                        loadingIndicator.dismiss()
                        resources.data.let { response ->

                            if (response != null) {
                                if (response.status) {

                            if (choreiscompleted=="1"){
                                AlertDialog.Builder(context)

                                    .setMessage("Chore Marked Incomplete") // Specifying a listener allows you to take an action before dismissing the dialog.
                                    // The dialog is automatically dismissed when a dialog button is clicked.
                                    .setCancelable(false)
                                    .setPositiveButton(
                                        android.R.string.yes
                                    ) { dialog, which ->
                                        findNavController().navigate( UpdateChoresFragmentDirections.actionGlobalDialogUpdateChore(choreid,"","","","",choreid,""))
                                    }
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show()

                            }else{
                                Toast.makeText(requireContext(), response.message, Toast.LENGTH_SHORT).show()
                                setFragmentResult(
                                    "refresh",
                                    bundleOf("fromchorecomplete" to true)
                                )
                            }

//                                    getFinishedChoreList()
                                }
                            }
                        }

                    }
                    Status.ERROR -> {
                        loadingIndicator.dismiss()

                    }
                    Status.LOADING -> {
                        loadingIndicator.show()
                    }
                }


            }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun click(){
        if (!isNetworkConnected(requireContext())){
            Toast.makeText(context, R.string.txt_no_internet, Toast.LENGTH_SHORT).show()
        }else {

        binding.llRewards.setOnClickListener(){



                findNavController().navigate(RewardFragmentDirections.actionGlobalReward("", ""))

        }
        binding.tvAssSeemore.setOnClickListener(){
            var from=""
            if (GlobalClass.prefs.getValueString("isadmin")=="0"){
                from="assignchildren"
            }
            else{
                from="Assigned"
            }
            GlobalClass.setTitle="Assigned Chores"

            findNavController().navigate(MoreChoresFragmentDirections.actionGlobalNavMorechores(from,"",""))
        }


        binding.tvFinSeemore.setOnClickListener(){
            GlobalClass.setTitle="Finished Chores"

            findNavController().navigate(MoreChoresFragmentDirections.actionGlobalNavMorechores("Finished","",""))

        }


    }
    }


}





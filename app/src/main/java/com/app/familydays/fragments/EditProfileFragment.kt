package com.app.familydays.fragments

import android.app.DatePickerDialog
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getColor
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import br.com.ilhasoft.support.validation.Validator
import coil.load
import coil.transform.CircleCropTransformation
import com.app.familydays.R
import com.app.familydays.activities.DashboardActivity
import com.app.familydays.databinding.FragmentEditProfileBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.utils.showMediaPickerDialog
import com.app.familydays.viewmodels.EditProfileVm
import com.app.familydays.viewmodels.FamilyMemberVm
import com.app.familydays.viewmodels.ForgotPassVm
import com.app.familydays.webservice.responses.FamilyMemberListResp
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaopiz.kprogresshud.KProgressHUD
import com.stfalcon.imageviewer.StfalconImageViewer
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.koin.android.ext.android.bind
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.sql.Date
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class EditProfileFragment : Fragment() {

    private lateinit var binding: FragmentEditProfileBinding
    private val viewModel: EditProfileVm by viewModel()
    private lateinit var loadingIndicator: KProgressHUD
    private lateinit var validator: Validator
    private var imageUri: Uri? = null
    val myCalendar = Calendar.getInstance()
    private val args: EditProfileFragmentArgs by navArgs()

    var role_id = 0
    var role = ""
    var getRole = ""
    var roleList = listOf("")
    var is_admin = 0
    var bday = ""
    private val viewModelMember: FamilyMemberVm by viewModel()

    var familyMemberList: ArrayList<FamilyMemberListResp.Data>? = null


    @RequiresApi(Build.VERSION_CODES.M)
    private val takePhoto =
        registerForActivityResult(ActivityResultContracts.TakePicture())
        {
            if (it) {
                updateProfilePic()
            }

        }

    @RequiresApi(Build.VERSION_CODES.M)
    private val pickImage = registerForActivityResult(ActivityResultContracts.GetContent()) {
        it?.let { uri ->
            imageUri = uri
            uri?.let {
                binding.imgProfile.load(it) {
                    crossfade(true)
                    placeholder(R.drawable.ic_placeholder)
                    error(R.drawable.ic_placeholder)
                    transformations(CircleCropTransformation())
                }
            }

            updateProfilePic()
        }
    }

    private lateinit var validateUserDetailViewList: List<EditText>


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_edit_profile, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        validator = Validator(binding)
        loadingIndicator = getLoadingIndicator(requireContext())

        setHasOptionsMenu(true)

        familyMemberList = ArrayList()
        validateUserDetailViewList = mutableListOf(

            binding.edtEmail,
            binding.edtPhNo,
            binding.editName,
            binding.edtUsername,
            binding.edtBirthdate
        )


        binding.ivUpload.setOnClickListener {

            imagepickerdialog()


        }
        binding.imgProfile.setOnClickListener {
            imagepickerdialog()
        }

        binding.llAdmin.setOnClickListener {
            if (binding.ivUnChecked.isVisible) {
                is_admin = 1
                binding.ivChecked.visibility = View.VISIBLE
                binding.ivUnChecked.visibility = View.GONE

                binding.llAdmin.isEnabled = false
            }

        }


        binding.txtRole.setOnItemClickListener { adapterView, view, i, l ->

            Log.e("text", binding.txtRole.text.toString())
            for (i in 0 until familyMemberList!!.size) {

                if (familyMemberList!![i].role == 2) {

                    if (familyMemberList!![i].roleType == binding.txtRole.text.toString()) {
                        Toast.makeText(
                            context,
                            getString(R.string.txt_role_validation_check),
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.txtRole.setText(getRole)

                        break
                    }

                } else if (familyMemberList!![i].role == 3) {
                    if (familyMemberList!![i].roleType == binding.txtRole.text.toString()) {
                        Toast.makeText(
                            context,
                            getString(R.string.txt_role_validation_check),
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.txtRole.setText(getRole)

                        break
                    }

                }
            }


        }


        binding.edtBirthdate.setOnClickListener {
            val calendar = Calendar.getInstance()

            val date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                onDateSet(view, year, monthOfYear, dayOfMonth)
            }

            val dateDialog = DatePickerDialog(
                requireContext(), R.style.datepickerstyle, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            )
            dateDialog.datePicker.maxDate = calendar.timeInMillis
//            dateDialog.datePicker.minDate = System.currentTimeMillis() - 1000

            dateDialog.show()
            dateDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE)
                .setTextColor(resources.getColor(R.color.colorPrimary))
            dateDialog.getButton(DatePickerDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(R.color.colorPrimary))
        }
        binding.tvOk.setOnClickListener {
            if (binding.txtRole.text.toString() == "Father") {
                role = "2"
//                GlobalClass.prefs.SaveString("roletypeF","Father")

            } else if (binding.txtRole.text.toString() == "Mother") {
                role = "3"
//                GlobalClass.prefs.SaveString("roletypeM","Mother")

            }
            updateUserDetail()
        }

        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (validator.validate() && isNetworkConnected(requireContext())) {
            viewModel.getUserDetail(

                args.token
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    if (response.data.isAdmin == 1) {
                                        is_admin = 1
                                        binding.llAdmin.visibility = View.VISIBLE
                                        binding.ivChecked.visibility = View.VISIBLE
                                        binding.ivUnChecked.visibility = View.GONE
//                                            binding.llAdmin.isEnabled=false
                                    } else {
                                        is_admin = 0
                                        binding.llAdmin.visibility = View.VISIBLE
                                        binding.ivChecked.visibility = View.GONE
                                        binding.ivUnChecked.visibility = View.VISIBLE
//                                            binding.llAdmin.isEnabled=true
                                    }

                                    if (GlobalClass.prefs.getValueString("role_id").equals("4")||GlobalClass.prefs.getValueString("role_id")=="5") {
                                        disablecontrol()
                                    } else if (GlobalClass.prefs.getValueString("role_id").equals("2")&&GlobalClass.prefs.getValueString("isadmin")=="0"||
                                        GlobalClass.prefs.getValueString("role_id")=="3"&&GlobalClass.prefs.getValueString("isadmin")=="0"
                                        ) {
                                        binding.txtmsg.text="Note:-Only Admin can edit the profile."
                                        disablecontrol()

                                    }else{
                                        binding.editFamilyName.isEnabled = false
                                        binding.edtUsername.isEnabled = false

                                        binding.llAdmin.visibility = View.VISIBLE


                                    }

                                    binding.edtEmail.setText(response.data.email)
                                    binding.edtPhNo.setText(response.data.phoneNo)


                                    Glide.with(this)
                                        .load(response.data.profileUrl)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .into(binding.imgProfile)

                                    viewModel.setUserDetail(response.data)
                                    binding.edtBirthdate.setText(displayTime(response.data.birthDate))

                                    if (response.data.role.toString() == "2") {
                                        viewModel.userLive.value?.rolename = "Father"
                                        roleList = listOf("Father", "Mother")
                                        role_id = response.data.role!!
                                        GlobalClass.prefs.SaveString("roletype", "Father")
                                        getRole = "Father"


                                    } else if (response.data.role.toString() == "3") {
                                        viewModel.userLive.value?.rolename = "Mother"
                                        roleList = listOf("Father", "Mother")
                                        role_id = response.data.role!!
                                        GlobalClass.prefs.SaveString("roletype", "Mother")
                                        getRole = "Mother"


                                    } else if (response.data.role.toString() == "4") {
                                        viewModel.userLive.value?.rolename = "Son"
                                        roleList = listOf("Son", "Daughter")
                                        binding.imgProfile.isEnabled = false
                                        binding.tilFamilyName.visibility = View.GONE
                                        role_id = response.data.role!!

                                    } else if (response.data.role.toString() == "5") {
                                        viewModel.userLive.value?.rolename = "Daughter"
                                        roleList = listOf("Son", "Daughter")
                                        binding.tilFamilyName.visibility = View.GONE
                                        binding.imgProfile.isEnabled = false
                                        role_id = response.data.role!!

                                    }

                                    binding.txtRole.setAdapter(
                                        ArrayAdapter(
                                            requireContext(),
                                            android.R.layout.simple_spinner_dropdown_item,
                                            roleList
                                        )
                                    )


                                } else {
                                    Toast.makeText(
                                        context,
                                        response.message,
                                        Toast.LENGTH_LONG
                                    ).show()
                                }

                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
        getFamilyMemberList()

    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun imagepickerdialog() {
        context?.let { c ->
            showMediaPickerDialog(c, {
                imageUri = FileProvider.getUriForFile(
                    c.applicationContext,
                    com.app.familydays.BuildConfig.APPLICATION_ID + ".provider",
                    File.createTempFile("image", ".jpg", c.filesDir)
                )
                takePhoto.launch(imageUri)
            }, {
                pickImage.launch("image/*")
            })
        }
    }

    fun disablecontrol() {
        binding.tvOk.visibility = View.INVISIBLE
        binding.imgProfile.isEnabled = false
        binding.ivUpload.visibility = View.INVISIBLE
        binding.editFamilyName.isEnabled = false
        binding.edtPhNo.isEnabled = false
        binding.editName.isEnabled = false
        binding.txtRole.isEnabled = false

        binding.edtEmail.isEnabled = false
        binding.edtPassword.isEnabled = false
        binding.edtUsername.isEnabled = false
        binding.edtBirthdate.isEnabled = false
        binding.llAdmin.visibility = View.INVISIBLE
        binding.txtmsg.visibility=View.VISIBLE

    }

    override fun onPrepareOptionsMenu(menu: Menu) {

//        menu.findItem(R.id.item_profile).isVisible = false
        menu.findItem(R.id.item_add_family).isVisible = false
        menu.findItem(R.id.item_child_profile).isVisible = false
        menu.findItem(R.id.item_add_reward).isVisible=false

        super.onPrepareOptionsMenu(menu)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun updateProfilePic() {


        if (isNetworkConnected(requireContext())) {

            val filePart: MultipartBody.Part? = imageUri?.let {
                activity?.application?.contentResolver?.openInputStream((it))?.use { inputStream ->
                    MultipartBody.Part.createFormData(
                        "profile_file",
                        "image.jpg",
                        inputStream.readBytes().toRequestBody("image/jpeg".toMediaType())
                    )
                }
            }

            viewModel.submitProfilePic(
                GlobalClass.prefs.getValueString("token").toString(), filePart
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            resource.data?.let { response ->
                                if (response.status) {
//                                    viewModel.setUserDetail(response.data)
//                                    GlobalClass.prefs.SaveString(
//                                        "profilePic",
//                                        response.data.profileUrl.toString()
//                                    )

                                    GlobalClass.prefs.SaveString(
                                        "profilePic",
                                        response.data.profileUrl
                                    )

                                    (requireContext() as DashboardActivity).loadprofilepic()

                                    loadingIndicator.dismiss()
                                    setFragmentResult(
                                        "refresh",
                                        bundleOf("fromEditProfile" to true)
                                    )
                                    findNavController().navigateUp()


                                }
                                Toast.makeText(
                                    context,
                                    response.message,
                                    Toast.LENGTH_LONG
                                ).show()

                            }

                        }
                        Status.ERROR -> {
                            resource.exception?.printStackTrace()
                            loadingIndicator.dismiss()
                        }

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }


    fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
        val userAge: Calendar = GregorianCalendar(year, month, day)
        val minAdultAge: Calendar = GregorianCalendar()
        minAdultAge.add(Calendar.YEAR, -18)
        if (role_id == 2 || role_id == 3) {
            if (minAdultAge.before(userAge)) {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.txt_birthday_validation),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val myFormat = "MMM-dd-yyyy"
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                binding.edtBirthdate.setText(sdf.format(myCalendar.time))
            }
//            Toast.makeText(context, getString(R.string.txt_birthday_validation), Toast.LENGTH_SHORT).show()
        } else {
            val myFormat = "MMM-dd-yyyy"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            binding.edtBirthdate.setText(sdf.format(myCalendar.time))

        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun updateUserDetail() {
        if (binding.txtRole.text.toString() == "Father") {
            viewModel.userLive.value?.role = 2
            role = "2"
        } else if (binding.txtRole.text.toString() == "Mother") {
            viewModel.userLive.value?.role = 3
            role = "3"
        }

        if (binding.edtBirthdate.text.toString() != "") {
            bday = finalDateTime(binding.edtBirthdate.text.toString()).toString()

        } else {
            bday = ""
        }

        if (validator.validate(validateUserDetailViewList) && isNetworkConnected(requireContext())) {
            viewModel.updateUserDetail(
                args.token,
                binding.editName.text.toString(),
                binding.edtEmail.text.toString(),
                binding.edtPhNo.text.toString(),
                role, bday!!,
                binding.edtPassword.text.toString(),
                is_admin.toString()
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {
                                    Toast.makeText(
                                        requireContext(),
                                        "Profile updated successfully!",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    setFragmentResult(
                                        "refresh",
                                        bundleOf("fromEditProfile" to true)
                                    )
                                    findNavController().navigateUp()
                                } else {
                                    Toast.makeText(
                                        context,
                                        response.message,
                                        Toast.LENGTH_LONG
                                    ).show()
                                }


                            }
                        }
                        Status.ERROR -> {
                            resource.exception?.printStackTrace()
                            loadingIndicator.dismiss()
                        }

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun getFamilyMemberList() {
        if (isNetworkConnected(requireContext())) {

            viewModelMember.getFamilyMember(
                GlobalClass.prefs.getValueString("token").toString()
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            resource.data?.let { response ->
                                if (response.status) {
                                    loadingIndicator.dismiss()

                                    familyMemberList?.clear()
                                    familyMemberList?.addAll(response.data)

                                }

                            }


                        }
                        Status.ERROR -> loadingIndicator.dismiss()


                        Status.LOADING -> loadingIndicator.show()
                    }

                }

        }
    }

    fun displayTime(inputDateTime: String?): String? {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "MMM-dd-yyyy"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: java.util.Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }


    fun finalDateTime(inputDateTime: String?): String? {
        val inputPattern = "MMM-dd-yyyy"
        val outputPattern = "yyyy-MM-dd"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: java.util.Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

}
package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

class ClaimListResp(
        @Json(name = "data")
        val `data`: List<Data>,
        @Json(name = "error")
        val error: Int,
        @Json(name = "message")
        val message: String,
        @Json(name = "status")
        val status: Boolean
) {
    data class Data(

        @Json(name = "reward_id")
        val rewardId: Int,
        @Json(name = "category")
        val category: String,
        @Json(name = "category_icon")
        val category_icon: String,
        @Json(name = "point")
        val point: Int,
        @Json(name = "create_by")
        val create_by: String,
        @Json(name = "is_createby")
        val is_createby: String,
        @Json(name = "create_date")
        val create_date: String
        ,
        @Json(name = "brand_url")
        val brand_url: String=""
    )
}
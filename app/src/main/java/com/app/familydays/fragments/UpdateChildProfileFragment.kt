package com.app.familydays.fragments

import android.app.DatePickerDialog
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import br.com.ilhasoft.support.validation.Validator
import coil.load
import coil.transform.CircleCropTransformation
import com.app.familydays.R
import com.app.familydays.activities.DashboardActivity
import com.app.familydays.databinding.FragmentUpdateChildProfileBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.utils.showMediaPickerDialog
import com.app.familydays.viewmodels.EditProfileVm
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class UpdateChildProfileFragment : Fragment() {

    private lateinit var binding: FragmentUpdateChildProfileBinding
    private val viewModel: EditProfileVm by viewModel()
    private lateinit var loadingIndicator: KProgressHUD
    private lateinit var validator: Validator
    private var imageUri: Uri? = null
    private val args: UpdateChildProfileFragmentArgs by navArgs()
    var role_id = 0
    var role = ""
    var child_id = 0
    var is_admin = 0
    var roleList = listOf("")
    val myCalendar = Calendar.getInstance()

    @RequiresApi(Build.VERSION_CODES.M)
    private val takePhoto =
        registerForActivityResult(ActivityResultContracts.TakePicture())
        {
            if (it) {
                updateProfilePic()
            }

        }

    @RequiresApi(Build.VERSION_CODES.M)
    private val pickImage = registerForActivityResult(ActivityResultContracts.GetContent()) {
        it?.let { uri ->
            imageUri = uri
            uri?.let {
                binding.imgProfile.load(it) {
                    crossfade(true)
                    placeholder(R.drawable.ic_placeholder)
                    error(R.drawable.ic_placeholder)
                    transformations(CircleCropTransformation())
                }
            }

            updateProfilePic()
        }
    }

    private lateinit var validateUserDetailViewList: List<EditText>


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_update_child_profile,
                container,
                false
            )
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        validator = Validator(binding)
        loadingIndicator = getLoadingIndicator(requireContext())

        setHasOptionsMenu(true)

        Log.e("token", GlobalClass.prefs.getValueString("token").toString())



        validateUserDetailViewList = mutableListOf(

            binding.editName,
            binding.edtUsername,

            binding.edtBirthdate
        )



        binding.txtRole.setAdapter(
            ArrayAdapter(
                requireContext(),
                android.R.layout.simple_spinner_dropdown_item,
                roleList
            )
        )
        binding.llAdmin.setOnClickListener() {
            if (is_admin == 0) {
                is_admin = 1
                binding.ivChecked.setImageResource(R.drawable.ic_checked)
            } else {
                is_admin = 0
                binding.ivChecked.setImageResource(R.drawable.ic_checked)
            }
        }

        binding.edtBirthdate.setOnClickListener {

            val calendar = Calendar.getInstance()

            val date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                onDateSet(view, year, monthOfYear, dayOfMonth)
            }

            val dateDialog = DatePickerDialog(
                requireContext(), R.style.datepickerstyle, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            )

//            dateDialog.datePicker.minDate = System.currentTimeMillis() - 1000
            dateDialog.datePicker.maxDate = calendar.timeInMillis

            dateDialog.show()
            dateDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE)
                .setTextColor(resources.getColor(R.color.colorPrimary))
            dateDialog.getButton(DatePickerDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(R.color.colorPrimary))
        }


        binding.ivUpload.setOnClickListener {
            imagepickerdialog()


        }

        binding.imgProfile.setOnClickListener {
            imagepickerdialog()
        }
        binding.tvOk.setOnClickListener {
            if (binding.txtRole.text.toString() == "Son") {
                role = "4"
            } else if (binding.txtRole.text.toString() == "Daughter") {
                role = "5"
            } else if (binding.txtRole.text.toString() == "Mother") {
                role = "3"
            } else if (binding.txtRole.text.toString() == "Father") {
                role = "2"
            } else {
                role = ""
            }

            if (binding.edtEmail.text.toString() != "") {
                if (isValidEmailId(binding.edtEmail.text.toString().trim())) {
//                    Toast.makeText(requireContext(), "Valid Email Address.", Toast.LENGTH_SHORT).show()
                    updateUserDetail()
                } else {
                    Toast.makeText(requireContext(), "InValid Email Address.", Toast.LENGTH_SHORT)
                        .show()
                }

            } else {
                updateUserDetail()
            }


        }

        return binding.root
    }

    private fun isValidEmailId(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getuser()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getuser() {

        if (isNetworkConnected(requireContext())) {
            viewModel.getUserDetail(
                args.childToken
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    if (response.error == 200) {

                                        if (response.data.role == 3 || response.data.role == 2) {


                                            binding.tilFamilyName.visibility = View.VISIBLE

                                        }

                                        if (response.data.isAdmin == 1) {
                                            binding.llAdmin.visibility = View.VISIBLE
                                        } else if (response.data.role == 2 || response.data.role == 3) {
                                            binding.llAdmin.visibility = View.VISIBLE
                                            binding.ivChecked.setImageResource(R.drawable.ic_uncheck)
                                        }


                                        /*   if (GlobalClass.prefs.getValueString("role")
                                                   .toString() == "Child"||GlobalClass.prefs.getValueString("isadmin")==response.data.isAdmin.toString()
                                           ) {
                                               role_id = 4
                                               if (role_id != response.data.role) {
                                                   binding.tvOk.visibility = View.INVISIBLE
                                                   binding.editName.isEnabled = false
                                                    binding.editFamilyName.isEnabled=false
                                                   binding.edtUsername.isEnabled = false
                                                   binding.edtEmail.isEnabled = false
                                                   binding.edtBirthdate.isEnabled = false
                                                   binding.edtPassword.isEnabled = false
                                                   binding.txtRole.isEnabled = false
                                                   binding.edtPhNo.isEnabled = false

                                               }
                                           }else{
                                               binding.editFamilyName.isEnabled=false
                                               binding.edtUsername.isEnabled=false

                                           }*/

                                        /* if (GlobalClass.prefs.getValueString("isadmin")=="0"){



                                                 disablecontrol()

                                         }
                                         else{

                                             if (response.data.isAdmin==1&&response.data.role==3){
                                                 disablecontrol()
                                             }
                                             binding.editFamilyName.isEnabled=false
                                             binding.edtUsername.isEnabled=false
                                         }*/


                                        if (GlobalClass.prefs.getValueString("role")
                                                .equals("Child")
                                        ) {
                                            disablecontrol()
                                        } else {
                                            if (response.data.role == 2 || response.data.role == 3) {

                                                binding.edtUsername.isEnabled = false


                                           /*     binding.editFamilyName.isEnabled = false
                                                binding.edtUsername.isEnabled = false*/
                                            }

                                        }



                                        Glide.with(this)
                                            .load(response.data.profileUrl)
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true)
                                            .into(binding.imgProfile)
                                        viewModel.setUserDetail(response.data)
                                        child_id = response.data.userId!!.toInt()
                                        is_admin = response.data.isAdmin!!.toInt()
                                        binding.edtPhNo.setText(response.data.phoneNo.toString())
                                        binding.edtEmail.setText(response.data.email.toString())
                                        binding.edtBirthdate.setText(displayTime(response.data.birthDate.toString()))


                                        if (response.data.role.toString() == "2") {
                                            viewModel.userLive.value?.rolename = "Father"
                                            roleList = listOf("Father", "Mother")
                                            role_id = response.data.role!!
                                            binding.edtUsername.isEnabled=false


                                        } else if (response.data.role.toString() == "3") {
                                            viewModel.userLive.value?.rolename = "Mother"
                                            roleList = listOf("Father", "Mother")
                                            role_id = response.data.role!!
                                            binding.edtUsername.isEnabled=false


                                        } else if (response.data.role.toString() == "4") {
                                            viewModel.userLive.value?.rolename = "Son"
                                            roleList = listOf("Son", "Daughter")
                                            binding.imgProfile.isEnabled = false
                                            role_id = response.data.role!!


                                        } else if (response.data.role.toString() == "5") {
                                            viewModel.userLive.value?.rolename = "Daughter"
                                            roleList = listOf("Son", "Daughter")
                                            binding.imgProfile.isEnabled = false
                                            role_id = response.data.role!!

                                        }
                                        binding.txtRole.setAdapter(
                                            ArrayAdapter(
                                                requireContext(),
                                                android.R.layout.simple_spinner_dropdown_item,
                                                roleList
                                            )
                                        )
                                    } else {
                                        Toast.makeText(
                                            context,
                                            response.message,
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }

                                }

                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }

    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun imagepickerdialog() {
        context?.let { c ->
            showMediaPickerDialog(c, {
                imageUri = FileProvider.getUriForFile(
                    c.applicationContext,
                    com.app.familydays.BuildConfig.APPLICATION_ID + ".provider",
                    File.createTempFile("image", ".jpg", c.filesDir)
                )
                takePhoto.launch(imageUri)
            }, {
                pickImage.launch("image/*")
            })
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {

//        menu.findItem(R.id.item_profile).isVisible = false
        menu.findItem(R.id.item_add_family).isVisible = false
        menu.findItem(R.id.item_child_profile).isVisible = false
        menu.findItem(R.id.item_add_reward).isVisible=false

        super.onPrepareOptionsMenu(menu)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun updateProfilePic() {


        if (isNetworkConnected(requireContext())) {

            val filePart: MultipartBody.Part? = imageUri?.let {
                activity?.application?.contentResolver?.openInputStream((it))?.use { inputStream ->
                    MultipartBody.Part.createFormData(
                        "profile_file",
                        "image.jpg",
                        inputStream.readBytes().toRequestBody("image/jpeg".toMediaType())
                    )
                }
            }

            viewModel.submitProfilePic(
                args.childToken, filePart
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            resource.data?.let { response ->
                                if (response.status) {
//                                    viewModel.setUserDetail(response.data)
                                    GlobalClass.prefs.SaveString(
                                        "profilePic",
                                        response.data.profileUrl
                                    )

                                    getuser()

                                    (requireContext() as DashboardActivity).loadprofilepic()

                                    loadingIndicator.dismiss()
                                }
                                Toast.makeText(
                                    context,
                                    response.message,
                                    Toast.LENGTH_LONG
                                ).show()

                            }

                        }
                        Status.ERROR -> {
                            resource.exception?.printStackTrace()
                            loadingIndicator.dismiss()
                        }

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }

    fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
        val userAge: Calendar = GregorianCalendar(year, month, day)
        val minAdultAge: Calendar = GregorianCalendar()
        minAdultAge.add(Calendar.YEAR, -18)
        if (role_id == 2 || role_id == 3) {
            if (minAdultAge.before(userAge)) {
                Toast.makeText(
                    requireContext(),

                    getString(R.string.txt_birthday_validation),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val myFormat = "MMM-dd-yyyy" //In which you need put here
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                binding.edtBirthdate.setText(sdf.format(myCalendar.time))
            }
//            Toast.makeText(context, getString(R.string.txt_birthday_validation), Toast.LENGTH_SHORT).show()
        } else {
            val myFormat = "MMM-dd-yyyy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            binding.edtBirthdate.setText(sdf.format(myCalendar.time))

        }


    }


    fun disablecontrol() {
        binding.tvOk.visibility = View.INVISIBLE
        binding.llAdmin.visibility = View.INVISIBLE
        binding.imgProfile.isEnabled = false
        binding.ivUpload.visibility = View.INVISIBLE
        binding.editFamilyName.isEnabled = false
        binding.edtPhNo.isEnabled = false
        binding.editName.isEnabled = false
        binding.txtRole.isEnabled = false
        binding.edtUsername.isEnabled = false
        binding.edtEmail.isEnabled = false
        binding.edtPassword.isEnabled = false
        binding.edtBirthdate.isEnabled = false
        binding.txtmsg.visibility=View.VISIBLE
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun updateUserDetail() {

        val filePart: MultipartBody.Part? = imageUri?.let {
            activity?.application?.contentResolver?.openInputStream((it))?.use { inputStream ->
                MultipartBody.Part.createFormData(
                    "profile_file",
                    "image.jpg",
                    inputStream.readBytes().toRequestBody("image/jpeg".toMediaType())
                )
            }
        }


        if (binding.txtRole.text.toString() == "Father") {
            viewModel.userLive.value?.role = 2
        } else if (binding.txtRole.text.toString() == "Mother") {
            viewModel.userLive.value?.role = 3
        } else if (binding.txtRole.text.toString() == "Son") {
            viewModel.userLive.value?.role = 4
        } else if (binding.txtRole.text.toString() == "Daughter") {
            viewModel.userLive.value?.role = 5
        }

        var bday = ""

        if (binding.edtBirthdate.text.toString() != "") {
            bday = finalDateTime(binding.edtBirthdate.text.toString()).toString()

        } else {
            bday = ""
        }

        if (validator.validate(validateUserDetailViewList) && isNetworkConnected(requireContext())) {
            viewModel.updateChildDetail(
                GlobalClass.prefs.getValueString("token").toString(),
                binding.edtUsername.text.toString().toRequestBody(),
                child_id.toString().toRequestBody(),
                binding.editName.text.toString().toRequestBody(),
                binding.edtEmail.text.toString().toRequestBody(),
                binding.edtPassword.text.toString().toRequestBody(),
                bday.toRequestBody(),
                role.toRequestBody(),
                is_admin.toString().toRequestBody(),
                binding.edtPhNo.text.toString().toRequestBody(),
                filePart
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    Toast.makeText(
                                        requireContext(),
                                        "Profile updated successfully!",
                                        Toast.LENGTH_LONG
                                    ).show()
                                findNavController().navigateUp()

                                } else {
                                    Toast.makeText(
                                        context,
                                        response.message,
                                        Toast.LENGTH_LONG
                                    ).show()
                                }


                            }
                        }
                        Status.ERROR -> {
                            resource.exception?.printStackTrace()
                            loadingIndicator.dismiss()
                        }

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }

    fun displayTime(inputDateTime: String?): String? {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "MMM-dd-yyyy"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    fun finalDateTime(inputDateTime: String?): String? {
        val inputPattern = "MMM-dd-yyyy"
        val outputPattern = "yyyy-MM-dd"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }
}
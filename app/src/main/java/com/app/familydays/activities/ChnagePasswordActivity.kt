package com.app.familydays.activities

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import com.app.familydays.R
import com.app.familydays.databinding.ActivityChnagePasswordBinding
import com.app.familydays.databinding.ActivityLoginBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.viewmodels.ChangePasswordVM
import com.app.familydays.viewmodels.LoginVm
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.android.viewmodel.ext.android.viewModel

class ChnagePasswordActivity : AppCompatActivity() {
    private lateinit var binding: ActivityChnagePasswordBinding
    private val viewModel: ChangePasswordVM by viewModel()
    private lateinit var loadingIndicator: KProgressHUD


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_chnage_password)
        loadingIndicator = getLoadingIndicator(this)


        binding.tvsubmit.setOnClickListener(){
            if (binding.edtPassword.text.toString().isEmpty()){
                Toast.makeText(applicationContext, "Please enter password", Toast.LENGTH_SHORT).show()
            }else if (binding.edtcPassword.text.toString().isEmpty()){
                Toast.makeText(applicationContext, "Please enter confirm password", Toast.LENGTH_SHORT).show()
            }
            else if (binding.edtPassword.text.toString()!=binding.edtcPassword.text.toString()){
                Toast.makeText(applicationContext, "Password and confirm password should be same.", Toast.LENGTH_SHORT).show()
            }else{
                resetpassword()
            }
        }








    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun resetpassword() {
        if (isNetworkConnected(baseContext))
            viewModel.changepassword(
                GlobalClass.prefs.getValueString("token").toString(),
                binding.edtcPassword.text.toString()
            ).observe(
                this
            ) { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        loadingIndicator.dismiss()
                        resource.data?.let { response ->
                            if (response.status) {

                                startActivity(
                                    Intent(
                                        this,
                                        DashboardActivity::class.java
                                    ).addFlags(
                                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    )
                                )
                                finish()
                            } else {

                                Toast.makeText(
                                    this,
                                    response.message,
                                    Toast.LENGTH_LONG
                                ).show()

                            }


                        }

                    }
                    Status.ERROR -> {
                        loadingIndicator.dismiss()

                    }
                    Status.LOADING -> {
                        loadingIndicator.show()
                    }
                }


            }

    }
}
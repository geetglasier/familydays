package com.app.familydays.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.AdapterView
import android.widget.DatePicker
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.adapters.BrandArrayAdapter
import com.app.familydays.adapters.BrandImageAdapter
import com.app.familydays.adapters.PointsAdapter
import com.app.familydays.databinding.FragmentUpdateRewardBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.viewmodels.*
import com.app.familydays.webservice.responses.GetRewardResp
import com.app.familydays.webservice.responses.RewardBrandResp
import com.app.familydays.webservice.responses.RewardCategoryResp
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.bottomsheet_layout.view.*
import kotlinx.android.synthetic.main.fragment_add_new_chore.*
import kotlinx.android.synthetic.main.fragment_add_new_chore.txtpoints
import kotlinx.android.synthetic.main.fragment_add_new_chore.txttimeframe
import kotlinx.android.synthetic.main.fragment_add_new_reward.*
import kotlinx.android.synthetic.main.webviewdialog.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList


class UpdateRewardFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    private lateinit var binding: FragmentUpdateRewardBinding
    private val viewModel: GetRewardDetailsVM by viewModel()
    private val viewModelupdate: UpdateRewardVM by viewModel()
    private lateinit var loadingIndicator: KProgressHUD
    private val args: UpdateRewardFragmentArgs by navArgs()
    private val viewModelsubbrand: GetRewardBrandVM by viewModel()


    private val viewModel2: PopularChoreListVm by viewModel()
    var day = 0
    var month: Int = 0
    var year: Int = 0

    var myday = 0
    var myMonth: Int = 0
    var myYear: Int = 0

    lateinit var time: String
    var date = Calendar.getInstance()


    var listbrand: ArrayList<RewardBrandResp.Data>? = null
    var choreIcon = ""

    var bottomSheetDialog: BottomSheetDialog? = null
    var editText: TextView? = null
    var editTextpoints: TextView? = null

    var txttitle: TextView? = null
    var webview: WebView? = null

    var btnclose: TextView? = null

    val selectedChild = StringBuilder()

    var selectedDate: LocalDate? = null
    var currentDate: LocalDate? = null
    var max: Int = 0
    var min: Int = 0
    var category_id = 0
    var brand_name = ""
    var frame_date = ""
    var url = ""
    var title = ""

    var brand_icon = ""
    var cat_id = 0
    var reward_id=0
    var category_name=""
    var category_icon=""
    var point=0



    var arrlistCatList: ArrayList<RewardCategoryResp.Data>? = null


    companion object {

        lateinit var instance: UpdateRewardFragment

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_update_reward, container, false)
        binding.args = args
        loadingIndicator = getLoadingIndicator(requireContext())

        listbrand = ArrayList()
        arrlistCatList = ArrayList()

        getrewardcategorylist()

        isCancelable=false
        binding.pointsll.setOnClickListener() {
            showcustomdialog()
        }

        instance = this
        // category_id=args.rewardId.toInt()


        if (args.selectedreward.isNotEmpty()) {
            Log.e("xxx", args.rewardId)
            //    cat_id = args.rewardId.toInt()
            GlobalClass.selectedimage_url
            binding.chkreward.setImageResource(R.drawable.ic_rec_checked)
            binding.chkcustom.setImageResource(R.drawable.ic_rec_unchecked)
            binding.etchoretitle.isEnabled = false


        } else if (args.selectedreward.isEmpty()&&args.rewardId.isNotEmpty()) {



        }else{

        }




        DrawableCompat.setTint(
            binding.etcustomtitle.getBackground(),
            ContextCompat.getColor(requireContext(), R.color.white)
        )

        binding.llCustomerreward.setOnClickListener() {

            category_id = 1
            cat_id = 1
            binding.chkreward.setImageResource(R.drawable.ic_rec_unchecked)
            binding.chkcustom.setImageResource(R.drawable.ic_rec_checked)
            binding.etchoretitle.isEnabled = true
            binding.spnrbrandname.visibility = View.GONE
            binding.etcustomtitle.visibility = View.VISIBLE
            binding.rvimgitem.visibility = View.INVISIBLE
            binding.etchoretitle.hint = "Please enter reward name"
            binding.etcustomtitle.hint = "Please enter brand name"
            binding.etchoretitle.setText("")

            brand_icon = ""

        }

        binding.imgClose.setOnClickListener() {
            GlobalClass.selectedChildId = ArrayList()
            findNavController().navigateUp()

         /*   setFragmentResult(
                "refresh",
                bundleOf("fromUpdateChore" to true)
            )*/
        }
        binding.txtsave.setOnClickListener() {

            if (brand_icon == ""&&category_id==1) {
                brand_icon = "Other"
                category_name=binding.etchoretitle.text.toString()
            }else{
                brand_icon=GlobalClass.selectedimage_url
                Log.e("xxx",GlobalClass.selectedimage_url)
            }



            if (binding.etchoretitle.text.isEmpty()) {
                Toast.makeText(
                    requireContext(),
                    "Please enter and select reward name",
                    Toast.LENGTH_SHORT
                ).show()

            }else if(brand_name==""){
                Toast.makeText(
                    requireContext(),
                    "Please select brand name",
                    Toast.LENGTH_SHORT
                ).show()
            }else{
                if (category_id==1){
                    brand_name = etcustomtitle.text.toString()
                }

                updaterewardapicall()

            }


        }

        binding.txttimeframe.setOnClickListener() {
            //Toast.makeText(requireContext(), "Date Picker", Toast.LENGTH_SHORT).show()
            datepickerdialog()
//            showDateTimePicker()
        }



        binding.etchoretitle.setOnClickListener() {
            binding.chkreward.setImageResource(R.drawable.ic_rec_unchecked)
            binding.chkcustom.setImageResource(R.drawable.ic_rec_checked)
        }

        binding.spnrbrandname.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                // your code here
                val imageurls = parentView!!.getItemAtPosition(position)

                val sub = imageurls as RewardBrandResp.Data


                Log.e("selectname", imageurls.toString())

                brand_name = imageurls.brand_name


                Log.d("selectedXXX", brand_name)




                binding.rvimgitem.adapter = BrandImageAdapter(requireContext(),"update").apply {
                    brandList = imageurls.sub_brands
                }


            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here

            }


        }

        reward_id=args.rewardId.toInt()

        Log.d("XXXR",reward_id.toString())
        if (args.selectedreward == "Other") {
            binding.etchoretitle.hint = "Please enter reward title"
            binding.chkreward.setImageResource(R.drawable.ic_rec_unchecked)
            binding.chkcustom.setImageResource(R.drawable.ic_rec_checked)
        } else {
            binding.etchoretitle.setText(args.selectedreward)
            Log.d("XXX", args.rewardId)
            if (args.rewardId.isNotEmpty()) {
                getrewardbrand(args.catId)
            }


        }
        binding.llreward.setOnClickListener() {
            if (category_id == 0) {
                category_id = 1
            }

            GlobalClass.setTitle = "Reward"
            findNavController().navigate(
                PopularChoreListFragmentDirections.actionGlobalNavPopularChore(
                    "updatereward",
                    "",
                    reward_id.toString()
                )
            )
        }


        return binding.root

    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun getrewardbrand(category:String) {
        if (isNetworkConnected(requireContext())) {
            viewModelsubbrand.getRewardBrand(
                category
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

//                                    min=response.minimum_point
//                                    max=response.maximum_point
                                    binding.rvimgitem.visibility=View.VISIBLE
                                    val subBrand: ArrayList<String> = ArrayList()
                                    for (i in response.data.indices) {
                                        subBrand.add(response.data[i].brand_name)

                                    }

                                    listbrand!!.clear()
                                    listbrand!!.addAll(response.data)

                                    val adapter =
                                        BrandArrayAdapter(requireActivity(), response.data)
                                    binding.spnrbrandname.adapter = adapter


                                }


                            }
                        }

                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()

                    }

                }

        }

    }

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(Build.VERSION_CODES.M)
    private fun getrewarddetails() {
        viewModel.getrewarddetails(
            args.rewardId,args.notificationId
        ).observe(this) { resources ->
            when (resources.status) {
                Status.SUCCESS -> {
                    loadingIndicator.dismiss()
                    resources.data.let { response ->
                        if (response != null) {
                            if (response.status) {

                                if(response.data.category == "Other"){
                                    binding.etchoretitle.setText(response.data.reward_name)
                                    binding.etcustomtitle.setText(response.data.brand_name)




                                }else{
                                    binding.etchoretitle.setText(response.data.category)
                                    binding.etcustomtitle.setText(response.data.category)


                                }



                                max = response.data.maximum_point
                                min = response.data.minimum_point
                                binding.minimumpts.text = min.toString()
                                binding.maximumpts.text = max.toString()
                                binding.txtpoints.text = response.data.point.toString()

                                GlobalClass.selectedimage_url=response.data.brand_icon_name


                                brand_name=response.data.brand_name
                               // binding.rvimgitem.visibility=View.VISIBLE

                                category_name=response.data.category

                                binding.txttimeframe.setText(parseDateTime(response.data.due_date))

                                brand_icon=response.data.brand_icon
                                binding.llreward.isEnabled=true

                                if (GlobalClass.prefs.getValueString("token")!=response.data.token&&GlobalClass.prefs.getValueString("isadmin")=="0"){
                                    /*     binding.llreward.isEnabled=false
                                         binding.llCustomerreward.isEnabled=false*/
                                    binding.etchoretitle.isEnabled=false
                                    binding.spnrbrandname.isEnabled=false
                                    binding.txttimeframe.isEnabled=false
                                    binding.rvimgitem.isEnabled=false
                                    binding.pointsll.isEnabled=false
                                    binding.txtsave.isEnabled=false
                                    binding.llimgitem.isEnabled=false
                                }else if (GlobalClass.prefs.getValueString("token")==response.data.token&&GlobalClass.prefs.getValueString("isadmin")=="0"){
                                    /* binding.llreward.isEnabled=false
                                     binding.llCustomerreward.isEnabled=false*/
                                    binding.etchoretitle.isEnabled=false
                                    binding.rvimgitem.isEnabled=false
                                    binding.spnrbrandname.isEnabled=false
                                }

                                if(args.from == ""){
                                    if (arrlistCatList!!.size > 0) {
                                        for (i in 0 until arrlistCatList!!.size) {

                                        Log.d("arrtitle",arrlistCatList!![i].title)
                                            if(arrlistCatList!![i].title == response.data.category){

                                                binding.spnrbrandname.visibility = View.VISIBLE
                                                binding.etcustomtitle.visibility=View.GONE

                                                Log.e("category",arrlistCatList!![i].cat_id)
                                                getrewardbrand(arrlistCatList!![i].cat_id)

                                                GlobalClass.selectepos=i


                                                cat_id=arrlistCatList!![i].cat_id.toInt()
                                                category_icon= arrlistCatList!![i].iconName



                                                binding.chkreward.setImageResource(R.drawable.ic_rec_checked)
                                                binding.chkcustom.setImageResource(R.drawable.ic_rec_unchecked)


                                                Log.d("xxxcat",category_icon)


                                                break

                                            }

                                            else{
                                                binding.chkcustom.setImageResource(R.drawable.ic_rec_checked)
                                                binding.chkreward.setImageResource(R.drawable.ic_rec_unchecked)
                                                cat_id=1
                                                category_id=1
                                                category_icon= arrlistCatList!![i].iconName

                                                binding.etcustomtitle.visibility=View.VISIBLE
                                                binding.spnrbrandname.visibility = View.GONE
                                                binding.etcustomtitle.setText(response.data.brand_name)

                                            }

                                        }


                                        /*  val format = SimpleDateFormat("MMM-dd-yyyy ")
                                          val dateToStr = format.format(response.data.due_date)
                                          binding.txttimeframe.setText(dateToStr)*/



                                    }
                                }else{

                                    if(response.data.category == "Other"){
                                        binding.etchoretitle.setText(response.data.reward_name)

                                    }else{
                                        binding.etchoretitle.setText(response.data.category)

                                    }


                                    if (arrlistCatList!!.size > 0) {
                                        for (i in 0 until arrlistCatList!!.size) {

                                            if(arrlistCatList!![i].title == args.selectedreward){

                                                Log.e("category",arrlistCatList!![i].cat_id)



                                                getrewardbrand(arrlistCatList!![i].cat_id)

                                                cat_id=arrlistCatList!![i].cat_id.toInt()
                                                category_icon= arrlistCatList!![i].iconName



                                                Log.d("xxxcat",category_icon)


                                                binding.chkreward.setImageResource(R.drawable.ic_rec_checked)
                                                binding.chkcustom.setImageResource(R.drawable.ic_rec_unchecked)

                                                break

                                            }

                                            else{
                                                binding.chkcustom.setImageResource(R.drawable.ic_rec_checked)
                                                binding.chkreward.setImageResource(R.drawable.ic_rec_unchecked)
                                                cat_id=1
                                                category_id=1



                                            }

                                        }


                                        /*  val format = SimpleDateFormat("MMM-dd-yyyy ")
                                          val dateToStr = format.format(response.data.due_date)
                                          binding.txttimeframe.setText(dateToStr)*/



                                    }

                                }



                            }
                        }
                    }

                }

                Status.ERROR -> {
                    loadingIndicator.dismiss()

                }
                Status.LOADING -> {
                    loadingIndicator.show()
                }
            }

        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun getrewardcategorylist() {

        if (isNetworkConnected(requireContext())) {
            viewModel2.getRewardcategoryList(
                GlobalClass.prefs.getValueString("token").toString()
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    arrlistCatList!!.clear()
                                    arrlistCatList!!.addAll(response.data)

                                    getrewarddetails()

                                }


                            }
                        }

                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()

                    }

                }

        }

    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun updaterewardapicall(){
        var prefix = ""
        for (str in GlobalClass.selectedChildId.distinct()) {
            selectedChild.append(prefix)
            prefix = ","
            selectedChild.append(str)
        }
        frame_date = binding.txttimeframe.text.toString()

        if (brand_icon == ""&&category_id==1) {
            brand_icon = "Other"
        }

        val today = Date()
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val dateToStr = format.format(today)

        Log.e("date", dateToStr)

        if (brand_icon == ""&&category_id==1) {
            brand_icon = "Other"

        }
        if (binding.etchoretitle.text.isEmpty()) {
            Toast.makeText(
                requireContext(),
                "Please enter and select reward name",
                Toast.LENGTH_SHORT
            ).show()

        }else if (brand_icon == ""&&category_id==0){
            Toast.makeText(requireContext(), "please select product", Toast.LENGTH_SHORT).show()
        } else {
            if (!isNetworkConnected(requireContext())) {
                Toast.makeText(
                    requireContext(),
                    "please check your internet connection",
                    Toast.LENGTH_SHORT
                ).show()
            } else {




                Log.d("reward_id", reward_id.toString())
                Log.d("cat_id", cat_id.toString())
                Log.d("category",   binding.etchoretitle.text.toString())
                Log.d("cat_icon", category_icon)
                Log.d("brand name",   brand_name)
                Log.d("brand_icon", brand_icon)
                Log.d("date", finalDateTime(binding.txttimeframe.text.toString()).toString())
                Log.d("points", binding.txtpoints.text.toString())




                viewModelupdate.updatereward(
                    GlobalClass.prefs.getValueString("token").toString(),
                    binding.etcustomtitle.text.toString(),
                    reward_id,
                    cat_id,
                    binding.etchoretitle.text.toString(),
                    category_icon,
                    brand_name,
                    brand_icon,
                    finalDateTime(binding.txttimeframe.text.toString()).toString(),
                    binding.txtpoints.text.toString().toInt()
                ).observe(this) { resources ->
                    when (resources.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resources.data.let { response ->

                                if (response != null) {

                                    if (response.status) {



                                        GlobalClass.chore_type = "0"
                                        setFragmentResult(
                                            "refresh",
                                            bundleOf("fromUpdateChore" to true)
                                        )
                                        GlobalClass.selectedimage_url=""

                                        Toast.makeText(
                                            requireContext(),
                                            response.message,
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        findNavController().navigateUp()
                                    }
                                }

                            }

                        }

                        Status.ERROR -> {
                            loadingIndicator.dismiss()
                        }
                        Status.LOADING -> {
                            loadingIndicator.show()

                        }


                    }
                }
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun finalDateTime(inputDateTime: String?): String? {
        val inputPattern = "MMM-dd-yyyy"
        val outputPattern = "dd-MM-yyyy"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.US)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.US)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    private fun datepickerdialog() {
        val calendar = Calendar.getInstance()

        year = calendar[Calendar.YEAR]
        month = calendar[Calendar.MONTH]
        day = calendar[Calendar.DAY_OF_MONTH]
        val datePickerDialog =
            DatePickerDialog(requireActivity(), R.style.DialogTheme, this, year, month, day)
        datePickerDialog.datePicker.minDate = calendar.timeInMillis
        datePickerDialog.show()
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )
        )
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )
        )
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000

    }

    private fun showcustomdialog() {

        val viewGroup: ViewGroup? = requireView().findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(requireContext())
            .inflate(R.layout.bottomsheet_layout, viewGroup, false)
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())


        //setting the view of the builder to our custom view that we already inflated

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it

        //finally creating the alert dialog and displaying it
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
        editTextpoints = dialogView.findViewById<TextView>(R.id.etpoints)
        // editText.filters = arrayOf<InputFilter>(MinMaxFilter("1", "500"))
        editTextpoints!!.setText(binding.txtpoints.text.toString())



        dialogView.imgback.setOnClickListener() {
            alertDialog.dismiss()
            GlobalClass.selectedimage_url = ""

        }
        val pointslist: MutableList<String> = ArrayList()
        for (i in min..max) {
            pointslist.add(i.toString())
            // Log.d("xxx",pointslist.size.toString())
        }

        dialogView.etpoints.setOnClickListener() {

            bottomSheetDialog = BottomSheetDialog(requireContext())
            val btnsheet = layoutInflater.inflate(R.layout.bottomsheet, null)


            val rec = btnsheet.findViewById<RecyclerView>(R.id.rvpoints)

            rec.layoutManager = LinearLayoutManager(requireContext())
            val adapter = PointsAdapter(requireContext(), pointslist, "updatereward")
            rec.adapter = adapter

            bottomSheetDialog!!.setContentView(btnsheet)

//            val bottomSheetBehavior = BottomSheetBehavior.from(btnsheet.getParent() as View)
//            bottomSheetBehavior.peekHeight = 500

            bottomSheetDialog!!.show()
        }
        dialogView.btnsave.setOnClickListener() {
            txtpoints.text = editTextpoints!!.text.toString()

            alertDialog.dismiss()


        }


    }


    fun showwebviewdialog(url: String) {

        val viewGroup: ViewGroup? = requireView().findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(requireContext())
            .inflate(R.layout.webviewdialog, viewGroup, false)
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())


        //setting the view of the builder to our custom view that we already inflated

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it

        //finally creating the alert dialog and displaying it
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
        txttitle = dialogView.findViewById<TextView>(R.id.textviewtitle)
        btnclose = dialogView.findViewById(R.id.textviewclose)
        webview = dialogView.findViewById(R.id.webview)


        // editText.filters = arrayOf<InputFilter>(MinMaxFilter("1", "500"))
        // editTextpoints!!.setText(binding.txtpoints.text.toString())

        class MyJavaScriptInterface(private val contentView: TextView) {
            fun processContent(aContent: String) {
                contentView.post {
                    contentView.text = aContent
                    txttitle!!.setText(aContent)

                }
            }
        }


        val webViewClient: WebViewClient = object : WebViewClient() {

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(request?.url.toString())
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {

                super.onPageStarted(view, url, favicon)


            }

            override fun onPageFinished(view: WebView?, url: String?) {

                txttitle!!.text = view?.title
                loadingIndicator.dismiss()
                super.onPageFinished(view, url)
            }
        }
        webview!!.webViewClient = webViewClient
//             binding.webview.settings.defaultTextEncodingName = "utf-8"
        webview!!.settings.javaScriptEnabled = true
        webview!!.settings.javaScriptCanOpenWindowsAutomatically = true
        webview!!.settings.domStorageEnabled = true

        webview!!.loadUrl(url)



        webview!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action === KeyEvent.ACTION_DOWN) {
                    val webView = v as WebView
                    when (keyCode) {
                        KeyEvent.KEYCODE_BACK -> if (webView.canGoBack()) {
                            webView.goBack()
                            return true
                        }
                    }
                }
                return false
            }
        })

        dialogView.textviewclose.setOnClickListener() {
            alertDialog.dismiss()
        }


    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {

        myYear = year
        myday = dayOfMonth
        myMonth = month + 1

        val c = Calendar.getInstance()




        selectedDate = LocalDate.of(myYear, myMonth, myday)
        currentDate =
            LocalDate.of(c[Calendar.YEAR], c[Calendar.MONTH] + 1, c[Calendar.DAY_OF_MONTH])

        Log.e("selecteddate", selectedDate.toString() + "=======" + currentDate.toString())

//set date here
        txttimeframe.setText(parseDateTime("$myday-$myMonth-$myYear"))

    }


    fun parseDateTime(inputDateTime: String?): String? {
        val inputPattern = "dd-MM-yyyy"
        val outputPattern = "MMM-dd-yyyy"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.US)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.US)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }


}
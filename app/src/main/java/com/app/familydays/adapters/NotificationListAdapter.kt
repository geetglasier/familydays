package com.app.familydays.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.databinding.ItemNotificationListBinding
import com.app.familydays.databinding.ItemPopularChoreBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.GetNotificationListResp
import com.app.familydays.webservice.responses.PopularChoreResp
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import android.widget.TextView

import android.text.Spannable

import android.text.style.ForegroundColorSpan

import android.text.SpannableString

import android.R
import android.annotation.SuppressLint
import android.graphics.Color
import android.text.Spanned
import androidx.core.content.res.TypedArrayUtils.getText
import android.text.Html
import androidx.navigation.fragment.findNavController
import com.app.familydays.fragments.ChatFragmentDirections
import com.app.familydays.fragments.ClaimListFragmentDirections
import com.app.familydays.fragments.UpdateChoresFragmentDirections
import com.app.familydays.fragments.UpdateRewardFragmentDirections
import kotlinx.android.synthetic.main.item_notification_list.view.*


class NotificationListAdapter(
    val context: Context,
    val onItemClicked:(GetNotificationListResp.Data) -> Unit,
):
    RecyclerView.Adapter<NotificationListAdapter.NotificationListViewHolder>() {
    var notificationlist: List<GetNotificationListResp.Data>? = null
        @SuppressLint("NotifyDataSetChanged")
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    inner class NotificationListViewHolder(val binding: ItemNotificationListBinding) :
        RecyclerView.ViewHolder(binding.root)  {
        fun bind(position: Int) {
            binding.notification = notificationlist?.get(position)

            if (notificationlist!![position].is_read=="0"){
                binding.clSelected.visibility=View.VISIBLE
            }else{
                binding.clSelected.visibility=View.INVISIBLE
            }

            if (notificationlist!![position].type == "send_message") {
                //move to message fragment

                val text = "<font color='black'>" +notificationlist!![position].full_name+" "+"</font><font color='gray'>"+notificationlist!![position].content+"</font>"+" "+"<font color='black'>" +" "+"</font>"

                binding.tvnotificationTitle.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE)


            } else if (notificationlist!![position].type == "cliam_create_child") {
                //move to comment listing
                val text = "<font color='black'>" +notificationlist!![position].message+" "+"</font><font color='gray'>"+notificationlist!![position].content+"</font>"+" "+"<font color='black'>" +notificationlist!![position].username+" "+"</font>"

                binding.tvnotificationTitle.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE)


            } else if (notificationlist!![position].type == "reward_create_by_child" || notificationlist!![position].type == "reward_create_by_parent") {
                //move to forum detail
                val text = "<font color='black'>" +notificationlist!![position].message+" "+"</font><font color='gray'>"+notificationlist!![position].content+"</font>"+" "+"<font color='black'>" +notificationlist!![position].username+" "+"</font>"

                binding.tvnotificationTitle.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE)


            }
            else if (notificationlist!![position].type == "chore_create_by_parent" || notificationlist!![position].type == "chore_create_by_child") {
                //move to forum detail

                val text = "<font color='black'>" +notificationlist!![position].message+" "+"</font><font color='gray'>"+notificationlist!![position].content+"</font>"+"\n"+"<font color='black'>" +notificationlist!![position].username+" "+"</font>"

                binding.tvnotificationTitle.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE)

            }/*else if(notificationlist!![position].type =="reward_create_by_parent"){

                val text = "<font color='black'>" +notificationlist!![position].message+" "+"</font><font color='gray'>"+notificationlist!![position].content+"</font>"+" "+"<font color='black'>" +notificationlist!![position].username+" "+"</font>"

                binding.tvnotificationTitle.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE)


            }else if (notificationlist!![position].type =="parents_confirm_reward"){




            }*/


            Glide.with(itemView)
                .load(notificationlist!![position]!!.profile_url)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(binding.imgProfile)

            binding.root.setOnClickListener(){
                onItemClicked(notificationlist?.get(position)!!)
            }


            binding.executePendingBindings()

        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        NotificationListViewHolder(
            ItemNotificationListBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    override fun onBindViewHolder(
        holder: NotificationListAdapter.NotificationListViewHolder,
        position: Int
    ) {
        holder.bind(position)

    }

    override fun getItemCount() = notificationlist?.size ?: 0

}
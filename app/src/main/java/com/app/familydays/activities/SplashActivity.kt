package com.app.familydays.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import com.app.familydays.R
import com.app.familydays.global.GlobalClass
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {

    var type: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val intent = intent.extras
        if (intent != null) {
            Log.e("type", intent!!["type"].toString())

            type = intent!!["type"].toString()
        }
        lifecycleScope.launch {
            delay(2000)
            if (GlobalClass.prefs.getValueString("token")?.isEmpty() == true) {
                startActivity(
                    Intent(baseContext, MainActivity::class.java).addFlags(
                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    )
                )
            } else {
            startActivity(
                Intent(baseContext, DashboardActivity::class.java).addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                )
            )
            }
        }
    }
}
package com.app.familydays.adapters

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.ItemFinishedChoreBinding
import com.app.familydays.databinding.ItemFinishedchorechildBinding

import com.app.familydays.fragments.DashboardFragment
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.ChildChoreResp

import java.text.SimpleDateFormat
import java.util.*

class FinishedchorechildAdapter (val context: Context,
val onItemClicked: (ChildChoreResp.Finish_chore) -> Unit
) :
RecyclerView.Adapter<FinishedchorechildAdapter.finishedchorechildViewHolder>() {
    var choreRespList: List<ChildChoreResp.Finish_chore>? = null
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    inner class finishedchorechildViewHolder(val binding: ItemFinishedchorechildBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @RequiresApi(Build.VERSION_CODES.M)

        fun bind(position: Int) {
            binding.finishedchorechild = choreRespList?.get(position)

            if (GlobalClass.prefs.getValueString("role").equals("Father") ||
                GlobalClass.prefs.getValueString("role").equals("Mother") ||
                choreRespList?.get(position)!!.is_admin == 1
            ) {

                binding.ll.visibility = View.VISIBLE

                if (choreRespList?.get(position)!!.is_admin_complete == 0) {


                    binding.ivCompleted.setImageDrawable(context.resources.getDrawable(R.drawable.ic_rec_checked))
                    binding.tvCompleted.setTextColor(context.resources.getColor(R.color.colorPrimary))


                } else if (choreRespList?.get(position)!!.is_admin_complete == 1) {

                    binding.ivInCompleted.setImageDrawable(context.resources.getDrawable(R.drawable.ic_rec_checked))
                    binding.tvCompleted.setTextColor(context.resources.getColor(R.color.colorPrimary))


                } else {

                    binding.ivCompleted.setImageDrawable(context.resources.getDrawable(R.drawable.ic_rec_unchecked))
                    binding.tvCompleted.setTextColor(context.resources.getColor(R.color.clr_gray_text))

                }


            } else {
                binding.ll.visibility = View.GONE

            }



            binding.llCompleted.setOnClickListener {


               if (choreRespList?.get(position)!!.is_admin_complete == 0) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else if (choreRespList?.get(position)!!.is_admin_complete == 1) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else {
                    binding.ivCompleted.isEnabled = true
                    binding.ivInCompleted.isEnabled = true


                    DashboardFragment.instance.iscompletedApiCall(
                        "0",
                        choreRespList?.get(position)!!.chore_id.toString()
                    )
                }


            }

            binding.llInCompleted.setOnClickListener {

                if (choreRespList?.get(position)!!.is_admin_complete == 0) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else if (choreRespList?.get(position)!!.is_admin_complete == 1) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else {
                    binding.ivCompleted.isEnabled = true
                    binding.ivInCompleted.isEnabled = true


                    DashboardFragment.instance.iscompletedApiCall(
                        "1",
                        choreRespList?.get(position)!!.chore_id.toString()
                    )

                }

            }

            if (choreRespList?.size!! > 1) {
                if (position == choreRespList?.size!! - 1) {
                    binding.vwLine.visibility = View.GONE
                }
            }

            if (GlobalClass.prefs.getValueString("role").equals("Father") ||
                GlobalClass.prefs.getValueString("role").equals("Mother") ||
                choreRespList?.get(position)!!.is_admin == 1
            ) {
                if (choreRespList?.get(position)!!.is_admin_complete == 0) {
                    //green
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))
                } else if (choreRespList?.get(position)!!.is_admin_complete == 1) {
                    //green
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))
                } else if (choreRespList?.get(position)!!.is_admin_complete == 2) {
                    //black
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.black))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.black))
                }


            } else {
                if (choreRespList?.get(position)!!.is_admin_complete == 0) {
                    //green
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))
                } else if (choreRespList?.get(position)!!.is_admin_complete == 2) {
                    //black

                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.black))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.black))
                }

            }

            if (binding.finishedchorechild?.is_admin_complete == 0) {
                binding.tvDateTime.text =
                    "Created for " + binding.finishedchorechild?.create_by + " " + binding.finishedchorechild?.cho_is_expired + " on " + dueDateFormate(
                        binding.finishedchorechild?.due_date
                    )
            } else {
                binding.tvDateTime.text =
                    "Created by " + binding.finishedchorechild?.create_by + " " + binding.finishedchorechild?.cho_is_expired + " on " + dueDateFormate(
                        binding.finishedchorechild?.due_date
                    )
            }


            binding.root.setOnClickListener {
                onItemClicked(binding.finishedchorechild!!)
            }
            binding.executePendingBindings()
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        finishedchorechildViewHolder(
                ItemFinishedchorechildBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(
        holder: FinishedchorechildAdapter.finishedchorechildViewHolder,
        position: Int
    ) {
        holder.bind(position)
    }

    override fun getItemCount() = choreRespList?.size ?: 0

    fun dueDateFormate(dueDate: String?): String {
        var spf = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val newDate: Date = spf.parse(dueDate)
        spf = SimpleDateFormat("MMM-dd-yyyy hh:mm aa")
        val newDateString: String = spf.format(newDate)
        Log.i("datetime", newDateString)
        return newDateString

    }
}
package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

class RewardBrandResp(
    @Json(name = "data")
    val `data`: List<Data>,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean) {

    data class Data(
        @Json(name = "sub_brands")
        val `sub_brands`: List<Sub_Brands>,
        @Json(name = "brand_id")
        val brand_id: String,
        @Json(name = "brand_name")
        val brand_name: String
        ){
        data class Sub_Brands(
            @Json(name = "id")
            val id: String,
            @Json(name = "brand_id")
            val brand_id: String,
            @Json(name = "brand_icon_name")
            val brand_icon_name: String,
            @Json(name = "brand_icon")
            val brand_icon: String,
            @Json(name = "brand_url")
            val brand_url: String,

            )
    }
}
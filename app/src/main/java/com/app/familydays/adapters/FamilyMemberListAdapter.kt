package com.app.familydays.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.databinding.ItemFamilyMemberBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.FamilyMemberListResp
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class FamilyMemberListAdapter(
        val onItemClicked: (FamilyMemberListResp.Data) -> Unit
) :
        RecyclerView.Adapter<FamilyMemberListAdapter.BlockUserViewHolder>() {
    var familyMemberList: List<FamilyMemberListResp.Data>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class BlockUserViewHolder(val binding: ItemFamilyMemberBinding) :
            RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.member = familyMemberList?.get(position)
            binding.root.setOnClickListener {
                onItemClicked(binding.member!!)
            }

            if (familyMemberList!![position].role == 2) {
                GlobalClass.prefs.SaveString("roletypeF", "Father")

            }
            if (familyMemberList!![position].role == 3) {
                GlobalClass.prefs.SaveString("roletypeM", "Mother")
            }




            if (familyMemberList!![position].isAdmin == "Admin") {
                binding.tvRole.text = familyMemberList!![position].roleType + "(" + familyMemberList!![position].isAdmin + ")"

            } else {
                binding.tvRole.text = familyMemberList!![position].roleType
            }

            if (familyMemberList!![position].roleType == "Mother" || familyMemberList!![position].roleType == "Father") {
                binding.tvpoints.visibility = View.GONE
            } else {
                binding.tvpoints.visibility = View.VISIBLE
            }



            if (familyMemberList!![position].email == "") {
                binding.tvEmail.text = familyMemberList!![position].username
            } else {
                binding.tvEmail.text = familyMemberList!![position].email

            }

            Glide.with(itemView)
                    .load(familyMemberList!![position]!!.profileUrl)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(binding.imgProfile)


            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            BlockUserViewHolder(
                    ItemFamilyMemberBinding
                            .inflate(
                                    LayoutInflater.from(parent.context),
                                    parent, false
                            )
            )

    override fun onBindViewHolder(
            holder: FamilyMemberListAdapter.BlockUserViewHolder,
            position: Int
    ) {
        holder.bind(position)


    }

    override fun getItemCount() = familyMemberList?.size ?: 0
}
package com.app.familydays.fragments

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.DatePicker
import android.widget.TextView
import android.widget.TimePicker
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import br.com.ilhasoft.support.validation.Validator
import com.app.familydays.R
import com.app.familydays.adapters.AssignedChoreAdapter
import com.app.familydays.adapters.RewardListAdapter
import com.app.familydays.databinding.FragmentAddFamilyMemberBinding
import com.app.familydays.databinding.FragmentAddNewChoreBinding
import com.app.familydays.databinding.FragmentAddNewRewardBinding
import com.app.familydays.databinding.FragmentRewardBinding
import com.app.familydays.global.AppConstant
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.utils.unAuthorisedUserDialog
import com.app.familydays.viewmodels.*
import com.app.familydays.webservice.responses.FamilyMemberListResp
import com.app.familydays.webservice.responses.GetAssignedChoreResp
import com.app.familydays.webservice.responses.GetRewardResp
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.webviewdialog.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList


class RewardFragment :  Fragment(){

    var RewardList: ArrayList<GetRewardResp.Data>? = null
    private lateinit var binding: FragmentRewardBinding
    private val viewModel:RewardlistVM by viewModel()
    private val viewModelrewardbyuser:RewardlistVM by viewModel()
    private lateinit var loadingIndicator: KProgressHUD

    private val viewModeldelete:DeleteChoreVM by viewModel()
    private val viewModelconfirm:ConfirmRewardVM by viewModel()
    private val viewModelclaim:ClaimRewardVM by viewModel()
    private val args: RewardFragmentArgs by navArgs()

    var isconfirm=0
    var reward_id=0


    var txttitle: TextView? = null
    var webview: WebView? = null

    var btnclose: TextView? = null





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_reward, container, false)
        setHasOptionsMenu(true)

        if(  DashboardFragment.instance.loadingIndicator.isShowing) {
            DashboardFragment.instance.loadingIndicator.dismiss()
        }

        loadingIndicator = getLoadingIndicator(requireContext())



        Log.d("xxx",args.from)

       if (args.from=="profile"){
           rewardlistuserwiseapi()
       }else{
           rewardlistapi()
       }


        return binding.root

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()
        setFragmentResultListener("refresh") { _, _ ->
            rewardlistapi()

        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun rewardlistapi() {
        val today = Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date",dateToStr)

        if (isNetworkConnected(requireContext())) {
            viewModel.getRewardList(
                GlobalClass.prefs.getValueString("token").toString(),"0",dateToStr
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.data.isEmpty()){
                                    binding.txtNorewrd.visibility=View.VISIBLE
                                    binding.rvReward.visibility=View.GONE
                                }else{
                                    binding.txtNorewrd.visibility=View.GONE
                                    binding.rvReward.visibility=View.VISIBLE

                                }

                                if (response.status) {


                                    if(response.data.size == 6){
                                        binding.tvFinSeemore.visibility=View.VISIBLE
                                    }else{
                                        binding.tvFinSeemore.visibility=View.GONE

                                    }

                                    binding.tvFinSeemore.setOnClickListener(){
                                        GlobalClass.setTitle="Reward"

                                        findNavController().navigate(MoreChoresFragmentDirections.actionGlobalNavMorechores("Rewards","",""))

                                    }

                                 binding.rvReward.adapter=RewardListAdapter(
                                            requireContext(),
                                            onItemClicked = {

                                                           findNavController().navigate(UpdateRewardFragmentDirections.actionGlobalDialogUpdateReward(
                                                              ""
                                                            ,"",it.rewardId.toString(),"","",""))

                                            },onclaimnowClicked = {


                                         if(it.brand_url==""){

                                             val dialog = AlertDialog.Builder(context)
                                                 .setTitle("Are you sure you want to claim this reward?")
                                                 .setPositiveButton(
                                                     "Ok"
                                                 ) { dialog, whichButton ->

                                                     reward_id=it.rewardId
                                                     callclaimapi()
                                                 }
                                                 .setNegativeButton("Cancel") { dialog, whichButton ->
                                                     dialog.dismiss()
                                                 }
                                                 .show()

                                         }else{
                                             val dialog = AlertDialog.Builder(context)
                                                 .setTitle("What do you want?")
                                                 .setPositiveButton(
                                                     "View Reward"
                                                 ) { dialog, whichButton ->

                                                     showWebViewDialog(it.brand_url)
                                                     dialog.dismiss()

                                                 }
                                                 .setNegativeButton("Claim Now") { dialog, whichButton ->

                                                     reward_id=it.rewardId
                                                     callclaimapi()
                                                 }
                                                 .show()
                                         }



                                     },onconfirmClicked = {


                                         confirmrewardapicall(it.rewardId,isconfirm)

                                     }, onDeleteItemClicked = {
                                                //call delete reward api id= it.choreId


                                         val dialog = AlertDialog.Builder(context)
                                             .setTitle("Delete Chore")
                                             .setTitle("Are you sure you want to delete this reward?")
                                             .setPositiveButton(
                                                 "Ok"
                                             ) { dialog, whichButton ->

                                                 viewModeldelete.deletereward(it.rewardId.toString()).observe(viewLifecycleOwner) { resource ->
                                                     when (resource.status) {
                                                         Status.SUCCESS -> {
                                                             loadingIndicator.dismiss()
                                                             resource.data.let { response ->
                                                                 if (response != null) {
                                                                     if (response.status) {
                                                                         Toast.makeText(
                                                                             requireContext(),
                                                                             response.message,
                                                                             Toast.LENGTH_SHORT
                                                                         ).show()

                                                                         setFragmentResult(
                                                                             "refresh",
                                                                             bundleOf("fromdeletereward" to true)
                                                                         )
                                                                         //findNavController().navigateUp()
                                                                     }
                                                                 }

                                                             }

                                                         }
                                                         Status.LOADING -> {
                                                             loadingIndicator.show()
                                                         }
                                                         Status.ERROR -> {
                                                             loadingIndicator.dismiss()
                                                         }
                                                     }

                                                 }
                                             }
                                             .setNegativeButton("Cancel") { dialog, whichButton ->
                                                 dialog.dismiss()
                                             }
                                             .show()



                                     },"",onlongItemClicked = {

                                     }).apply {
                                                rewardRespList=response.data.toMutableList()

                                        }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }



    @RequiresApi(Build.VERSION_CODES.M)
    private fun rewardlistuserwiseapi() {
        val today = Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date",dateToStr)

        if (isNetworkConnected(requireContext())) {
            viewModel.getRewardListuserwise(
               args.token,"0",dateToStr
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.data.isEmpty()){
                                    binding.txtNorewrd.visibility=View.VISIBLE
                                    binding.rvReward.visibility=View.GONE
                                }else{
                                    binding.txtNorewrd.visibility=View.GONE
                                    binding.rvReward.visibility=View.VISIBLE

                                }

                                if (response.status) {

                                    if(response.data.size == 6){
                                        binding.tvFinSeemore.visibility=View.VISIBLE
                                    }else{
                                        binding.tvFinSeemore.visibility=View.GONE

                                    }

                                    binding.tvFinSeemore.setOnClickListener(){
                                        GlobalClass.setTitle="Reward"

                                        findNavController().navigate(MoreChoresFragmentDirections.actionGlobalNavMorechores("Rewardsprofile","",args.token))

                                    }

                                    binding.rvReward.adapter=RewardListAdapter(
                                        requireContext(),
                                        onItemClicked = {



                                        },onclaimnowClicked = {



                                        },onconfirmClicked = {


                                           // confirmrewardapicall(it.rewardId,isconfirm)

                                        }, onDeleteItemClicked = {
                                            //call delete reward api id= it.choreId

                                        },"profile",onlongItemClicked = {

                                        }).apply {
                                        rewardRespList=response.data.toMutableList()

                                    }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }

    private fun callclaimapi() {
        val today = Date()
        val format = SimpleDateFormat("MMM-dd-yyyy hh:mm a")
        var dateToStr = format.format(today)
        val date_time=dateToStr
        viewModelclaim.claimreward(GlobalClass.prefs.getValueString("token").toString(),reward_id,date_time).observe(viewLifecycleOwner) { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    loadingIndicator.dismiss()
                    resource.data.let { response ->
                        if (response != null) {
                            if (response.status) {

                                Toast.makeText(
                                    requireContext(),
                                    response.message,
                                    Toast.LENGTH_SHORT
                                ).show()

                                setFragmentResult(
                                    "refresh",
                                    bundleOf("fromdeleteChore" to true)
                                )
                                findNavController().navigateUp()
                            }else{
                                Toast.makeText(
                                    requireContext(),
                                    response.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                    }

                }
                Status.LOADING -> {
                    loadingIndicator.show()
                }
                Status.ERROR -> {
                    loadingIndicator.dismiss()
                }
            }

        }

    }


    fun showWebViewDialog(url: String) {

        val viewGroup: ViewGroup? = requireView().findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(requireContext())
            .inflate(R.layout.webviewdialog, viewGroup, false)
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())


        //setting the view of the builder to our custom view that we already inflated

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it

        //finally creating the alert dialog and displaying it
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
        txttitle = dialogView.findViewById<TextView>(R.id.textviewtitle)
        btnclose = dialogView.findViewById(R.id.textviewclose)
        webview = dialogView.findViewById(R.id.webview)


        // editText.filters = arrayOf<InputFilter>(MinMaxFilter("1", "500"))
        // editTextpoints!!.setText(binding.txtpoints.text.toString())

        class MyJavaScriptInterface(private val contentView: TextView) {
            fun processContent(aContent: String) {
                contentView.post {
                    contentView.text = aContent
                    txttitle!!.setText(aContent)

                }
            }
        }


        val webViewClient: WebViewClient = object : WebViewClient() {

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(request?.url.toString())
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {

                super.onPageStarted(view, url, favicon)


            }

            override fun onPageFinished(view: WebView?, url: String?) {

                txttitle!!.text = view?.title
                loadingIndicator.dismiss()
                super.onPageFinished(view, url)
            }
        }
        webview!!.webViewClient = webViewClient
//             binding.webview.settings.defaultTextEncodingName = "utf-8"
        webview!!.settings.javaScriptEnabled = true
        webview!!.settings.javaScriptCanOpenWindowsAutomatically = true
        webview!!.settings.domStorageEnabled = true

        webview!!.loadUrl(url)



        webview!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action === KeyEvent.ACTION_DOWN) {
                    val webView = v as WebView
                    when (keyCode) {
                        KeyEvent.KEYCODE_BACK -> if (webView.canGoBack()) {
                            webView.goBack()
                            return true
                        }
                    }
                }
                return false
            }
        })

        dialogView.textviewclose.setOnClickListener() {
            alertDialog.dismiss()
        }


    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.item_add_family).isVisible = false
        menu.findItem(R.id.item_add_reward).isVisible=true


        menu.findItem(R.id.item_add_reward).setOnMenuItemClickListener {
            findNavController().navigate(AddNewRewardFragmentDirections.actionGlobalDialogNewReward("","",""))
            true
        }
        super.onPrepareOptionsMenu(menu)
    }


    fun confirmrewardapicall(rewardid:Int,isconfrim:Int){

        viewModelconfirm.confirmreward(rewardid.toString(),isconfrim).observe(viewLifecycleOwner) { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    loadingIndicator.dismiss()
                    resource.data.let { response ->
                        if (response != null) {
                            if (response.status) {
                                Toast.makeText(
                                    requireContext(),
                                    response.message,
                                    Toast.LENGTH_SHORT
                                ).show()

                                setFragmentResult(
                                    "refresh",
                                    bundleOf("fromdeleteChore" to true)
                                )
                               // findNavController().navigateUp()
                            }
                        }

                    }

                }
                Status.LOADING -> {
                    loadingIndicator.show()
                }
                Status.ERROR -> {
                    loadingIndicator.dismiss()
                }
            }

        }

    }









}
package com.app.familydays.fragments

import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.constraintlayout.motion.widget.TransitionBuilder.validate
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import br.com.ilhasoft.support.validation.Validator
import com.app.familydays.BuildConfig
import com.app.familydays.R
import com.app.familydays.databinding.ActivityContactBinding
import com.app.familydays.databinding.FragmentContactHelpBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.utils.showMediaPickerDialog
import com.app.familydays.viewmodels.ContactAdminVm
import com.kaopiz.kprogresshud.KProgressHUD
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.koin.android.ext.android.bind
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File

class ContactHelpFragment : Fragment() {

    private lateinit var binding: FragmentContactHelpBinding
    private val viewModel: ContactAdminVm by viewModel()
    private lateinit var loadingIndicator: KProgressHUD
    private lateinit var validator: Validator
    private var imageUri: Uri? = null

    private val takePhoto =
        registerForActivityResult(ActivityResultContracts.TakePicture())
        {
            if (it) {
                viewModel.setImageUri(imageUri.toString())
            }
        }
    private val pickImage = registerForActivityResult(ActivityResultContracts.GetContent()) {
        it?.let { uri ->
            imageUri = uri
            viewModel.setImageUri(imageUri.toString())
        }
    }
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_contact_help, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        validator = Validator(binding)
        loadingIndicator = getLoadingIndicator(requireContext())

        setHasOptionsMenu(true)

        viewModel.contactAdminReq.value?.full_name=GlobalClass.prefs.getValueString("username").toString()
        viewModel.contactAdminReq.value?.email=GlobalClass.prefs.getValueString("email").toString()
        viewModel.contactAdminReq.value?.phone=GlobalClass.prefs.getValueString("phone").toString()

        binding.tvUpload.setOnClickListener {
            showMediaPickerDialog(requireContext(), {
                imageUri = FileProvider.getUriForFile(
                    requireContext(), BuildConfig.APPLICATION_ID + ".provider",
                    File.createTempFile("image", ".jpg", requireContext().filesDir)
                )
                takePhoto.launch(imageUri)
            }, {
                pickImage.launch("image/*")
            })
        }


        binding.tvSend.setOnClickListener {

            if (binding.editName.text.isEmpty()){
                Toast.makeText(requireContext(), "Please enter name", Toast.LENGTH_SHORT).show()
            }else if (binding.edtPhNo.text.isEmpty()){
                Toast.makeText(requireContext(), "Please enter phone number", Toast.LENGTH_SHORT).show()
            }else if (binding.edtSubject.text.isEmpty()){
                Toast.makeText(requireContext(), "Please enter subject", Toast.LENGTH_SHORT).show()
            }else if (!binding.edtEmail.text.toString().trim().matches(Patterns.EMAIL_ADDRESS.toRegex())||binding.edtEmail.text.isEmpty()) {
                Toast.makeText(requireContext(), "Please enter valid e-mail", Toast.LENGTH_SHORT)
                    .show()
            }else if (binding.edtMessage.text.isEmpty()){
                Toast.makeText(requireContext(), "Please enter message", Toast.LENGTH_SHORT).show()
            }else{



                if (isNetworkConnected(requireContext())) {
                    val filePart: MultipartBody.Part? = imageUri?.let {
                        requireContext().contentResolver.openInputStream((it))?.use { inputStream ->
                            MultipartBody.Part.createFormData(
                                "media_file",
                                "image.jpg",
                                inputStream.readBytes().toRequestBody("image/jpeg".toMediaType())
                            )
                        }
                    }
                    viewModel.contactAdmin(
                        filePart
                    ).observe(
                        viewLifecycleOwner
                    ) { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {
                                loadingIndicator.dismiss()
                                resource.data?.let { response ->
                                    if (response.status) {

                                        Toast.makeText(
                                            requireContext(),
                                            "Your message was sent successfully.",
                                            Toast.LENGTH_LONG
                                        ).show()

                                        findNavController().navigateUp()

                                    } else {

                                        Toast.makeText(
                                           requireContext(),
                                            response.message,
                                            Toast.LENGTH_LONG
                                        ).show()

                                    }
                                    findNavController().navigateUp()

                                }

                            }
                            Status.ERROR -> {
                                loadingIndicator.dismiss()

                            }
                            Status.LOADING -> {
                                loadingIndicator.show()
                            }
                        }
                    }

                }
            }
        }


        return binding.root
    }
    override fun onPrepareOptionsMenu(menu: Menu) {

//        menu.findItem(R.id.item_profile).isVisible = false
        menu.findItem(R.id.item_add_family).isVisible = false
        menu.findItem(R.id.item_child_profile).isVisible = false

        super.onPrepareOptionsMenu(menu)
    }
    private fun validate(): Boolean {
        if (!validator.validate())
            return false

        return true
    }

}
package com.app.familydays.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.app.familydays.R
import com.app.familydays.adapters.*
import com.app.familydays.databinding.FragmentMoreChoresBinding
import com.app.familydays.global.AppConstant
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.utils.unAuthorisedUserDialog
import com.app.familydays.viewmodels.*
import com.app.familydays.webservice.responses.GetAssignedChoreResp
import com.app.familydays.webservice.responses.GetFinishedChoreResp
import com.app.familydays.webservice.responses.GetRewardResp
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.content_main2.view.*
import kotlinx.android.synthetic.main.webviewdialog.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import java.util.Date as Date1


class MoreChoresFragment : Fragment() {




    private val args: MoreChoresFragmentArgs by navArgs()
    private lateinit var binding: FragmentMoreChoresBinding
    private val viewModelget: EditProfileVm by viewModel()

    private val viewModel: AdminFinishedChoreListVm by viewModel()
    private val viewModel1: AdminAssignedChoreListVm by viewModel()
    private val viewModelRewardlist: RewardlistVM by viewModel()

    private val viewModelfilter: FilterVM by viewModel()

    private lateinit var loadingIndicator: KProgressHUD
    private val viewModeldelete: DeleteChoreVM by viewModel()
    var load_more: String="1"
    var to_date: String = ""
    var from_date: String = ""
    lateinit var status: String
    private val viewModel2: ChoreisCompleteVM by viewModel()
    private val viewModelclaim: ClaimRewardVM by viewModel()
    private val viewModeldeletereward: DeleteChoreVM by viewModel()
    private val viewModelconfirm: ConfirmRewardVM by viewModel()
    private val viewModelclaimlist: ClaimListVM by viewModel()
    private val viewmodelassignfinishedbychild: ChildMemberDetailVm by viewModel()

    private val viewmodelapprove: ApproveChoresVM by viewModel()

    //selection apis
    var arrrewardList = ArrayList<GetRewardResp.Data>()
    var arrassigneList = ArrayList<GetAssignedChoreResp.Data>()
    var arrfinishList = ArrayList<GetFinishedChoreResp.Data>()


    var fabtype = ""

    var ids: ArrayList<Int> = ArrayList()
    var selectedid: ArrayList<String> = ArrayList()


   // val selectedChild = StringBuilder()
    var selectedids = StringBuilder()
    var isconfirm = 0
    var reward_id = 0
    var fromfilter = ""
    var selectedChildId: ArrayList<Int> = ArrayList()
    private var collabPopupContainer: ViewGroup? = null
    private var collabPopupWindow: PopupWindow? = null


    private var fab_main: FloatingActionButton? = null
    private var fab_delete: FloatingActionButton? = null
    private var fab_approve: FloatingActionButton? = null
    private var fab_mark: FloatingActionButton? = null
    private var fab_open: Animation? = null
    private var fab_close: Animation? = null
    private var fab_clock: Animation? = null
    private var fab_anticlock: Animation? = null
    /* var textview_mail: TextView? = null
     var textview_share:TextView? = null*/
    lateinit var user_token:String
    lateinit var user_id:String

    lateinit var rewarlist: GetRewardResp


    var ishide = false


    var isOpen = false


    var txttitle: TextView? = null
    var webview: WebView? = null

    var btnclose: TextView? = null


    companion object {
        lateinit var instance: MoreChoresFragment
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_more_chores, container, false)
        getuser()
        loadingIndicator = getLoadingIndicator(requireContext())

        instance = this
        binding.args = args


        var fab = view?.findViewById<View>(R.id.icFab)

//        fab_main=binding.icFab.fab
//        fab_delete=binding.icFab.clDelete
//        fab_approve=binding.icFab.clApprove
//        fab_mark=binding.icFab.clMark

        fab_close = AnimationUtils.loadAnimation(requireContext(), R.anim.fab_close);
        fab_open = AnimationUtils.loadAnimation(requireContext(), R.anim.fab_open);
        fab_clock = AnimationUtils.loadAnimation(requireContext(), R.anim.fab_rotate_clock);
        fab_anticlock = AnimationUtils.loadAnimation(requireContext(), R.anim.fab_rotate_anticlock);


        getchild_id(GlobalClass.selectedChildId)


        setHasOptionsMenu(true)


        /*  collabPopupWindow = PopupWindow(
              collabPopupContainer,
              ViewGroup.LayoutParams.MATCH_PARENT,
              ViewGroup.LayoutParams.MATCH_PARENT,
              true
          )*/


        Log.e("xxx", args.from)

        /* binding.clmore.setOnLongClickListener{

             binding.icAdd.visibility=View.VISIBLE
            true
         }*/


//        if (args.from == "finishedfilter") {
//            binding.rvFinishedChores.visibility = View.VISIBLE
//            binding.rvAssignChoreschores.visibility = View.GONE
//            binding.rvrewards.visibility = View.GONE
//            binding.rvclaim.visibility = View.GONE
//            if (GlobalClass.prefs.getValueString("role_id") == "2" || GlobalClass.prefs.getValueString(
//                    "role_id"
//                ) == "3"
//            ) {
//                binding.rvFinishedChores.visibility = View.VISIBLE
//                binding.rvAssignChoreschores.visibility = View.GONE
//                binding.rvrewards.visibility = View.GONE
//                binding.rvclaim.visibility = View.GONE
//                load_more = "1"
//                status = "finished_chores"
//                getFinishedChoreListChild()
//                fromfilter = "Finished"
//            }
//        } else if (args.from == "assignedfilter") {
//            if (GlobalClass.prefs.getValueString("role_id") == "2" || GlobalClass.prefs.getValueString(
//                    "role_id"
//                ) == "3"
//            ) {
//                binding.rvAssignChoreschores.visibility = View.VISIBLE
//                binding.rvFinishedChores.visibility = View.GONE
//                binding.rvrewards.visibility = View.GONE
//                binding.rvclaim.visibility = View.GONE
//                load_more = "1"
//                status = "assign_chores"
//                getAssignedChoreListChild()
//                fromfilter = "Assigned"
//            }
//        } else if (args.from == "rewardfilter") {
//            binding.rvrewards.visibility = View.VISIBLE
//
//            binding.rvFinishedChores.visibility = View.GONE
//            binding.rvAssignChoreschores.visibility = View.GONE
//            binding.rvclaim.visibility = View.GONE
//
//            load_more = "1"
//            rewardlistapi()
//            fromfilter = "reward"
//        }


        from_date = GlobalClass.start_date
        to_date = GlobalClass.end_date
        Log.d("XXX", to_date)


        /*  if (from_date.isEmpty()&& GlobalClass.selectedChildId.size == 0||from_date.isEmpty()&&to_date.isEmpty()&& GlobalClass.selectedChildId.size == 0) {
             binding.txtfilter.visibility=View.GONE

          } else if (GlobalClass.selectedChildId.size>0 &&from_date.isEmpty()&&to_date.isEmpty()||GlobalClass.selectedChildId.size>0 &&from_date.isEmpty()) {
              binding.txtfilter.visibility=View.GONE
          }else if (GlobalClass.selectedChildId.size==0){
              binding.txtfilter.visibility=View.GONE
          }*/
        if (GlobalClass.clear == "true") {
            binding.txtfilter.visibility = View.GONE
        }

        if (args.filterby.isNotEmpty()) {
       /*     var prefix = ""
            for (str in GlobalClass.selectedchild.distinct()) {
                selectedChild.append(prefix)
                prefix = ","
                selectedChild.append(str)
            }*/

            var childlist = GlobalClass.selectedchild.distinct()

            val selectedChild=  childlist.toString().removePrefix("[").removeSuffix("]").filter { !it.isWhitespace() }

            Log.e(
                "selectedchild",
                childlist.toString().removePrefix("[").removeSuffix("]").filter { !it.isWhitespace() })

            if (GlobalClass.clear == "true") {
                binding.txtfilter.visibility = View.GONE
            } else if (args.filterby == "Filter by child") {
                binding.txtfilter.visibility = View.VISIBLE
                if (selectedChild.isNotEmpty()) {
                    binding.txtfilter.setText("Filter by Child:$selectedChild")
                } else {
                    binding.txtfilter.visibility = View.GONE
                }

                //binding.txtfilter
            } else if (args.filterby == "date") {
                binding.txtfilter.visibility = View.VISIBLE
                if (GlobalClass.clear == "true") {
                    binding.txtfilter.visibility = View.GONE
                } else if (from_date.isNotEmpty() && to_date.isEmpty()) {
                    binding.txtfilter.setText("Filter by date:" + GlobalClass.start_date)
                } else if (from_date.isNotEmpty() && to_date.isNotEmpty() || from_date.isNotEmpty() && to_date.isNotEmpty() && GlobalClass.selectedChildId.size > 0) {
                    binding.txtfilter.setText("Filter by date:" + GlobalClass.start_date + " to " + GlobalClass.end_date)
                } else {
                    binding.txtfilter.visibility = View.GONE
                }
            } else {
                binding.txtfilter.visibility = View.VISIBLE
                if (from_date.isNotEmpty() && to_date.isEmpty()&&GlobalClass.selectedChildId.size > 0) {
                    binding.txtfilter.text = "Filter by date:" + GlobalClass.start_date+"\n"+"Filter by Child:"+selectedChild
                } else{
                    binding.txtfilter.text =
                        "Filter by date:$from_date to $to_date\nFilter by Child:$selectedChild"
                }
             /*   binding.txtfilter.text =
                    "Filter by date:$from_date to $to_date\nFilter by Child:$selectedChild"*/
            }
        }


        //     if (GlobalClass.prefs.getValueString("role_id")=="2"||GlobalClass.prefs.getValueString("role_id")=="3"){
        if (args.from == "Assigned"||args.from == "assignchildren") {
            binding.rvAssignChoreschores.visibility = View.VISIBLE

            binding.rvFinishedChores.visibility = View.GONE
            binding.rvrewards.visibility = View.GONE
            binding.rvclaim.visibility = View.GONE
            load_more = "1"
            getAssignedChoreList()
            fromfilter = "Assigned"

        } else if (args.from == "Finished") {
            binding.rvFinishedChores.visibility = View.VISIBLE
            binding.rvAssignChoreschores.visibility = View.GONE
            binding.rvrewards.visibility = View.GONE
            binding.rvclaim.visibility = View.GONE
            load_more = "1"
            getFinishedChoreList()
            fromfilter = "Finished"


        } else if (args.from == "Claims") {

            load_more = "1"
            binding.rvclaim.visibility = View.VISIBLE

            binding.rvFinishedChores.visibility = View.GONE
            binding.rvAssignChoreschores.visibility = View.GONE
            binding.rvrewards.visibility = View.GONE
            fromfilter = "Claims"
            claimlistapi()
            from_date = "claims"
        } else if (args.from == "Rewards") {
            binding.rvrewards.visibility = View.VISIBLE

            binding.rvFinishedChores.visibility = View.GONE
            binding.rvAssignChoreschores.visibility = View.GONE
            binding.rvclaim.visibility = View.GONE

            load_more = "1"
            status = "Reward"
            rewardlistapi()
            fromfilter = "Rewards"

        } else if (args.from == "finishedfilter") {

            binding.rvFinishedChores.visibility = View.VISIBLE
            fromfilter="Finished"
            binding.rvAssignChoreschores.visibility = View.GONE
            binding.rvrewards.visibility = View.GONE
            binding.rvclaim.visibility = View.GONE
            if (GlobalClass.clearfilter==1){
                load_more="1"
                getFinishedChoreList()
            }else{
                filterfinishedchoreapi()
            }




        } else if (args.from == "assignedfilter") {

            Log.e("startXXX", GlobalClass.start_date)

            fromfilter="Assigned"
            binding.rvAssignChoreschores.visibility = View.VISIBLE
            binding.rvFinishedChores.visibility = View.GONE
            binding.rvrewards.visibility = View.GONE
            binding.rvclaim.visibility = View.GONE

            if (GlobalClass.clearfilter==1 ){
                getAssignedChoreList()
            }else{
                filterassignedchoreapi()
            }



        } else if (args.from == "rewardfilter") {
            binding.rvrewards.visibility = View.VISIBLE
            fromfilter = "Rewards"
            binding.rvFinishedChores.visibility = View.GONE
            binding.rvAssignChoreschores.visibility = View.GONE

            binding.rvclaim.visibility = View.GONE

            if (GlobalClass.clearfilter==1){
                load_more="1"
                rewardlistapi()
            }else{
                filterrewardlistapi()
            }
        } else if (args.from == "Rewardsprofile") {
            binding.rvrewards.visibility = View.VISIBLE

            binding.rvFinishedChores.visibility = View.GONE
            binding.rvAssignChoreschores.visibility = View.GONE
            binding.rvclaim.visibility = View.GONE
            fromfilter="rewardchild"
            rewardlistuserwiseapi()


        } else if (args.from == "claimfilter") {
            binding.rvclaim.visibility = View.VISIBLE
            fromfilter = "Claims"
            binding.rvFinishedChores.visibility = View.GONE
            binding.rvAssignChoreschores.visibility = View.GONE
            binding.rvrewards.visibility = View.GONE
            if (GlobalClass.clearfilter==1){
                load_more="1"
                claimlistapi()
            }else{
                filterclaimlistapi()
            }

        } else if (args.from == "assignchild") {
            binding.rvAssignChoreschores.visibility = View.VISIBLE
            binding.rvFinishedChores.visibility = View.GONE
            binding.rvrewards.visibility = View.GONE
            binding.rvclaim.visibility = View.GONE
           // load_more = "0"
            status="assigned_chores"
            getAssigned_finishedChoreListChild()

            fromfilter = "assignchild"

        } else if (args.from == "finishchild") {
            binding.rvFinishedChores.visibility = View.VISIBLE
            binding.rvAssignChoreschores.visibility = View.GONE
            binding.rvrewards.visibility = View.GONE
            binding.rvclaim.visibility = View.GONE
            load_more = "1"
            status="finished_chores"
            getAssigned_finishedChoreListChild()
            fromfilter = "finishchild"


        }else if(args.from=="childclaims"){
            binding.rvclaim.visibility = View.VISIBLE
            fromfilter = "childclaim"
            binding.rvFinishedChores.visibility = View.GONE
            binding.rvAssignChoreschores.visibility = View.GONE
            binding.rvrewards.visibility = View.GONE
            claimlistuserwise()

        }else if(args.from=="assignchildfilter"){
            binding.rvAssignChoreschores.visibility = View.VISIBLE
            binding.rvFinishedChores.visibility = View.GONE
            binding.rvrewards.visibility = View.GONE
            binding.rvclaim.visibility = View.GONE
         //   load_more = "1"
            status="assigned_chores"

            if (GlobalClass.clearfilter==1){
                //  load_more="1"
                getAssigned_finishedChoreListChild()
                fromfilter = "assignchild"
            }else{
                //getAssigned_finishedChoreListChild()
                Log.e("startXXX", GlobalClass.start_date)
                fromfilter = "assignchild"
                filterassignedchoreapi()
            }


      //      getFinishedChoreListChild()
        }else if(args.from=="finishchildfilter"){
            binding.rvAssignChoreschores.visibility = View.GONE
            binding.rvFinishedChores.visibility = View.VISIBLE
            binding.rvrewards.visibility = View.GONE
            binding.rvclaim.visibility = View.GONE
            load_more = "1"
            status="finished_chores"
            if (GlobalClass.clearfilter==1){
                //  load_more="1"
                getAssigned_finishedChoreListChild()
                fromfilter = "finishchild"
            }else{
                //getAssigned_finishedChoreListChild()
                Log.e("startXXX", GlobalClass.start_date)
                fromfilter = "finishchildfilter"
                filterfinishedchoreapi()
            }

        }else if (args.from=="rewardfilterchild"){
            binding.rvrewards.visibility = View.VISIBLE

            binding.rvFinishedChores.visibility = View.GONE
            binding.rvAssignChoreschores.visibility = View.GONE
            binding.rvclaim.visibility = View.GONE
            fromfilter="rewardchild"

            if (GlobalClass.clearfilter==1){
                //  load_more="1"
                rewardlistuserwiseapi()

            }else{
                //getAssigned_finishedChoreListChild()
                Log.e("startXXX", GlobalClass.start_date)

                filterrewardlistapi()
            }


        }else if(args.from=="childclaimfilter"){
            binding.rvclaim.visibility = View.VISIBLE
            fromfilter = "childclaim"
            binding.rvFinishedChores.visibility = View.GONE
            binding.rvAssignChoreschores.visibility = View.GONE
            binding.rvrewards.visibility = View.GONE
            if (GlobalClass.clearfilter==1){
                //  load_more="1"
                claimlistuserwise()

            }else{
                //getAssigned_finishedChoreListChild()
                Log.e("startXXX", GlobalClass.start_date)
                filterclaimlistapi()
            }

        }

        /*   }else{
              if (args.from=="Assigned"){
                  binding.rvAssignChoreschores.visibility=View.VISIBLE
                  load_more="0"
                  status="assign_chores"
                  getAssignedChoreListChild()
                  fromfilter="Assigned"
              }else if(args.from=="Finished"){
                  binding.rvFinishedChores.visibility=View.VISIBLE
                  load_more="1"
                  status="finished_chores"
                  getFinishedChoreListChild()
                  fromfilter="Finished"
              }else if (args.from=="Rewards"){
                  binding.rvrewards.visibility=View.VISIBLE
                  load_more="1"
                  rewardlistapi()
                  fromfilter="reward"
              }else if (args.from=="finishedfilter"){
                   binding.rvFinishedChores.visibility=View.VISIBLE
                   filterfinishedchoreapi()
               }

               else if (args.from=="assignedfilter"){

                  binding.rvAssignChoreschores.visibility=View.VISIBLE
                   filterassignedchoreapi()
               }
               else if (args.from=="rewardfilter"){

                  binding.rvrewards.visibility=View.VISIBLE
                   filterrewardlistapi()
               }




           }*/







        return binding.root


    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun filterclaimlistapi() {


        GlobalClass.setTitle = "Claims"
        lateinit var childid:String
        if (args.from=="childclaimfilter"){
            if (args.token==GlobalClass.prefs.getValueString("token")){
                childid= GlobalClass.prefs.getValueString("id").toString()
            }else{
                childid= GlobalClass.prefs.getValueString("user_id").toString()
            }

        }else{
            childid=getchild_id(GlobalClass.selectedChildId)
        }


        load_more = "0"

        if (isNetworkConnected(requireContext())) {
            viewModelfilter.getckaimfilterlist(
                GlobalClass.prefs.getValueString("token").toString(),
                from_date,
                to_date,
               childid
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    binding.rvclaim.adapter =
                                        ClaimListAdapter(requireContext(), onItemClicked = {


                                        }).apply {
                                            claimRespList = response.data
                                        }


                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }


    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun claimlistapi() {


        GlobalClass.setTitle = "Claims"


        val today = Date1()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date", dateToStr)

        if (isNetworkConnected(requireContext())) {
            viewModelclaimlist.getClaimsList(
                GlobalClass.prefs.getValueString("token").toString(), load_more, dateToStr,""
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->


                                binding.rvclaim.adapter = ClaimListAdapter(
                                    requireContext(),
                                    onItemClicked = {
                                        if (it.brand_url != "") {
                                            showWebViewDialog(it.brand_url)
                                        }
                                    }).apply {
                                    claimRespList = response.data
                                }

                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun claimlistuserwise() {
        val today = java.util.Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date",dateToStr)
        lateinit var token:String



        if (isNetworkConnected(requireContext())) {
            viewModelclaimlist.getClaimsListuserwise(
                user_token,"1",dateToStr
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->

                                if (response.data.isEmpty()){
                                    binding.txtNoclaim.visibility=View.VISIBLE
                                    binding.rvclaim.visibility=View.GONE
                                }else{
                                    binding.txtNoclaim.visibility=View.GONE
                                    binding.rvclaim.visibility=View.VISIBLE

                                }



                                binding.rvclaim.adapter=ClaimListAdapter(requireContext(),onItemClicked = {
                                    if(it.brand_url != ""){
                                        showWebViewDialog(it.brand_url)
                                    }

                                }).apply {
                                    claimRespList=response.data
                                }

                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }

    fun getchild_id(id: ArrayList<Int>): String {

//        val selectedChild = StringBuilder()
//        var prefix = ""
//        for (str in id.distinct()) {
//            selectedChild.append(prefix)
//            prefix = ","
//            selectedChild.append(str)
//        }
//        return selectedChild.toString()

        var idslist = id.distinct()

        Log.e(
            "idList",
            idslist.toString().removePrefix("[").removeSuffix("]").filter { !it.isWhitespace() })


        return idslist.toString().removePrefix("[").removeSuffix("]").filter { !it.isWhitespace() }

    }


    override fun onPrepareOptionsMenu(menu: Menu) {


        menu.findItem(R.id.item_filter).isVisible = true
        Log.d("fromxxxx",fromfilter)

        menu.findItem(R.id.item_filter).setOnMenuItemClickListener {


            GlobalClass.start_date=""
            GlobalClass.end_date=""
            if (GlobalClass.clearfilter==1){
                GlobalClass.clearfilter=0
            }
            Log.d("xxxxnew",fromfilter)

            findNavController().navigate(FilterFragmentDirections.actionGlobalNavFilter(fromfilter))

            true
        }



        super.onPrepareOptionsMenu(menu)


    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun showfab() {

        binding.icFab.clmainfab.visibility = View.VISIBLE



        binding.icFab.clApprove.visibility = View.GONE



        binding.icFab.clDelete.visibility = View.GONE


        binding.icFab.clMark.visibility = View.GONE



        binding.icFab.clCancel.visibility = View.GONE


        selectedids = java.lang.StringBuilder()







        binding.icFab.fabadd.setOnClickListener() {

//            Log.e("selection", "${ viewModelRewardlist.selectedRewards.size}")
            if (isOpen) {


                binding.icFab.clDelete.visibility = View.GONE
                binding.icFab.clApprove.visibility = View.GONE
                binding.icFab.clMark.visibility = View.GONE
                binding.icFab.clCancel.visibility = View.GONE


                binding.icFab.clApprove.fbApprove.startAnimation(fab_close)
                binding.icFab.clMark.fbMark.startAnimation(fab_close)
                binding.icFab.clDelete.fbDelete.startAnimation(fab_close)
                binding.icFab.clCancel.fbCancel.startAnimation(fab_close)
                binding.icFab.fabadd.startAnimation(fab_anticlock)



                binding.icFab.clApprove.isClickable = false
                binding.icFab.clMark.isClickable = false
                binding.icFab.clDelete.isClickable = false
                binding.icFab.clCancel.isClickable = false

                isOpen = false
            } else {


                if (fabtype == "assigned") {

                    binding.icFab.clApprove.visibility = View.VISIBLE


                    binding.icFab.clDelete.visibility = View.VISIBLE


                    binding.icFab.clMark.visibility = View.VISIBLE


                    binding.icFab.clCancel.visibility = View.VISIBLE

                } else if (fabtype == "finished") {

                    binding.icFab.clApprove.visibility = View.VISIBLE

                    binding.icFab.clMark.visibility = View.GONE

                    binding.icFab.clDelete.visibility = View.GONE


                    binding.icFab.clCancel.visibility = View.VISIBLE


                } else if (fabtype == "reward") {
                    binding.icFab.clApprove.visibility = View.VISIBLE

                    binding.icFab.clMark.visibility = View.GONE

                    binding.icFab.clDelete.visibility = View.VISIBLE

                    binding.icFab.clCancel.visibility = View.VISIBLE

                }




                binding.icFab.clApprove.fbApprove.startAnimation(fab_open)
                binding.icFab.clMark.fbMark.startAnimation(fab_open)
                binding.icFab.clDelete.fbDelete.startAnimation(fab_open)
                binding.icFab.clCancel.fbCancel.startAnimation(fab_open)
                binding.icFab.fabadd.startAnimation(fab_clock)

                binding.icFab.clApprove.isClickable = true;
                binding.icFab.clMark.isClickable = true
                binding.icFab.clDelete.isClickable = true
                binding.icFab.clMark.isClickable = true

                binding.icFab.clCancel.isClickable = true



                isOpen = true;
            }
        }


        binding.icFab.clCancel.fbCancel.setOnClickListener() {


            binding.icFab.clDelete.visibility = View.GONE
            binding.icFab.clApprove.visibility = View.GONE
            binding.icFab.clCancel.visibility = View.GONE
            binding.icFab.clMark.visibility = View.GONE


            binding.icFab.clApprove.fbApprove.startAnimation(fab_close)
            binding.icFab.clMark.fbMark.startAnimation(fab_close)
            binding.icFab.clDelete.fbDelete.startAnimation(fab_close)
            binding.icFab.clCancel.fbCancel.startAnimation(fab_close)
            binding.icFab.fabadd.startAnimation(fab_anticlock)

            binding.icFab.clApprove.isClickable = false;
            binding.icFab.clMark.isClickable = false;
            binding.icFab.clDelete.isClickable = false;
            binding.icFab.clMark.isClickable = false;
            binding.icFab.clCancel.isClickable = false;


            GlobalClass.alSelectedPositionsreward.clear()


            if (fabtype == "assigned") {
                for (i in 0 until arrassigneList.size) {
                    arrassigneList[i].isChecked = false
                }
                binding.rvAssignChoreschores.adapter!!.notifyDataSetChanged()

            } else if (fabtype == "finished") {

                for (i in 0 until arrfinishList.size) {
                    arrfinishList[i].isChecked = false
                }
                binding.rvFinishedChores.adapter!!.notifyDataSetChanged()

            } else if (fabtype == "reward") {

                for (i in 0 until arrrewardList.size) {
                    arrrewardList[i].isChecked = false
                }
                binding.rvrewards.adapter!!.notifyDataSetChanged()
            }

            isOpen = false;
        }

        binding.icFab.clApprove.fbApprove.setOnClickListener() {

            var prefix = ""
            for (str in GlobalClass.alSelectedPositionsreward.distinct()) {
                selectedids.append(prefix)
                prefix = ","
                selectedids.append(str)
            }

            Log.d("stringid", selectedids.toString())

            if (fabtype == "assigned") {

                /* for (i in 0 until GlobalClass.alSelectedPositionsreward.size) {
                     Log.e(
                         "idsSend",
                         GlobalClass.alSelectedPositionsreward[i].toString()

                 }*/
                //   Toast.makeText(requireContext(), "Approve Assigned", Toast.LENGTH_SHORT).show()

                viewmodelapprove.chore_approve(
                    GlobalClass.prefs.getValueString("token").toString(),
                    selectedids.toString()
                )
                    .observe(viewLifecycleOwner) { resources ->
                        when (resources.status) {
                            Status.SUCCESS -> {
                                loadingIndicator.dismiss()
                                resources.data.let { response ->

                                    if (response != null) {
                                        if (response.status) {
                                            Toast.makeText(
                                                requireContext(),
                                                response.message,
                                                Toast.LENGTH_SHORT
                                            ).show()

//                                    getFinishedChoreList()
                                        } else {
                                            Toast.makeText(
                                                requireContext(),
                                                response.message,
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }
                                        setFragmentResult(
                                            "refresh",
                                            bundleOf("fromdeleteChore" to true)
                                        )

                                        GlobalClass.selectepos = -1
                                        GlobalClass.isselected = 0
                                        GlobalClass.selectedChildId = ArrayList()
                                    }
                                }

                            }
                            Status.ERROR -> {
                                loadingIndicator.dismiss()

                            }
                            Status.LOADING -> {
                                loadingIndicator.show()
                            }
                        }


                    }


            } else if (fabtype == "finished") {
                for (i in 0 until GlobalClass.alSelectedPositionsreward.size) {
                    Log.e(
                        "idsSend",
                        GlobalClass.alSelectedPositionsreward[i].toString()
                    )
                }

                viewmodelapprove.chore_approve(
                    GlobalClass.prefs.getValueString("token").toString(),
                    selectedids.toString()
                )
                    .observe(viewLifecycleOwner) { resources ->
                        when (resources.status) {
                            Status.SUCCESS -> {
                                loadingIndicator.dismiss()
                                resources.data.let { response ->

                                    if (response != null) {
                                        if (response.status) {
                                            Toast.makeText(
                                                requireContext(),
                                                response.message,
                                                Toast.LENGTH_SHORT
                                            ).show()

                                            GlobalClass.selectepos = -1
                                            GlobalClass.isselected = 0
                                            GlobalClass.selectedChildId = ArrayList()


                                            getFinishedChoreList()
                                        } else {
                                            Toast.makeText(
                                                requireContext(),
                                                response.message,
                                                Toast.LENGTH_SHORT
                                            ).show()


                                        }


                                    }
                                }

                            }
                            Status.ERROR -> {
                                loadingIndicator.dismiss()

                            }
                            Status.LOADING -> {
                                loadingIndicator.show()
                            }
                        }


                    }

                // Toast.makeText(requireContext(), "Approve Finished", Toast.LENGTH_SHORT).show()


            } else if (fabtype == "reward") {
                for (i in 0 until GlobalClass.alSelectedPositionsreward.size) {
                    Log.e(
                        "idsSend",
                        GlobalClass.alSelectedPositionsreward[i].toString()
                    )
                }

                confirmrewardapicall(selectedids.toString(), 0)

                //Toast.makeText(requireContext(), "Approve Delete", Toast.LENGTH_SHORT).show()
            }


        }


        binding.icFab.clMark.fbMark.setOnClickListener() {

            var prefix = ""
            for (str in GlobalClass.alSelectedPositionsreward.distinct()) {
                selectedids.append(prefix)
                prefix = ","
                selectedids.append(str)
            }


            if (fabtype == "assigned") {


                /* for (i in 0 until GlobalClass.alSelectedPositionsreward.size) {
                     Log.e(
                         "idsSend",
                         GlobalClass.alSelectedPositionsreward[i].toString()
                     )
                 }*/

                iscompletedApiCall("0", selectedids.toString())


            } else if (fabtype == "finished") {
                iscompletedApiCall("0", selectedids.toString())


            } else if (fabtype == "reward") {

                for (i in 0 until GlobalClass.alSelectedPositionsreward.size) {
                    Log.e(
                        "idsSend",
                        GlobalClass.alSelectedPositionsreward[i].toString()
                    )
                }
            }

            //  confirmrewardapicall(selectedids.toString(), isconfirm)

        }

        binding.icFab.clDelete.fbDelete.setOnClickListener() {

            var prefix = ""
            for (str in GlobalClass.alSelectedPositionsreward.distinct()) {
                selectedids.append(prefix)
                prefix = ","
                selectedids.append(str)
            }

            if (fabtype == "assigned") {

                for (i in 0 until GlobalClass.alSelectedPositionsreward.size) {
                    Log.e(
                        "idsSend",
                        GlobalClass.alSelectedPositionsreward[i].toString()
                    )
                }
                //Toast.makeText(requireContext(), "delete assigned", Toast.LENGTH_SHORT).show()
                androidx.appcompat.app.AlertDialog.Builder(
                    requireContext()
                )
                    .setTitle("Delete Chore")
                    .setTitle("Are you sure you want to delete this chore?")
                    .setCancelable(false)
                    .setPositiveButton(
                        "Ok"
                    ) { dialog, whichButton ->

                        viewModeldelete.deletechore(selectedids.toString())
                            .observe(viewLifecycleOwner) { resource ->
                                when (resource.status) {
                                    Status.SUCCESS -> {
                                        loadingIndicator.dismiss()
                                        resource.data.let { response ->
                                            if (response != null) {
                                                if (response.status) {
                                                    Toast.makeText(
                                                        requireContext(),
                                                        response.message,
                                                        Toast.LENGTH_SHORT
                                                    ).show()

                                                    setFragmentResult(
                                                        "refresh",
                                                        bundleOf("fromdeleteChore" to true)
                                                    )
                                                    findNavController().navigateUp()
                                                }
                                            }

                                        }

                                    }
                                    Status.LOADING -> {
                                        loadingIndicator.show()
                                    }
                                    Status.ERROR -> {
                                        loadingIndicator.dismiss()
                                    }
                                }

                            }
                    }
                    .setNegativeButton("Cancel") { dialog, whichButton ->
                        dialog.dismiss()
                    }
                    .show()

            } else if (fabtype == "finished") {

                for (i in 0 until GlobalClass.alSelectedPositionsreward.size) {
                    Log.e(
                        "idsSend",
                        GlobalClass.alSelectedPositionsreward[i].toString()
                    )
                }


            } else if (fabtype == "reward") {

                for (i in 0 until GlobalClass.alSelectedPositionsreward.size) {
                    Log.e(
                        "idsSend",
                        GlobalClass.alSelectedPositionsreward[i].toString()
                    )
                }


                androidx.appcompat.app.AlertDialog.Builder(requireContext())
                    .setTitle("Delete Chore")
                    .setTitle("Are you sure you want to delete this reward?")
                    .setCancelable(false)
                    .setPositiveButton(
                        "Ok"
                    ) { dialog, whichButton ->

                        viewModeldeletereward.deletereward(selectedids.toString()).observe(
                            viewLifecycleOwner
                        ) { resource ->
                            when (resource.status) {
                                Status.SUCCESS -> {
                                    loadingIndicator.dismiss()
                                    resource.data.let { response ->
                                        if (response != null) {
                                            if (response.status) {
                                                Toast.makeText(
                                                    requireContext(),
                                                    response.message,
                                                    Toast.LENGTH_SHORT
                                                ).show()

                                                setFragmentResult(
                                                    "refresh",
                                                    bundleOf("fromdeletereward" to true)
                                                )


                                                //findNavController().navigateUp()
                                            }
                                        }

                                    }

                                }
                                Status.LOADING -> {
                                    loadingIndicator.show()
                                }
                                Status.ERROR -> {
                                    loadingIndicator.dismiss()
                                }
                            }

                        }
                    }
                    .setNegativeButton("Cancel") { dialog, whichButton ->
                        dialog.dismiss()
                    }
                    .show()

                // Log.d("xxx",selectedids.toString())

                //Toast.makeText(requireContext(), "delete reward", Toast.LENGTH_SHORT).show()
            }

            //  confirmrewardapicall(selectedids.toString(), isconfirm)

        }


    }

    fun showWebViewDialog(url: String) {

        val viewGroup: ViewGroup? = requireView().findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(requireContext())
            .inflate(R.layout.webviewdialog, viewGroup, false)
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())


        //setting the view of the builder to our custom view that we already inflated

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it

        //finally creating the alert dialog and displaying it
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
        txttitle = dialogView.findViewById<TextView>(R.id.textviewtitle)
        btnclose = dialogView.findViewById(R.id.textviewclose)
        webview = dialogView.findViewById(R.id.webview)


        // editText.filters = arrayOf<InputFilter>(MinMaxFilter("1", "500"))
        // editTextpoints!!.setText(binding.txtpoints.text.toString())

        class MyJavaScriptInterface(private val contentView: TextView) {
            fun processContent(aContent: String) {
                contentView.post {
                    contentView.text = aContent
                    txttitle!!.setText(aContent)

                }
            }
        }


        val webViewClient: WebViewClient = object : WebViewClient() {

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(request?.url.toString())
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {

                super.onPageStarted(view, url, favicon)


            }

            override fun onPageFinished(view: WebView?, url: String?) {

                txttitle!!.text = view?.title
                loadingIndicator.dismiss()
                super.onPageFinished(view, url)
            }
        }
        webview!!.webViewClient = webViewClient
//             binding.webview.settings.defaultTextEncodingName = "utf-8"
        webview!!.settings.javaScriptEnabled = true
        webview!!.settings.javaScriptCanOpenWindowsAutomatically = true
        webview!!.settings.domStorageEnabled = true

        webview!!.loadUrl(url)



        webview!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action === KeyEvent.ACTION_DOWN) {
                    val webView = v as WebView
                    when (keyCode) {
                        KeyEvent.KEYCODE_BACK -> if (webView.canGoBack()) {
                            webView.goBack()
                            return true
                        }
                    }
                }
                return false
            }
        })

        dialogView.textviewclose.setOnClickListener() {
            alertDialog.dismiss()
        }


    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getAssignedChoreList() {
//        "date_time": 2021-08-11 18:54:53



        val today = Date1()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date", dateToStr)

        if (isNetworkConnected(requireContext())) {
            viewModel1.getAdminAssignedChoreList(
                GlobalClass.prefs.getValueString("token").toString(), load_more, dateToStr
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {


                                    arrassigneList.clear()
                                    arrassigneList.addAll(response.data)




                                    binding.rvAssignChoreschores.adapter =
                                        AssignedChoreAdapter(
                                            requireContext(),
                                            onItemClicked = {

                                                if (fabtype == "assigned") {
                                                    it.isChecked = !it.isChecked

                                                    binding.rvAssignChoreschores.adapter!!.notifyDataSetChanged()

                                                } else {
                                                    findNavController().navigate(
                                                        UpdateChoresFragmentDirections.actionGlobalDialogUpdateChore(
                                                            it.choreId.toString(),
                                                            it.iconUrl,
                                                            it.token,"",""
                                                            ,"","")
                                                    )
                                                }
                                            }, onDeleteItemClicked = {
                                                //call delete chore api id= it.choreId
                                                androidx.appcompat.app.AlertDialog.Builder(
                                                    requireContext()
                                                )
                                                    .setTitle("Delete Chore")
                                                    .setTitle("Are you sure you want to delete this chore?")
                                                    .setCancelable(false)
                                                    .setPositiveButton(
                                                        "Ok"
                                                    ) { dialog, whichButton ->

                                                        viewModeldelete.deletechore(it.choreId.toString())
                                                            .observe(viewLifecycleOwner) { resource ->
                                                                when (resource.status) {
                                                                    Status.SUCCESS -> {
                                                                        loadingIndicator.dismiss()
                                                                        resource.data.let { response ->
                                                                            if (response != null) {
                                                                                if (response.status) {
                                                                                    Toast.makeText(
                                                                                        requireContext(),
                                                                                        response.message,
                                                                                        Toast.LENGTH_SHORT
                                                                                    ).show()

                                                                                    setFragmentResult(
                                                                                        "refresh",
                                                                                        bundleOf("fromdeleteChore" to true)
                                                                                    )

                                                                                }
                                                                            }

                                                                        }

                                                                    }
                                                                    Status.LOADING -> {
                                                                        loadingIndicator.show()
                                                                    }
                                                                    Status.ERROR -> {
                                                                        loadingIndicator.dismiss()
                                                                    }
                                                                }

                                                            }
                                                    }
                                                    .setNegativeButton("Cancel") { dialog, whichButton ->
                                                        dialog.dismiss()
                                                    }
                                                    .show()


                                            }, onlongItemClicked = {
                                                fabtype = "assigned"
                                                if (GlobalClass.prefs.getValueString("role_id") == "2" || GlobalClass.prefs.getValueString(
                                                        "role_id"
                                                    ) == "3"
                                                ) {
                                                    showfab()
                                                } else {
                                                    //do nothing
                                                }

                                            }).apply {
                                            choreRespList = arrassigneList
                                        }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getAssigned_finishedChoreListChild() {
//        "date_time": 2021-08-11 18:54:53

        val today = Date1()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date", dateToStr)


        if (isNetworkConnected(requireContext())) {
            viewModel1.getChildAssignedChoreList(
                GlobalClass.prefs.getValueString("token").toString(),
                "1",
                to_date,
                from_date,
                status,
            dateToStr
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                   Log.d("ZZZZ",args.from)

                                    binding.rvAssignChoreschores.adapter =
                                        AssignChoresChildAdapter(
                                            requireContext(),
                                            onItemClicked = {

                                                findNavController().navigate(
                                                    UpdateChoresFragmentDirections.actionGlobalDialogUpdateChore(
                                                        it.chore_id.toString(),
                                                        it.icon_url,
                                                        it.token, "", "", ""
                                                        ,"")
                                                )


                                            }).apply {
                                            choreRespList = response.assign_chore
                                        }
                                    binding.rvFinishedChores.adapter =
                                        FinishedchorechildAdapter(requireContext(),onItemClicked = {

                                        }).apply {
                                            choreRespList = response.finish_chore
                                        }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }




    @RequiresApi(Build.VERSION_CODES.M)
 /*   private fun get_finishedChoreListChild() {
//        "date_time": 2021-08-11 18:54:53

        val today = Date1()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date", dateToStr)


        if (isNetworkConnected(requireContext())) {
            viewModel1.getChildAssignedChoreList(
                GlobalClass.prefs.getValueString("token").toString(),
                "1",
                to_date,
                from_date,
                status,
                dateToStr
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    Log.d("ZZZZ",args.from)

                                    binding.rvAssignChoreschores.adapter =
                                        AssignChoresChildAdapter(
                                            requireContext(),
                                            onItemClicked = {

                                                findNavController().navigate(
                                                    UpdateChoresFragmentDirections.actionGlobalDialogUpdateChore(
                                                        it.chore_id.toString(),
                                                        it.icon_url,
                                                        it.token, "", "", ""
                                                        ,"")
                                                )


                                            }).apply {
                                            choreRespList = response.assign_chore
                                        }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }*/

    private fun rewardlistapi() {
        val today = Date1()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date", dateToStr)



        if (isNetworkConnected(requireContext())) {
            viewModelRewardlist.getRewardList(
                GlobalClass.prefs.getValueString("token").toString(), load_more, dateToStr
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->

                                if (response.status) {


                                    arrrewardList.clear()

                                    arrrewardList.addAll(response.data)



                                    for (element in response.data) {
                                        ids.add(element.rewardId)
                                    }

                                    var prefix = ""
                                    for (str in ids.distinct()) {
                                        selectedids.append(prefix)
                                        prefix = ","
                                        selectedids.append(str)
                                    }

                                    Log.d("selected reward", selectedids.toString())

                                    binding.rvrewards.adapter = RewardListAdapter(
                                        requireContext(),
                                        onItemClicked = {

                                            if (fabtype == "reward") {
                                                if (fabtype == "reward") {

                                                    it.isChecked = !it.isChecked

                                                    binding.rvrewards.adapter!!.notifyDataSetChanged()
                                                } else {

                                                    findNavController().navigate(
                                                        UpdateRewardFragmentDirections.actionGlobalDialogUpdateReward(
                                                            "", "", it.rewardId.toString(), "", ""
                                                            ,"")
                                                    )
                                                }
                                            }
                                        }, onclaimnowClicked = {

                                            androidx.appcompat.app.AlertDialog.Builder(
                                                requireContext()
                                            )
                                                .setTitle("Delete Chore")
                                                .setTitle("Are you sure you want to claim this reward?")
                                                .setCancelable(false)
                                                .setPositiveButton(
                                                    "Ok"
                                                ) { dialog, whichButton ->

                                                    reward_id = it.rewardId
                                                    callclaimapi()
                                                }
                                                .setNegativeButton("Cancel") { dialog, whichButton ->
                                                    dialog.dismiss()
                                                }
                                                .show()


                                        }, onconfirmClicked = {


                                            confirmrewardapicall(it.rewardId.toString(), isconfirm)

                                        }, onDeleteItemClicked = {
                                            //call delete reward api id= it.choreId


                                            androidx.appcompat.app.AlertDialog.Builder(
                                                requireContext()
                                            )
                                                .setTitle("Delete Chore")
                                                .setTitle("Are you sure you want to delete this reward?")
                                                .setCancelable(false)
                                                .setPositiveButton(
                                                    "Ok"
                                                ) { dialog, whichButton ->

                                                    viewModeldeletereward.deletereward(it.rewardId.toString())
                                                        .observe(
                                                            viewLifecycleOwner
                                                        ) { resource ->
                                                            when (resource.status) {
                                                                Status.SUCCESS -> {
                                                                    loadingIndicator.dismiss()
                                                                    resource.data.let { response ->
                                                                        if (response != null) {
                                                                            if (response.status) {
                                                                                Toast.makeText(
                                                                                    requireContext(),
                                                                                    response.message,
                                                                                    Toast.LENGTH_SHORT
                                                                                ).show()

                                                                                setFragmentResult(
                                                                                    "refresh",
                                                                                    bundleOf("fromdeletereward" to true)
                                                                                )
                                                                                //findNavController().navigateUp()
                                                                            }
                                                                        }

                                                                    }

                                                                }
                                                                Status.LOADING -> {
                                                                    loadingIndicator.show()
                                                                }
                                                                Status.ERROR -> {
                                                                    loadingIndicator.dismiss()
                                                                }
                                                            }

                                                        }
                                                }
                                                .setNegativeButton("Cancel") { dialog, whichButton ->
                                                    dialog.dismiss()
                                                }
                                                .show()


                                        }, "", onlongItemClicked = {

                                            fabtype = "reward"
                                            if (GlobalClass.prefs.getValueString("role_id") == "2" || GlobalClass.prefs.getValueString(
                                                    "role_id"
                                                ) == "3"
                                            ) {
                                                showfab()
                                            } else {
                                                //do nothing
                                            }

                                        }).apply {
                                        rewardRespList = arrrewardList

                                    }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun getFinishedChoreList() {

        fabtype = "finished"

        if (isNetworkConnected(requireContext())) {
            viewModel.getAdminFinishedChoreList(
                GlobalClass.prefs.getValueString("token").toString(), load_more
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    arrfinishList.clear()
                                    arrfinishList.addAll(response.data)


                                    binding.rvFinishedChores.adapter =
                                        FinishedChoreAdapter("morechore",
                                            requireContext(),
                                            onItemClicked = {

                                                if (fabtype == "finished") {
                                                    it.isChecked = !it.isChecked

                                                    binding.rvFinishedChores.adapter!!.notifyDataSetChanged()
                                                }


                                            }, onLongclicked = {
                                                if (GlobalClass.prefs.getValueString("role_id") == "2" || GlobalClass.prefs.getValueString(
                                                        "role_id"
                                                    ) == "3"
                                                ) {
                                                    showfab()
                                                } else {
                                                    //do nothing
                                                }
                                            }).apply {
                                            choreRespList = arrfinishList
                                        }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }

                                GlobalClass.selectepos = -1
                                GlobalClass.isselected = 0
                                GlobalClass.selectedChildId = ArrayList()
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }

    }


    //

  /*  @RequiresApi(Build.VERSION_CODES.M)
    private fun  getFinishedChoreListChild() {


        val today = java.util.Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date",dateToStr)


        if (isNetworkConnected(requireContext())) {
            viewModel.getChildFinishedChoreList(
                GlobalClass.prefs.getValueString("token").toString(),
                load_more,
                to_date,
                from_date,
                status,
                dateToStr

            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {


                                    binding.rvFinishedChores.adapter =
                                        FinishedchorechildAdapter(
                                            requireContext(),
                                            onItemClicked = {}).apply {
                                            choreRespList = response.finished_chore
                                        }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }

                                GlobalClass.selectepos = -1
                                GlobalClass.isselected = 0
                                GlobalClass.selectedChildId = ArrayList()
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }

    }*/

    @RequiresApi(Build.VERSION_CODES.M)
    fun iscompletedApiCall(choreiscompleted: String, choreid: String) {


        val today = Date1()
        val format = SimpleDateFormat("MMM-dd-yyyy hh:mm a")
        var dateToStr = format.format(today)
        val date_time = dateToStr

        viewModel2.chore_iscomplete(
            GlobalClass.prefs.getValueString("token").toString(),
            choreid,
            choreiscompleted,
            date_time

        )
            .observe(viewLifecycleOwner) { resources ->
                when (resources.status) {
                    Status.SUCCESS -> {
                        loadingIndicator.dismiss()
                        resources.data.let { response ->

                            if (response != null) {
                                if (response.status) {

                                    if (choreiscompleted == "1") {
                                        androidx.appcompat.app.AlertDialog.Builder(requireContext())

                                            .setMessage("Chore marked incomplete") // Specifying a listener allows you to take an action before dismissing the dialog.
                                            // The dialog is automatically dismissed when a dialog button is clicked.
                                            .setCancelable(false)
                                            .setPositiveButton(
                                                android.R.string.yes
                                            ) { dialog, which ->
                                                findNavController().navigate(
                                                    UpdateChoresFragmentDirections.actionGlobalDialogUpdateChore(
                                                        choreid,
                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        choreid
                                                        ,"")
                                                )
                                            }
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show()

                                        setFragmentResult(
                                            "refresh",
                                            bundleOf("fromdeletereward" to true)
                                        )

                                    } else {
                                        Toast.makeText(
                                            requireContext(),
                                            response.message,
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        setFragmentResult(
                                            "refresh",
                                            bundleOf("fromChore" to true)
                                        )
                                    }
//                                    getFinishedChoreList()
                                }

                                GlobalClass.selectepos = -1
                                GlobalClass.isselected = 0
                                GlobalClass.selectedChildId = ArrayList()
                            }
                        }

                    }
                    Status.ERROR -> {
                        loadingIndicator.dismiss()

                    }
                    Status.LOADING -> {
                        loadingIndicator.show()
                    }
                }


            }

    }


    //confirm and Claim Section


    private fun callclaimapi() {
        val today = Date1()
        val format = SimpleDateFormat("MMM-dd-yyyy hh:mm a")
        var dateToStr = format.format(today)
        val date_time = dateToStr
        viewModelclaim.claimreward(
            GlobalClass.prefs.getValueString("token").toString(),
            reward_id,
            date_time
        ).observe(viewLifecycleOwner) { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    loadingIndicator.dismiss()
                    resource.data.let { response ->
                        if (response != null) {
                            if (response.status) {

                                Toast.makeText(
                                    requireContext(),
                                    response.message,
                                    Toast.LENGTH_SHORT
                                ).show()

                                setFragmentResult(
                                    "refresh",
                                    bundleOf("fromdeletereward" to true)
                                )
                                findNavController().navigateUp()
                            } else {
                                Toast.makeText(
                                    requireContext(),
                                    response.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                    }

                }
                Status.LOADING -> {
                    loadingIndicator.show()
                }
                Status.ERROR -> {
                    loadingIndicator.dismiss()
                }
            }

        }

    }

    fun confirmrewardapicall(rewardid: String, isconfrim: Int) {

        viewModelconfirm.confirmreward(rewardid, isconfrim)
            .observe(viewLifecycleOwner) { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        loadingIndicator.dismiss()
                        resource.data.let { response ->
                            if (response != null) {
                                if (response.status) {
                                    Toast.makeText(
                                        requireContext(),
                                        response.message,
                                        Toast.LENGTH_SHORT
                                    ).show()

                                    setFragmentResult(
                                        "refresh",
                                        bundleOf("fromconfirm" to true)
                                    )
                                    // findNavController().navigateUp()
                                }
                            }

                        }

                    }
                    Status.LOADING -> {
                        loadingIndicator.show()
                    }
                    Status.ERROR -> {
                        loadingIndicator.dismiss()
                    }
                }

            }

    }


    //Filter Section


    @RequiresApi(Build.VERSION_CODES.M)
    private fun filterfinishedchoreapi() {

        GlobalClass.setTitle = "Finished Chores"

        load_more = "0"
        var childid:String

        if (args.from=="finishedfilterchild"){
            fromfilter="finishchildfilter"
            childid= GlobalClass.prefs.getValueString("user_id").toString()
        }else{
            childid=getchild_id(GlobalClass.selectedChildId)
        }




        if (isNetworkConnected(requireContext())) {
            viewModelfilter.getfinishedfilterlist(
                GlobalClass.prefs.getValueString("token").toString(),
                load_more,
                from_date,
                to_date,
                childid
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    binding.rvFinishedChores.adapter =
                                        FinishedChoreAdapter("morechore",
                                            requireContext(),
                                            onItemClicked = {},
                                            onLongclicked = {

                                            }).apply {
                                            choreRespList = response.data
                                        }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }


    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun filterassignedchoreapi() {

        GlobalClass.setTitle = "Assigned Chores"

//       No need to pass load more we are remove load more from api
//        load_more = "1"
            var childid:String

        if (args.from=="assignedfilterchild"){
            childid= GlobalClass.prefs.getValueString("user_id").toString()
        }else{
            childid=getchild_id(GlobalClass.selectedChildId)
        }

        if (isNetworkConnected(requireContext())) {
            viewModelfilter.getassignedfilterlist(
                GlobalClass.prefs.getValueString("token").toString(),
                "",
                GlobalClass.start_date,
                GlobalClass.end_date,
                childid
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {
                                    binding.rvAssignChoreschores.adapter = AssignedChoreAdapter(
                                        requireContext(),
                                        onItemClicked = {
                                            findNavController().navigate(
                                                UpdateChoresFragmentDirections.actionGlobalDialogUpdateChore(
                                                    it.choreId.toString(),
                                                    it.iconUrl,
                                                    it.token, "", "", ""
                                                    ,"")
                                            )

                                        },
                                        onDeleteItemClicked = {
                                            //call delete chore api id= it.choreId
                                            androidx.appcompat.app.AlertDialog.Builder(
                                                requireContext()
                                            )
                                                .setCancelable(false)
                                                .setTitle("Delete Chore")
                                                .setTitle("Are you sure you want to delete this chore?")
                                                .setPositiveButton(
                                                    "Ok"
                                                ) { dialog, whichButton ->

                                                    viewModeldelete.deletechore(it.choreId.toString())
                                                        .observe(viewLifecycleOwner) { resource ->
                                                            when (resource.status) {
                                                                Status.SUCCESS -> {
                                                                    loadingIndicator.dismiss()
                                                                    resource.data.let { response ->
                                                                        if (response != null) {
                                                                            if (response.status) {
                                                                                Toast.makeText(
                                                                                    requireContext(),
                                                                                    response.message,
                                                                                    Toast.LENGTH_SHORT
                                                                                ).show()

                                                                                setFragmentResult(
                                                                                    "refresh",
                                                                                    bundleOf("fromdeleteChore" to true)
                                                                                )
                                                                                GlobalClass.selectedChildId =
                                                                                    ArrayList()
                                                                                findNavController().navigateUp()
                                                                            }
                                                                        }

                                                                    }

                                                                }
                                                                Status.LOADING -> {
                                                                    loadingIndicator.show()
                                                                }
                                                                Status.ERROR -> {
                                                                    loadingIndicator.dismiss()
                                                                }
                                                            }

                                                        }
                                                }
                                                .setNegativeButton("Cancel") { dialog, whichButton ->
                                                    dialog.dismiss()
                                                }
                                                .show()


                                        },
                                        onlongItemClicked = {}).apply {
                                        choreRespList = response.data
                                    }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }


    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun filterrewardlistapi() {


        GlobalClass.setTitle = "Rewards"

        lateinit var childid:String



        if (args.from=="rewardfilterchild"){
            childid= GlobalClass.prefs.getValueString("user_id").toString()
        }else{
            childid=getchild_id(GlobalClass.selectedChildId)
        }

     //   load_more = "1"

        if (isNetworkConnected(requireContext())) {
            viewModelfilter.getrewardfilterlist(
                GlobalClass.prefs.getValueString("token").toString(),
                from_date,
                to_date,
               childid
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {


                                    binding.rvrewards.adapter = RewardListAdapter(
                                        requireContext(),
                                        onItemClicked = {

                                            findNavController().navigate(
                                                UpdateRewardFragmentDirections.actionGlobalDialogUpdateReward(
                                                    "", "", it.rewardId.toString(), "", ""
                                                    ,"")
                                            )

                                        }, onclaimnowClicked = {
                                            androidx.appcompat.app.AlertDialog.Builder(
                                                requireContext()
                                            )
                                                .setCancelable(false)
                                                .setTitle("Delete Chore")
                                                .setTitle("Are you sure you want to claim this reward?")
                                                .setPositiveButton(
                                                    "Ok"
                                                ) { dialog, whichButton ->

                                                    reward_id = it.rewardId
                                                    callclaimapi()
                                                }
                                                .setNegativeButton("Cancel") { dialog, whichButton ->
                                                    dialog.dismiss()
                                                }
                                                .show()


                                        }, onconfirmClicked = {


                                            confirmrewardapicall(it.rewardId.toString(), isconfirm)

                                        }, onDeleteItemClicked = {
                                            //call delete reward api id= it.choreId


                                            androidx.appcompat.app.AlertDialog.Builder(
                                                requireContext()
                                            )
                                                .setCancelable(false)
                                                .setTitle("Delete Chore")
                                                .setTitle("Are you sure you want to delete this reward?")
                                                .setPositiveButton(
                                                    "Ok"
                                                ) { dialog, whichButton ->

                                                    viewModeldeletereward.deletereward(it.rewardId.toString())
                                                        .observe(viewLifecycleOwner) { resource ->
                                                            when (resource.status) {
                                                                Status.SUCCESS -> {
                                                                    loadingIndicator.dismiss()
                                                                    resource.data.let { response ->
                                                                        if (response != null) {
                                                                            if (response.status) {
                                                                                Toast.makeText(
                                                                                    requireContext(),
                                                                                    response.message,
                                                                                    Toast.LENGTH_SHORT
                                                                                ).show()

                                                                                setFragmentResult(
                                                                                    "refresh",
                                                                                    bundleOf("fromdeletereward" to true)
                                                                                )
                                                                                //findNavController().navigateUp()
                                                                                // GlobalClass.selectedChildId = ArrayList()
                                                                            }
                                                                        }

                                                                    }

                                                                }
                                                                Status.LOADING -> {
                                                                    loadingIndicator.show()
                                                                }
                                                                Status.ERROR -> {
                                                                    loadingIndicator.dismiss()
                                                                }
                                                            }

                                                        }
                                                }
                                                .setNegativeButton("Cancel") { dialog, whichButton ->
                                                    dialog.dismiss()
                                                }
                                                .show()


                                        }, "", onlongItemClicked = {

                                        }).apply {
                                        rewardRespList = response.data.toMutableList()

                                    }

                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }


    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun rewardlistuserwiseapi() {


        GlobalClass.setTitle = "Rewards"

        val today = Date1()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date", dateToStr)




        if (isNetworkConnected(requireContext())) {
            viewModelRewardlist.getRewardListuserwise(
               user_token, "1", dateToStr
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->


                                if (response.data.isEmpty()){
                                    binding.txtNorewrd.visibility=View.VISIBLE
                                }else{
                                    binding.txtNorewrd.visibility=View.GONE
                                }

                                binding.rvrewards.adapter =
                                    RewardListAdapter(requireContext(), onItemClicked = {},
                                        onclaimnowClicked = {},
                                        onconfirmClicked = {},
                                        onDeleteItemClicked = {}, "", onlongItemClicked = {

                                        }).apply {
                                        rewardRespList = response.data.toMutableList()
                                    }


                            }


                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }




    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()
        setFragmentResultListener("refresh") { _, _ ->
            rewardlistapi()
            getAssignedChoreList()
            getFinishedChoreList()

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        //GlobalClass.setTitle=""

//        GlobalClass.selectedchild = ArrayList()
//        GlobalClass.selectedChildId = ArrayList()
//        GlobalClass.start_date = ""
//        GlobalClass.end_date = ""
    }

    fun parseDate(time: String?): String? {
        val inputPattern = "dd-MM-yyyy"
        val outputPattern = "MM-dd-yyyy"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date1? = null
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }



    @RequiresApi(Build.VERSION_CODES.M)
    private fun getuser() {

        if (args.token.isEmpty()){
            user_token=GlobalClass.prefs.getValueString("token").toString()
        }else{
            user_token=args.token
        }


        if (isNetworkConnected(requireContext())) {
            viewModelget.getUserDetail(
              user_token
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    if (response.error == 200) {

                                       GlobalClass.prefs.SaveString("user_id",response.data.userId.toString())
                                      //  GlobalClass.prefs.SaveString("phone",response.data.phoneNo.toString())
                                    } else {
                                        Toast.makeText(
                                            requireContext(),
                                            response.message,
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }

                                }

                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }

    }


}
package com.app.familydays.webservice.responses


import com.squareup.moshi.Json

data class GetChildListResp(
    @Json(name = "data")
    val `data`: List<Data>,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean
) {
    data class Data(
        @Json(name = "child_id")
        val childId: Int,
        @Json(name = "full_name")
        val fullName: String
    )
}
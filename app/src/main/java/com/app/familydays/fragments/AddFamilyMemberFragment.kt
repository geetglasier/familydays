package com.app.familydays.fragments

import android.app.DatePickerDialog
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getColor
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import br.com.ilhasoft.support.validation.Validator
import coil.load
import coil.transform.CircleCropTransformation
import com.app.familydays.R
import com.app.familydays.databinding.FragmentAddFamilyMemberBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.*
import com.app.familydays.viewmodels.AddFamilyMemberVm
import com.app.familydays.viewmodels.FamilyMemberVm
import com.app.familydays.webservice.responses.FamilyMemberListResp
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.fragment_add_family_member.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class AddFamilyMemberFragment : Fragment() {

    private lateinit var binding: FragmentAddFamilyMemberBinding
    private val viewModel: AddFamilyMemberVm by viewModel()
    private lateinit var loadingIndicator: KProgressHUD
    private var imageUri: Uri? = null
    var role = ""
    var isAdmin = "0"
    val myCalendar = Calendar.getInstance()
    private val args: AddFamilyMemberFragmentArgs by navArgs()
    private val viewModelMember: FamilyMemberVm by viewModel()

    var familyMemberList: ArrayList<FamilyMemberListResp.Data>? = null
    private lateinit var validator: Validator

    private val takePhoto =
        registerForActivityResult(ActivityResultContracts.TakePicture())
        {
            if (it) {

                viewModel.setImageUri(imageUri.toString())
                binding.imgProfile.load(imageUri.toString()) {
                    crossfade(true)
                    placeholder(R.drawable.ic_placeholder)
                    error(R.drawable.ic_placeholder)
                    transformations(CircleCropTransformation())
                }
            }
        }
    private val pickImage = registerForActivityResult(ActivityResultContracts.GetContent()) {
        it?.let { uri ->
            imageUri = uri

            viewModel.setImageUri(imageUri.toString())
            binding.imgProfile.load(imageUri.toString()) {
                crossfade(true)
                placeholder(R.drawable.ic_placeholder)
                error(R.drawable.ic_placeholder)
                transformations(CircleCropTransformation())
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_add_family_member, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        validator = Validator(binding)
        loadingIndicator = getLoadingIndicator(requireContext())

        setHasOptionsMenu(true)
        familyMemberList = ArrayList()

        val roleList = listOf("Father", "Mother", "Son", "Daughter")

        binding.txtRole.setAdapter(
            ArrayAdapter(
                requireContext(),
                android.R.layout.simple_spinner_dropdown_item,
                roleList
            )


        )

        binding.txtRole.setOnItemClickListener { adapterView, view, i, l ->

            if (binding.edtBirthdate.text.toString() != "") {
                val birthday = binding.edtBirthdate.text.toString()

                val selectedbday = birthday.split("-")
                val date = selectedbday[2] + "-" + selectedbday[1] + "-" + selectedbday[0]

                if (roleList[i] == "Father" || roleList[i] == "Mother") {

                    val age = getAge(
                        selectedbday[2].toInt(),
                        selectedbday[1].toInt(),
                        selectedbday[0].toInt()
                    )
                    Log.e(
                        "age",
                        getAge(
                            selectedbday[2].toInt(),
                            selectedbday[1].toInt(),
                            selectedbday[0].toInt()
                        ).toString()
                    )

                    if (age != null) {
                        if (age < 18) {
                            Toast.makeText(
                                requireContext(),
                                getString(R.string.txt_birthday_validation),
                                Toast.LENGTH_SHORT
                            ).show()
                            binding.edtBirthdate.setText("")

                        }
                    }


                }

            }

            if (GlobalClass.prefs.getValueString("role").toString() == roleList[i]) {
                binding.llAdmin.visibility = View.VISIBLE
                //  Toast.makeText(requireContext(), "Role already exists!", Toast.LENGTH_SHORT).show()
//                binding.txtRole.setText(roleList[0])
                binding.edtEmail.hint = "Email"


            } else if (roleList[i] == "Son" || roleList[i] == "Daughter") {
                binding.llAdmin.visibility = View.INVISIBLE
                binding.edtEmail.hint = "Email(Optional)"
                binding.edtPhNo.hint = "Phone Number(Optional)"


            } else {
                binding.llAdmin.visibility = View.VISIBLE
                binding.edtEmail.hint = "Email"
                binding.edtPhNo.hint = "Phone Number"

            }

            Log.e("text", binding.txtRole.text.toString())
            for (i in 0 until familyMemberList!!.size) {

                if (familyMemberList!![i].role == 2) {

                    if (familyMemberList!![i].roleType == binding.txtRole.text.toString()) {
                        Toast.makeText(
                            context,
                            getString(R.string.txt_role_validation_check),
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.txtRole.setText("")

                        binding.txtRole.setHint(getString(R.string.txt_role))

                        break
                    }

                } else if (familyMemberList!![i].role == 3) {
                    if (familyMemberList!![i].roleType == binding.txtRole.text.toString()) {
                        Toast.makeText(
                            context,
                            getString(R.string.txt_role_validation_check),
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.txtRole.setText("")
                        binding.txtRole.setHint(getString(R.string.txt_role))

                        break
                    }

                }
            }


        }

        binding.imgProfile.setOnClickListener {
            showMediaPickerDialog(requireContext(), {
                imageUri = FileProvider.getUriForFile(
                    requireContext(), "com.app.familydays" + ".provider",
                    File.createTempFile("image", ".jpg", requireContext().filesDir)
                )
                takePhoto.launch(imageUri)
            }, {
                pickImage.launch("image/*")
            })
        }


        binding.edtBirthdate.setOnClickListener {

            val date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                onDateSet(view, year, monthOfYear, dayOfMonth)
            }

            val dateDialog = DatePickerDialog(
                requireContext(), R.style.datepickerstyle, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            )


            dateDialog.datePicker.maxDate = System.currentTimeMillis()

            dateDialog.show()
            dateDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE)
                .setTextColor(getColor(requireContext(), R.color.colorPrimary))
            dateDialog.getButton(DatePickerDialog.BUTTON_POSITIVE)
                .setTextColor(getColor(requireContext(), R.color.colorPrimary))
        }

        binding.llAdmin.setOnClickListener {

            if (binding.ivChecked.isVisible) {
                isAdmin = "0"
                binding.ivChecked.visibility = View.GONE
                binding.ivUnChecked.visibility = View.VISIBLE

//                binding.edtEmail.hint = "Email(Optional)"

            } else if (binding.ivUnChecked.isVisible) {
                isAdmin = "1"
                binding.ivUnChecked.visibility = View.GONE
                binding.ivChecked.visibility = View.VISIBLE
//                binding.edtEmail.hint = "Email"

            }
        }


        binding.tvSave.setOnClickListener {

            if (binding.txtRole.text.toString() == "Father") {
                role = "2"
                GlobalClass.prefs.SaveString("roletypeF", "Father")

            } else if (binding.txtRole.text.toString() == "Mother") {
                role = "3"
                GlobalClass.prefs.SaveString("roletypeM", "Mother")

            } else if (binding.txtRole.text.toString() == "Son") {
                isAdmin = ""
                role = "4"
            } else if (binding.txtRole.text.toString() == "Daughter") {
                isAdmin = ""
                role = "5"
            } else {
                role = ""
            }


            if (binding.edtUsername.text.isEmpty()){
                Toast.makeText(requireContext(), "Please enter username", Toast.LENGTH_SHORT).show()
            }
            else if (binding.editName.text.isEmpty()){
                Toast.makeText(requireContext(), "Please enter name", Toast.LENGTH_SHORT).show()
            }else if (binding.edtPassword.text.isEmpty()){
                Toast.makeText(requireContext(), "Please enter password", Toast.LENGTH_SHORT).show()
            }else if (isNetworkConnected(requireContext())) {

                if (role == "2" && edtEmail.text.toString() == "") {

                    Toast.makeText(requireContext(), "Please enter email", Toast.LENGTH_SHORT)
                        .show()

                } else if (role == "3" && edtEmail.text.toString() == "") {
                    Toast.makeText(requireContext(), "Please enter email", Toast.LENGTH_SHORT)
                        .show()

                } else if (role == "2" && edtPhNo.text.toString() == "") {

                    Toast.makeText(
                        requireContext(),
                        "Please enter phone number",
                        Toast.LENGTH_SHORT
                    )
                        .show()

                } else if (role == "3" && edtPhNo.text.toString() == "") {
                    Toast.makeText(
                        requireContext(),
                        "Please enter phone number",
                        Toast.LENGTH_SHORT
                    )
                        .show()

                }
                else {

                    addFamilyMember()


                }

            }


        }
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getFamilyMemberList()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun getFamilyMemberList() {
        if (isNetworkConnected(requireContext())) {

            viewModelMember.getFamilyMember(
                GlobalClass.prefs.getValueString("token").toString()
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            resource.data?.let { response ->
                                if (response.status) {
                                    loadingIndicator.dismiss()

                                    familyMemberList?.clear()
                                    familyMemberList?.addAll(response.data)

                                }

                            }


                        }
                        Status.ERROR -> loadingIndicator.dismiss()


                        Status.LOADING -> loadingIndicator.show()
                    }

                }

        }
    }

    fun addFamilyMember() {

        val filePart: MultipartBody.Part? = imageUri?.let {
            activity?.application?.contentResolver?.openInputStream((it))
                ?.use { inputStream ->
                    MultipartBody.Part.createFormData(
                        "profile_file",
                        "image.jpg",
                        inputStream.readBytes()
                            .toRequestBody("image/jpeg".toMediaType())
                    )
                }
        }

        viewModel.addFamilyMember(
            filePart,
            GlobalClass.prefs.getValueString("token").toString(),
            role,
            isAdmin,
            edtEmail.text.toString(),
            edtPassword.text.toString(),
            edtBirthdate.text.toString()
        ).observe(
            viewLifecycleOwner
        ) { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    loadingIndicator.dismiss()
                    resource.data?.let { response ->
                        if (response.status) {
                            if (response.error == 200) {

                                Toast.makeText(
                                    context,
                                    response.message,
                                    Toast.LENGTH_SHORT
                                ).show()

                                setFragmentResult(
                                    "memberlist",
                                    bundleOf("isRefresh" to true)
                                )
                                findNavController().navigateUp()

                            } else {


                                Toast.makeText(
                                    context,
                                    response.message,
                                    Toast.LENGTH_SHORT
                                ).show()


                            }

                        }
                    }

                }
                Status.ERROR -> {

                    loadingIndicator.dismiss()


                }
                Status.LOADING -> {


                    loadingIndicator.show()
                }
            }
        }
    }


    private fun isValidEmailId(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    private fun getAge(year: Int, month: Int, day: Int): Int? {
        val dob = Calendar.getInstance()
        val today = Calendar.getInstance()
        dob[year, month] = day
        var age = today[Calendar.YEAR] - dob[Calendar.YEAR]
//        if (today[Calendar.DAY_OF_YEAR] < dob[Calendar.DAY_OF_YEAR]) {
//            age--
//        }
//        val ageInt = age
        return age
    }

    override fun onPrepareOptionsMenu(menu: Menu) {

//        menu.findItem(R.id.item_profile).isVisible = false
        menu.findItem(R.id.item_add_family).isVisible = false
        menu.findItem(R.id.item_child_profile).isVisible = false
        menu.findItem(R.id.item_add_reward).isVisible=false

        super.onPrepareOptionsMenu(menu)
    }

    fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
        val userAge: Calendar = GregorianCalendar(year, month, day)
        val minAdultAge: Calendar = GregorianCalendar()
        minAdultAge.add(Calendar.YEAR, -18)

        val year = year
        val month = month
        val day = day

        val date = year.toString() + "-" + (month + 1) + "-" + day

        val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val cal = Calendar.getInstance()

        val currenttime = dateFormat.format(cal.time)
        val selectedtime = displayTime(date)
        val futuretime = dateFormat.format(System.currentTimeMillis())

        if (selectedtime == currenttime) {
            Toast.makeText(requireContext(), "you were not born in the future", Toast.LENGTH_SHORT)
                .show()
        } else {

            if (binding.txtRole.text.toString() == "Father" || binding.txtRole.text.toString() == "Mother") {

                if (minAdultAge.before(userAge)) {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.txt_birthday_validation),
                        Toast.LENGTH_SHORT
                    ).show()
                    binding.edtBirthdate.setText("")
                } else {
                    val myFormat = "MMM-dd-yyyy" //In which you need put here
                    val sdf = SimpleDateFormat(myFormat, Locale.US)
                    binding.edtBirthdate.setText(sdf.format(myCalendar.time))
                }

            } else {

                val myFormat = "MMM-dd-yyyy" //In which you need put here
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                binding.edtBirthdate.setText(sdf.format(myCalendar.time))
            }
        }


        /*  if (binding.txtRole.text.equals("Father")||binding.txtRole.text.equals("Mother")) {

            if (minAdultAge.before(userAge)) {
                Toast.makeText(
                        requireContext(),
                        getString(R.string.txt_birthday_validation),
                        Toast.LENGTH_SHORT
                )
                        .show()
            }

        } else {

            val myFormat = "dd-MM-yyyy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            binding.edtBirthdate.setText(sdf.format(myCalendar.time))
        }
    */


    }

    fun displayTime(inputDateTime: String?): String? {
        val inputPattern = "yyyy-M-dd"
        val outputPattern = "yyyy-MM-dd"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    fun dateformate(inputDateTime: String?): String? {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "yyyy-M-dd"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    private fun validate(): Boolean {
        if (!validator.validate())
            return false

        if (role == "") {
            Toast.makeText(
                requireContext(),
                getString(R.string.txt_role_validation),
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        return true
    }
}
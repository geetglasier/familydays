package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class AddRewardVM(private val mainRepository: MainRepository
) : ViewModel() {
    fun AddReward(token:String,category_id:String,rewards_name:String,brand_name:String,brand_icon:String,frame_date:String,point:String,child_id:String,date_time:String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.addreward(token,category_id,rewards_name,brand_name,brand_icon,frame_date,point,child_id,date_time)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

}
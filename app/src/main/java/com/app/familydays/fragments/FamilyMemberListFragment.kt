package com.app.familydays.fragments

import android.R.menu
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.app.familydays.R
import com.app.familydays.adapters.FamilyMemberListAdapter
import com.app.familydays.databinding.FragmentFamilyMemberListBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.viewmodels.FamilyMemberVm
import com.app.familydays.webservice.responses.FamilyMemberListResp
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.android.viewmodel.ext.android.viewModel


class FamilyMemberListFragment : Fragment() {

    private lateinit var binding: FragmentFamilyMemberListBinding
    private val viewModel: FamilyMemberVm by viewModel()
    private lateinit var loadingIndicator: KProgressHUD
    private lateinit var navController: NavController

    var isMother = ""
    var isFather = ""


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
                DataBindingUtil.inflate(
                        inflater,
                        R.layout.fragment_family_member_list,
                        container,
                        false
                )
        binding.lifecycleOwner = this
        loadingIndicator = getLoadingIndicator(requireContext())
        setHasOptionsMenu(true)


        setFragmentResultListener("memberlist") { _, _ ->
            //getFamilyMemberList()
        }

        return binding.root
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getFamilyMemberList()
    }

    override fun onPrepareOptionsMenu(menu: Menu) {

        if (GlobalClass.prefs.getValueString("role") == "Child") {
//            menu.findItem(R.id.item_profile).isVisible = false
            menu.findItem(R.id.item_add_family).isVisible = false
            menu.findItem(R.id.item_child_profile).isVisible = false
        } else {
            menu.findItem(R.id.item_child_profile).isVisible = false
//            menu.findItem(R.id.item_profile).isVisible = false
            menu.findItem(R.id.item_add_family).isVisible = true
        }
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.item_add_family) {

            findNavController().navigate(AddFamilyMemberFragmentDirections.actionGlobalNavNewFamilyMember(isFather,isMother))

        }

        return super.onOptionsItemSelected(item)

    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun getFamilyMemberList() {
        if (isNetworkConnected(requireContext())) {

            viewModel.getFamilyMember(
                    GlobalClass.prefs.getValueString("token").toString()
            )
                    .observe(
                            viewLifecycleOwner
                    ) { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {

                                resource.data?.let { response ->
                                    if (response.status) {
                                        loadingIndicator.dismiss()
                                        if (response.data.isEmpty()) {
                                            binding.rvFamilyMember.visibility = View.GONE
                                            binding.txtNoData.visibility = View.VISIBLE
                                        } else {

                                            binding.rvFamilyMember.visibility = View.VISIBLE
                                            binding.txtNoData.visibility = View.GONE



                                            for (i in 0 until response.data.size) {
                                                if (response.data[i].roleType == "Mother"){
                                                    isMother="yes"
                                                }else if (response.data[i].roleType == "Father"){
                                                    isFather="yes"
                                                }
                                            }

                                            binding.rvFamilyMember.adapter =
                                                    FamilyMemberListAdapter(
                                                            onItemClicked = {
                                                                Log.e("token", GlobalClass.prefs.getValueString("token").toString())

                                                                findNavController().navigate(
                                                                        ChildProfileFragmentDirections.actionGlobalNavChildProfile(
                                                                                it.token, it.profileUrl, it.role.toString(),it.childId.toString()
                                                                        )
                                                                )
                                                            }
                                                    ).apply {
                                                        familyMemberList = response.data
                                                    }


                                            loadingIndicator.dismiss()
                                        }

                                    }


                                }

                            }
                            Status.ERROR -> {
                                resource.exception?.printStackTrace()
                                loadingIndicator.dismiss()
                            }

                            Status.LOADING -> loadingIndicator.show()
                        }
                    }

        }
    }

}
package com.app.familydays.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.app.familydays.R
import com.app.familydays.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

    }

    fun login(view: View) {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    fun createFamily(view: View) {
        val intent = Intent(this, CreateFamilyActivity::class.java)
        startActivity(intent)
    }


    fun openContactUs(view: View) {
        val intent = Intent(this, ContactActivity::class.java)
        startActivity(intent)
    }

}
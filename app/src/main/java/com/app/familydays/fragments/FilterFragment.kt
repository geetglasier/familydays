package com.app.familydays.fragments

import android.R.id
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.app.familydays.R
import com.app.familydays.databinding.FragmentFilterBinding

import kotlinx.android.synthetic.main.fragment_filter.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView

import android.R.id.button1
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.app.familydays.adapters.ChildListAdapter
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.viewmodels.Family_Child_ListVm
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.android.viewmodel.ext.android.viewModel

import androidx.annotation.NonNull
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult

import com.archit.calendardaterangepicker.customviews.CalendarListener
import com.archit.calendardaterangepicker.customviews.printDate
import kotlin.collections.ArrayList


class FilterFragment : Fragment() {

    private lateinit var binding: FragmentFilterBinding
    private val TAG = FilterFragment::class.java.simpleName


    private lateinit var loadingIndicator: KProgressHUD


    private val viewModelchild: Family_Child_ListVm by viewModel()

    var selectedDate = Date()
    var start_date = ""
    var end_date = ""
    var filterby = ""
    var day = 0
    var month = 0
    var year = 0

    var end_day = 0
    var end_month = 0
    var end_year = 0

    private val args: FilterFragmentArgs by navArgs()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter, container, false)

        loadingIndicator = getLoadingIndicator(requireContext())

        Log.d("XXXFilter", args.from)
        binding.cdrvCalendar.setCalendarListener(calendarListener)

        val startMonth = Calendar.getInstance(TimeZone.getDefault())
        startMonth.set(2000, Calendar.JANUARY, 1)
        val endMonth = startMonth.clone() as Calendar
        endMonth.add(Calendar.MONTH, 480)
        Log.d(
            TAG,
            "Start month: " + startMonth.time.toString() + " :: End month: " + endMonth.time.toString()
        )
        binding.cdrvCalendar.setVisibleMonthRange(startMonth, endMonth)


        val current = Calendar.getInstance()

        binding.cdrvCalendar.setCurrentMonth(current)
        binding.cdrvCalendar.setSelectedDateRange(current, current);
        binding.cdrvCalendar.isSelected = true




        Log.d("XXX", args.from)
        familychildlist()
        if (args.from == "assignchild" || args.from == "finishchild"||args.from=="assignchildfilter"||args.from=="finishchildfilter"||args.from=="rewardchild"||args.from=="childclaim") {

            binding.txtsetchild.visibility = View.GONE
            binding.rvChildMember.visibility = View.GONE
            //do logic

        }



        binding.btnfilter.setOnClickListener {


            if (GlobalClass.selectedChildId.size == 0 && GlobalClass.start_date.isEmpty() && GlobalClass.end_date.isEmpty()) {
                Toast.makeText(requireContext(), "Please select any one filter", Toast.LENGTH_SHORT)
                    .show()
            } else {


                if (GlobalClass.clear=="true")
                    GlobalClass.clear=""


                if (start_date.isNotEmpty() && GlobalClass.selectedChildId.size == 0 || start_date.isNotEmpty() &&end_date.isNotEmpty() && GlobalClass.selectedChildId.size == 0) {
                    filterby = "date"

                } else if (GlobalClass.selectedChildId.size > 0 && start_date.isEmpty() && end_date.isEmpty() || GlobalClass.selectedChildId.size > 0 && start_date.isEmpty()) {
                    filterby = "Filter by child"
                } else {
                    filterby = "both"
                }

                Log.d("XXXNEW",args.from)

                if (args.from == "Finished" || args.from == "finishedfilter") {
                    findNavController().navigateUp()
                    findNavController().navigate(
                        MoreChoresFragmentDirections.actionGlobalNavMorechores(
                            "finishedfilter",
                            filterby, ""
                        )
                    )

                    //do logic

                } else if (args.from == "Assigned" || args.from == "assignedfilter") {
                    Log.e("clicked", "click")
                    findNavController().navigateUp()
                    findNavController().navigate(
                        MoreChoresFragmentDirections.actionGlobalNavMorechores(
                            "assignedfilter",
                            filterby, ""
                        )
                    )


                } else if ((args.from == "Rewards" || args.from == "reward")) {
                    findNavController().navigateUp()
                    findNavController().navigate(
                        MoreChoresFragmentDirections.actionGlobalNavMorechores(
                            "rewardfilter",
                            filterby, ""
                        )
                    )
                } else if (args.from == "Claims" || args.from == "claims") {
                    findNavController().navigateUp()
                    findNavController().navigate(
                        MoreChoresFragmentDirections.actionGlobalNavMorechores(
                            "claimfilter",
                            filterby, ""
                        )
                    )

                } else if (args.from == "assignchild") {
                    findNavController().navigateUp()
                    if (GlobalClass.clear.equals("0")){
                        findNavController().navigate(
                            MoreChoresFragmentDirections.actionGlobalNavMorechores(
                                "assignchild",
                                filterby, ""
                            )
                        )
                    }else{
                        findNavController().navigate(
                            MoreChoresFragmentDirections.actionGlobalNavMorechores(
                                "assignchildfilter",
                                filterby, ""
                            )
                        )
                    }



                } else if (args.from == "finishchild"||args.from=="finishchildfilter") {
                    findNavController().navigateUp()
                    findNavController().navigate(
                        MoreChoresFragmentDirections.actionGlobalNavMorechores(
                            "finishchildfilter",
                            filterby, ""
                        )
                    )
                }
                else if (args.from == "rewardchild"){
                    findNavController().navigateUp()
                    findNavController().navigate(
                        MoreChoresFragmentDirections.actionGlobalNavMorechores(
                            "rewardfilterchild",
                            filterby, ""
                        )
                    )
                }else if(args.from=="childclaim"){
                    findNavController().navigateUp()
                    findNavController().navigate(
                        MoreChoresFragmentDirections.actionGlobalNavMorechores(
                            "childclaimfilter",
                            filterby, ""
                        )
                    )
                }

            }



        }
        return binding.root
    }


    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.item_clear).isVisible = true

        menu.findItem(R.id.item_clear).setOnMenuItemClickListener {


            GlobalClass.selectedchild = ArrayList()
            GlobalClass.selectedChildId = ArrayList()
            GlobalClass.start_date = ""
            GlobalClass.end_date = ""

            GlobalClass.clear = "true"
            GlobalClass.clearfilter=1
           /* setFragmentResult(
                "refresh",
                bundleOf("fromfilter" to true)
            )*/

            findNavController().navigateUp()



            true
        }

        super.onPrepareOptionsMenu(menu)
    }

    private fun buildMinimumDate(): Date? {
        val calendar = Calendar.getInstance()
        calendar[Calendar.DAY_OF_YEAR] = 1
        calendar[Calendar.YEAR] = 2019
        return calendar.time
    }


    fun familychildlist() {


        viewModelchild.getChildlist(GlobalClass.prefs.getValueString("token").toString())
            .observe(viewLifecycleOwner) { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        loadingIndicator.dismiss()

                        resource.data?.let { response ->
                            if (response.status) {
                                binding.rvChildMember.adapter = ChildListAdapter(
                                ).apply {
                                    childList = response.data
                                }
                                setHasOptionsMenu(true)

                            }
                        }

                    }
                    Status.ERROR -> loadingIndicator.dismiss()

                    Status.LOADING -> loadingIndicator.show()
                }

            }

    }


    private val calendarListener: CalendarListener = object : CalendarListener {
        override fun onFirstDateSelected(startDate: Calendar) {

            day = startDate[Calendar.DAY_OF_MONTH]
            month = startDate[Calendar.MONTH] + 1
            year = startDate[Calendar.YEAR]



            Log.d("Selected month", "$day-$month-$year")
            start_date = "$day-$month-$year"
            GlobalClass.start_date = start_date



        }

        override fun onDateRangeSelected(startDate: Calendar, endDate: Calendar) {
//

            day = startDate[Calendar.DAY_OF_MONTH]
            month = startDate[Calendar.MONTH] + 1
            year = startDate[Calendar.YEAR]




            end_day = endDate[Calendar.DAY_OF_MONTH]
            end_month = endDate[Calendar.MONTH] + 1
            end_year = endDate[Calendar.YEAR]
            start_date = "$day-$month-$year"
            end_date = "$end_day-$end_month-$end_year"

            Log.d(
                "selecteddate", start_date + "End Date" + end_date
            )



            GlobalClass.start_date = start_date
            GlobalClass.end_date = end_date



        }
    }


    fun finalDateTime(inputDateTime: String?): String? {
        val inputPattern = "dd/MM/yyyy"
        val outputPattern = "dd-MM-yyyy"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.US)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.US)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    override fun onDestroy() {
        super.onDestroy()

        if (args.from == "Finished") {

            //do logic
            GlobalClass.setTitle = "Finished Chores"

        } else if (args.from == "Assigned") {
            GlobalClass.setTitle = "Assigned Chores"
        } else if ((args.from == "Rewards" || args.from == "reward"||args.from=="rewardchild")) {
            GlobalClass.setTitle = "Rewards"
        }

    }

    fun dateToCalendar() {
        val dateStr = "04/05/2010";

        val curFormater = SimpleDateFormat("dd/MM/yyyy")
        val dateObj = curFormater.parse(dateStr)
        val calendar = Calendar.getInstance()
        calendar.setTime(dateObj)

        binding.cdrvCalendar.setCurrentMonth(calendar)
        binding.cdrvCalendar.setSelectedDateRange(calendar, calendar)
        binding.cdrvCalendar.isSelected = true

    }

}
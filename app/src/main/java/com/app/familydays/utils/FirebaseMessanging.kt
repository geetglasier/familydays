package com.app.familydays.utils

import android.R
import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.app.familydays.activities.DashboardActivity
import com.app.familydays.activities.MainActivity
import com.app.familydays.global.GlobalClass
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject


import org.koin.android.ext.android.inject

class FirebaseMessanging: FirebaseMessagingService()  {

    var type: String? = ""

    override fun onNewToken(token: String) {
        super.onNewToken(token)

     GlobalClass.prefs.SaveString("devicetoken",token)
        Log.e("fcmtoken", token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        try {
            val params = remoteMessage.data
            val `object` = JSONObject(params as Map<*, *>)
            Log.e("JSONOBJECT", `object`.toString())

            if (remoteMessage.notification != null) {
                type = `object`.getString("type")

            } else {
                type = `object`.getString("type")


            }

        } catch (e: Exception) {

            Log.e("FIREBASE_Error", e.localizedMessage)
        }

        var resultIntent: Intent? = null
        var resultPendingIntent: PendingIntent? = null
        val stackBuilder = TaskStackBuilder.create(this)


        resultIntent = Intent(this, DashboardActivity::class.java)
            .putExtra("type",type)

        stackBuilder.addNextIntentWithParentStack(resultIntent)
        resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        showNotification(remoteMessage, resultPendingIntent)

    }

    @SuppressLint("ResourceType")
    fun showNotification(remoteMessage: RemoteMessage, resultPendingIntent: PendingIntent?) {
        Log.d("Notification", remoteMessage.data.toString())
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val NOTIFICATION_CHANNEL_ID = "my_channel_id_01"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                "My Notifications",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            // Configure the notification channel.
            notificationChannel.description = "Channel description"
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.vibrationPattern = longArrayOf(0, 100, 50, 100)
            notificationChannel.enableVibration(true)
            notificationManager.createNotificationChannel(notificationChannel)
        }
        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)

        if(remoteMessage.notification == null){
            notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(com.app.familydays.R.drawable.ic_notification_new) //     .setPriority(Notification.PRIORITY_MAX)
                .setColor(ContextCompat.getColor(applicationContext,com.app.familydays.R.color.colorPrimary))
                .setContentTitle(remoteMessage.data["body"])
                .setContentIntent(resultPendingIntent)
                .setContentText(remoteMessage.data["title"])

        }else{
            notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(com.app.familydays.R.drawable.ic_notification_new) //     .setPriority(Notification.PRIORITY_MAX)
                .setColor(ContextCompat.getColor(applicationContext,com.app.familydays.R.color.colorPrimary))
                .setContentTitle(remoteMessage.notification?.title)
                .setContentIntent(resultPendingIntent)
                .setContentText(remoteMessage.notification?.body)

        }


        notificationManager.notify( /*notification id*/1, notificationBuilder.build())
        notificationManager.notify( /*notification id*/1, notificationBuilder.build())
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupNotificationChannels(
        channelId: String,
        channelName: String,
        notificationManager: NotificationManager
    ) {

        val channel =
            NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
        channel.enableLights(true)
        channel.enableVibration(true)
        notificationManager.createNotificationChannel(channel)

    }

    private fun getNotificationIcon(): Int {
        val useWhiteIcon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
        return  com.app.familydays.R.drawable.ic_notification_new
    }
}
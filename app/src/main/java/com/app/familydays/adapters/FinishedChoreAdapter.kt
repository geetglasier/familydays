package com.app.familydays.adapters

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.ItemFinishedChoreBinding
import com.app.familydays.fragments.DashboardFragment
import com.app.familydays.fragments.MoreChoresFragment
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.GetFinishedChoreResp
import java.text.SimpleDateFormat
import java.util.*

class FinishedChoreAdapter(
    val from:String,
    val context: Context,
    val onItemClicked: (GetFinishedChoreResp.Data) -> Unit,
    val onLongclicked:(GetFinishedChoreResp.Data)->Unit,
) :
    RecyclerView.Adapter<FinishedChoreAdapter.FinishedChoreViewHolder>() {
    var choreRespList: List<GetFinishedChoreResp.Data>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class FinishedChoreViewHolder(val binding: ItemFinishedChoreBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @RequiresApi(Build.VERSION_CODES.M)
        fun bind(position: Int) {
            binding.finishedchore = choreRespList?.get(position)



            
            
            

            if (GlobalClass.prefs.getValueString("role").equals("Father") ||
                GlobalClass.prefs.getValueString("role").equals("Mother") ||
                choreRespList?.get(position)!!.isAdmin == 1
            ) {


                if(choreRespList?.get(position)!!.isChecked){
//                binding.clmain.setBackgroundColor(Color.LTGRAY)
                    binding.clSelected.visibility=View.VISIBLE

                    if(GlobalClass.alSelectedPositionsreward.contains(choreRespList?.get(position)!!.choreId)){

                    }else{
                        GlobalClass.alSelectedPositionsreward.add(choreRespList?.get(position)!!.choreId)
                    }


                }else{
//                binding.clmain.setBackgroundColor(Color.TRANSPARENT)
                    binding.clSelected.visibility=View.GONE

                    GlobalClass.alSelectedPositionsreward.remove(choreRespList?.get(position)!!.choreId)


                }




                binding.ll.visibility = View.VISIBLE

                if (choreRespList?.get(position)!!.isAdminComplete == 0) {


                    binding.ivCompleted.setImageDrawable(context.resources.getDrawable(R.drawable.ic_rec_checked))
                    binding.tvCompleted.setTextColor(context.resources.getColor(R.color.colorPrimary))


                } else if (choreRespList?.get(position)!!.isAdminComplete == 1) {

                    binding.ivInCompleted.setImageDrawable(context.resources.getDrawable(R.drawable.ic_rec_checked))
                    binding.tvCompleted.setTextColor(context.resources.getColor(R.color.colorPrimary))


                } else {

                    binding.ivCompleted.setImageDrawable(context.resources.getDrawable(R.drawable.ic_rec_unchecked))
                    binding.tvCompleted.setTextColor(context.resources.getColor(R.color.clr_gray_text))

                }


            } else {
                binding.ll.visibility = View.GONE

            }



            binding.llCompleted.setOnClickListener {

                if (choreRespList?.get(position)!!.isAdminComplete == 0) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else if (choreRespList?.get(position)!!.isAdminComplete == 1) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else {
                    binding.ivCompleted.isEnabled = true
                    binding.ivInCompleted.isEnabled = true


                    if(from == "dashboard"){
                        DashboardFragment.instance.iscompletedApiCall(
                            "0",
                            choreRespList?.get(position)!!.choreId.toString()
                        )
                    }else if(from == "morechore"){
                        MoreChoresFragment.instance.iscompletedApiCall(
                            "0",
                            choreRespList?.get(position)!!.choreId.toString()
                        )
                    }



                }


            }

            binding.llInCompleted.setOnClickListener {

                if (choreRespList?.get(position)!!.isAdminComplete == 0) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else if (choreRespList?.get(position)!!.isAdminComplete == 1) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else {
                    binding.ivCompleted.isEnabled = true
                    binding.ivInCompleted.isEnabled = true

                    if(from == "dashboard"){
                        DashboardFragment.instance.iscompletedApiCall(
                            "1",
                            choreRespList?.get(position)!!.choreId.toString()
                        )
                    }else if(from == "morechore"){
                        MoreChoresFragment.instance.iscompletedApiCall(
                            "1",
                            choreRespList?.get(position)!!.choreId.toString()
                        )
                    }



                }

            }

            if (choreRespList?.size!! > 1) {
                if (position == choreRespList?.size!! - 1) {
                    binding.vwLine.visibility = View.GONE
                }
            }

            if (GlobalClass.prefs.getValueString("role").equals("Father") ||
                GlobalClass.prefs.getValueString("role").equals("Mother") ||
                choreRespList?.get(position)!!.isAdmin == 1
            ) {
                if (choreRespList?.get(position)!!.isAdminComplete == 0) {
                    //green
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))
                } else if (choreRespList?.get(position)!!.isAdminComplete == 1) {
                    //green
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))
                } else if (choreRespList?.get(position)!!.isAdminComplete == 2) {
                    //black
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.black))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.black))
                }


            } else {
                if (choreRespList?.get(position)!!.isAdminComplete == 0) {
                    //green
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))
                } else if (choreRespList?.get(position)!!.isAdminComplete == 2) {
                    //black

                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.black))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.black))
                }

            }





            val due_dte= dueDateFormate(
                binding.finishedchore!!.dueDate).substring(0,dueDateFormate(
                binding.finishedchore!!.dueDate).length-9)


            val duetime: String = dueDateFormate(
                binding.finishedchore!!.dueDate).substring(dueDateFormate(
                binding.finishedchore!!.dueDate).length-9)
            Log.d("xxx", "Date:$due_dte\ntime:$duetime")


            if (binding.finishedchore?.isCreateby == 0) {
                binding.tvDateTime.text =
                    "Created for " + binding.finishedchore?.createBy + " " + binding.finishedchore?.choIsExpired + " on " + dueDateFormate(
                        binding.finishedchore?.dueDate
                    )
            } else {
                binding.tvDateTime.text =
                    "Created by " + binding.finishedchore?.createBy + " " + binding.finishedchore?.choIsExpired + " on " + dueDateFormate(
                        binding.finishedchore?.dueDate
                    )
            }


            binding.root.setOnClickListener {
                onItemClicked(binding.finishedchore!!)
            }
            binding.root.setOnLongClickListener(){
                onLongclicked(binding.finishedchore!!)
                true
            }

            binding.executePendingBindings()
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        FinishedChoreViewHolder(
            ItemFinishedChoreBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(
        holder: FinishedChoreAdapter.FinishedChoreViewHolder,
        position: Int
    ) {
        holder.bind(position)
    }

    override fun getItemCount() = choreRespList?.size ?: 0

    fun dueDateFormate(dueDate: String?): String {
        var spf = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val newDate: Date = spf.parse(dueDate)
        spf = SimpleDateFormat("MMM-dd-yyyy hh:mm aa")
        val newDateString: String = spf.format(newDate)
        Log.i("datetime", newDateString)
        return newDateString

    }
}
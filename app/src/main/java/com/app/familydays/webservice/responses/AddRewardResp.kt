package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

class AddRewardResp(
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean) {
}
package com.app.familydays.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.ClaimItemBinding
import com.app.familydays.databinding.RewardItemBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.ClaimListResp
import com.app.familydays.webservice.responses.GetRewardResp
import java.text.SimpleDateFormat
import java.util.*

class ClaimListAdapter(val context: Context,
                       val onItemClicked: (ClaimListResp.Data) -> Unit,

) :
    RecyclerView.Adapter<ClaimListAdapter.ClaimListViewHolder>() {


    var claimRespList: List<ClaimListResp.Data>? = null
        set(value) {
            field = value
            notifyDataSetChanged()

        }

    var isconfirm=0


    inner class ClaimListViewHolder(val binding: ClaimItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n", "ResourceAsColor")
        fun bind(position: Int) {

            binding.claimlist = claimRespList?.get(position)


            binding.tvclaimby.setText("Claimed by "+claimRespList!![position].create_by)

            binding.tvclaimon.setText("Claimed on\n"+dueDateFormate(claimRespList!![position].create_date))

            binding.tvPoint.setText(claimRespList!![position].point.toString()+" pts" )


            binding.root.setOnClickListener {
                onItemClicked(binding.claimlist!!)
            }

            binding.executePendingBindings()
        }

    }






    override fun getItemCount() = claimRespList?.size ?: 0


    @SuppressLint("SimpleDateFormat")
    fun dueDateFormate(dueDate: String?): String {
        var spf = SimpleDateFormat("yyyy-MM-dd")
        val newDate: Date = spf.parse(dueDate)
        spf = SimpleDateFormat("MMM-dd-yyyy")
        val newDateString: String = spf.format(newDate)
        Log.i("datetime", newDateString)
        return newDateString

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ClaimListViewHolder(
           ClaimItemBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    override fun onBindViewHolder(holder: ClaimListAdapter.ClaimListViewHolder , position: Int) {
        holder.bind(position)
    }


}
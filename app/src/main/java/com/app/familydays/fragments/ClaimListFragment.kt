package com.app.familydays.fragments

import android.app.AlertDialog
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.app.familydays.R
import com.app.familydays.adapters.ClaimListAdapter
import com.app.familydays.adapters.RewardListAdapter
import com.app.familydays.databinding.FragmentClaimListBinding
import com.app.familydays.databinding.FragmentRewardBinding
import com.app.familydays.global.AppConstant
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.utils.unAuthorisedUserDialog
import com.app.familydays.viewmodels.ClaimListVM
import com.app.familydays.viewmodels.RewardlistVM
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.webviewdialog.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

class  ClaimListFragment : Fragment() {

    private lateinit var binding: FragmentClaimListBinding
    private val viewModel: ClaimListVM by viewModel()
    private lateinit var loadingIndicator: KProgressHUD
    private val args: ClaimListFragmentArgs by navArgs()



    var txttitle: TextView? = null
    var webview: WebView? = null

    var btnclose: TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_claim_list, container, false)
        setHasOptionsMenu(true)

        if(  DashboardFragment.instance.loadingIndicator.isShowing) {
            DashboardFragment.instance.loadingIndicator.dismiss()
        }

        loadingIndicator = getLoadingIndicator(requireContext())



      if (args.from=="profile"){
          claimlistuserwise()
      }else{
          claimlistapi()
      }



        return binding.root
    }



    @RequiresApi(Build.VERSION_CODES.M)
    private fun claimlistapi() {
        val today = Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date",dateToStr)

        if (isNetworkConnected(requireContext())) {
            viewModel.getClaimsList(
                GlobalClass.prefs.getValueString("token").toString(),"0",dateToStr,args.notificationId
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->

                                if (response.data.isEmpty()){
                                    binding.txtNoclaim.visibility=View.VISIBLE
                                    binding.rvclaim.visibility=View.GONE
                                }else{
                                    binding.txtNoclaim.visibility=View.GONE
                                    binding.rvclaim.visibility=View.VISIBLE

                                }
                                if(response.data.size == 6){
                                    binding.tvclaimSeemore.visibility=View.VISIBLE
                                }else{
                                    binding.tvclaimSeemore.visibility=View.GONE

                                }


                                binding.tvclaimSeemore.setOnClickListener(){
                                    GlobalClass.setTitle="Claims"

                                    findNavController().navigate(MoreChoresFragmentDirections.actionGlobalNavMorechores("Claims","",""))

                                }




                                binding.rvclaim.adapter=ClaimListAdapter(requireContext(),onItemClicked = {
                                    if(it.brand_url != ""){
                                        showWebViewDialog(it.brand_url)
                                    }


                                }).apply {
                                    claimRespList=response.data
                                }

                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }


    fun showWebViewDialog(url: String) {

        val viewGroup: ViewGroup? = requireView().findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(requireContext())
            .inflate(R.layout.webviewdialog, viewGroup, false)
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())


        //setting the view of the builder to our custom view that we already inflated

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it

        //finally creating the alert dialog and displaying it
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
        txttitle = dialogView.findViewById<TextView>(R.id.textviewtitle)
        btnclose = dialogView.findViewById(R.id.textviewclose)
        webview = dialogView.findViewById(R.id.webview)


        // editText.filters = arrayOf<InputFilter>(MinMaxFilter("1", "500"))
        // editTextpoints!!.setText(binding.txtpoints.text.toString())

        class MyJavaScriptInterface(private val contentView: TextView) {
            fun processContent(aContent: String) {
                contentView.post {
                    contentView.text = aContent
                    txttitle!!.setText(aContent)

                }
            }
        }


        val webViewClient: WebViewClient = object : WebViewClient() {

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(request?.url.toString())
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {

                super.onPageStarted(view, url, favicon)


            }

            override fun onPageFinished(view: WebView?, url: String?) {

                txttitle!!.text = view?.title
                loadingIndicator.dismiss()
                super.onPageFinished(view, url)
            }
        }
        webview!!.webViewClient = webViewClient
//             binding.webview.settings.defaultTextEncodingName = "utf-8"
        webview!!.settings.javaScriptEnabled = true
        webview!!.settings.javaScriptCanOpenWindowsAutomatically = true
        webview!!.settings.domStorageEnabled = true

        webview!!.loadUrl(url)



        webview!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action === KeyEvent.ACTION_DOWN) {
                    val webView = v as WebView
                    when (keyCode) {
                        KeyEvent.KEYCODE_BACK -> if (webView.canGoBack()) {
                            webView.goBack()
                            return true
                        }
                    }
                }
                return false
            }
        })

        dialogView.textviewclose.setOnClickListener() {
            alertDialog.dismiss()
        }


    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun claimlistuserwise() {
        val today = Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date",dateToStr)

        if (isNetworkConnected(requireContext())) {
            viewModel.getClaimsListuserwise(
                args.token,"0",dateToStr
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->

                                if (response.data.isEmpty()){
                                    binding.txtNoclaim.visibility=View.VISIBLE
                                    binding.rvclaim.visibility=View.GONE
                                }else{
                                    binding.txtNoclaim.visibility=View.GONE
                                    binding.rvclaim.visibility=View.VISIBLE

                                }

                                if(response.data.size == 6){
                                    binding.tvclaimSeemore.visibility=View.VISIBLE
                                }else{
                                    binding.tvclaimSeemore.visibility=View.GONE

                                }
                                binding.tvclaimSeemore.setOnClickListener(){
                                    GlobalClass.setTitle="Claims"

                                    findNavController().navigate(MoreChoresFragmentDirections.actionGlobalNavMorechores("childclaims","",args.token))

                                }

                                binding.rvclaim.adapter=ClaimListAdapter(requireContext(),onItemClicked = {
                                    if(it.brand_url != ""){
                                        showWebViewDialog(it.brand_url)
                                    }

                                }).apply {
                                    claimRespList=response.data
                                }

                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }




}
package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class Family_Child_ListVm(
    private val mainRepository: MainRepository
) : ViewModel()  {
    fun getChildlist(token: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.childlist(token)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

}
package com.app.familydays.webservice.responses


import com.squareup.moshi.Json

data class UploadImageProfileResp(
    @Json(name = "data")
    var `data`: Data,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean
) {
    data class Data(
        @Json(name = "profile_url")
        var profileUrl: String
    )
}
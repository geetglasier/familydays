package com.app.familydays.webservice.responses


import com.squareup.moshi.Json

data class GetAssignedChoreResp(
    @Json(name = "data")
    val `data`: List<Data>,
    @Json(name = "disapproved_count")
    val disapprovedCount: Int=0,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean
) {
    data class Data(

        @Json(name = "chore_id")
        val choreId: Int,
        @Json(name = "create_by")
        val createBy: String,
        @Json(name = "due_date")
        val dueDate: String,
        @Json(name = "icon_url")
        val iconUrl: String,
        @Json(name = "is_admin")
        val isAdmin: Int,
        @Json(name = "is_admin_complete")
        val isAdminComplete: Int,
        @Json(name = "is_complete")
        val isComplete: Int,
        @Json(name = "is_conform")
        val isConform: Int,
        @Json(name = "is_createby")
        val isCreateby: Int,
        @Json(name = "is_daily")
        val isDaily: String,
        @Json(name = "left_days")
        val leftDays: String,
        @Json(name = "point")
        val point: Int,
        @Json(name = "title")
        val title: String,
        @Json(name = "token")
        val token: String,


        @Transient
        var isChecked:Boolean=false

    )
}
package com.app.familydays.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.databinding.ClaimItemBinding
import com.app.familydays.databinding.ItemMessageBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.ClaimListResp
import com.app.familydays.webservice.responses.GetMessagesResp
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class GetMessagesAdapter(val context: Context

) :
    RecyclerView.Adapter<GetMessagesAdapter.MessageViewHolder>() {


    var getmessageList: List<GetMessagesResp.Data>? = null
        @SuppressLint("NotifyDataSetChanged")
        set(value) {
            field = value
            notifyDataSetChanged()

        }

    inner class MessageViewHolder(val binding:ItemMessageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n", "ResourceAsColor")
        fun bind(position: Int) {

            binding.member=getmessageList!!.get(position)
            binding.token= GlobalClass.prefs.getValueString("token").toString()

            binding.tvmsg.text = getmessageList!![position].message
            binding.tvFullName.text=getmessageList!![position].fullname
            Glide.with(itemView)
                .load(getmessageList!![position].profileurl)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(binding.imgProfile)





            binding.executePendingBindings()
        }

    }

    override fun getItemCount() = getmessageList?.size ?: 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MessageViewHolder(
            ItemMessageBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    override fun onBindViewHolder(holder:MessageViewHolder, position: Int) {
        holder.bind(position)
    }
}
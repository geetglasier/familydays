package com.app.familydays.activities

import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.app.familydays.BuildConfig
import com.app.familydays.R
import com.app.familydays.databinding.ActivityContactBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.*
import com.app.familydays.viewmodels.ContactAdminVm
import com.kaopiz.kprogresshud.KProgressHUD
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File

class ContactActivity : AppCompatActivity() {

    private lateinit var binding: ActivityContactBinding
    private val viewModel: ContactAdminVm by viewModel()
    private lateinit var loadingIndicator: KProgressHUD
    private lateinit var validator: Validator
    private var imageUri: Uri? = null

    private val takePhoto =
        registerForActivityResult(ActivityResultContracts.TakePicture())
        {
            if (it) {
                viewModel.setImageUri(imageUri.toString())
            }
        }
    private val pickImage = registerForActivityResult(ActivityResultContracts.GetContent()) {
        it?.let { uri ->
            imageUri = uri
            viewModel.setImageUri(imageUri.toString())
        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        validator = Validator(binding)
        loadingIndicator = getLoadingIndicator(this)


        if(GlobalClass.prefs.getValueString("token").toString() != null){
        }



        binding.ivBack.setOnClickListener {
            finish()
        }

        binding.tvUpload.setOnClickListener {
            showMediaPickerDialog(this, {
                imageUri = FileProvider.getUriForFile(
                    applicationContext, BuildConfig.APPLICATION_ID + ".provider",
                    File.createTempFile("image", ".jpg", filesDir)
                )
                takePhoto.launch(imageUri)
            }, {
                pickImage.launch("image/*")
            })
        }


        binding.tvSend.setOnClickListener {

            if (binding.editName.text.isEmpty()){
                Toast.makeText(applicationContext, "Please enter name", Toast.LENGTH_SHORT).show()
            }else if (binding.edtPhNo.text.isEmpty()){
                Toast.makeText(applicationContext, "Please enter phone number", Toast.LENGTH_SHORT).show()
            }else if (binding.edtSubject.text.isEmpty()){
                Toast.makeText(applicationContext, "Please enter subject", Toast.LENGTH_SHORT).show()
            }else if (!binding.edtEmail.text.toString().trim().matches(Patterns.EMAIL_ADDRESS.toRegex())||binding.edtEmail.text.isEmpty()) {
                Toast.makeText(applicationContext, "Please enter valid e-mail", Toast.LENGTH_SHORT)
                    .show()
            }else if (binding.edtMessage.text.isEmpty()){
                Toast.makeText(applicationContext, "Please enter message", Toast.LENGTH_SHORT).show()
            }else{

            if (isNetworkConnected(baseContext)) {
                val filePart: MultipartBody.Part? = imageUri?.let {
                    application.contentResolver.openInputStream((it))?.use { inputStream ->
                        MultipartBody.Part.createFormData(
                            "media_file",
                            "image.jpg",
                            inputStream.readBytes().toRequestBody("image/jpeg".toMediaType())
                        )
                    }
                }
                viewModel.contactAdmin(
                    filePart
                ).observe(
                    this
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    Toast.makeText(
                                        baseContext,
                                        "Your message was sent successfully.",
                                        Toast.LENGTH_LONG
                                    ).show()

                                    finish()
                                } else {

                                    Toast.makeText(
                                        baseContext,
                                        response.message,
                                        Toast.LENGTH_LONG
                                    ).show()

                                }


                            }

                        }
                        Status.ERROR -> {
                            loadingIndicator.dismiss()

                        }
                        Status.LOADING -> {
                            loadingIndicator.show()
                        }
                    }
                }

            }
        }
        }

    }

    private fun validate(): Boolean {
        if (!validator.validate())
            return false

        return true
    }

}
package com.app.familydays.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import br.com.ilhasoft.support.validation.Validator
import com.app.familydays.R
import com.app.familydays.R.*
import com.app.familydays.adapters.AssignedChoreAdapter
import com.app.familydays.adapters.ChildAssignChoreAdapter
import com.app.familydays.adapters.ChildFinishedChore
import com.app.familydays.adapters.FinishedChoreAdapter
import com.app.familydays.databinding.FragmentAddFamilyMemberBinding
import com.app.familydays.databinding.FragmentProfileBinding
import com.app.familydays.global.AppConstant
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.utils.unAuthorisedUserDialog
import com.app.familydays.viewmodels.*
import com.app.familydays.webservice.responses.GetChildMemberDetailResp
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.android.viewmodel.dsl.ATTRIBUTE_VIEW_MODEL
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

class ChildProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private val viewModel: ChildMemberDetailVm by viewModel()
    private lateinit var loadingIndicator: KProgressHUD

    private val viewModeldelete: DeleteChoreVM by viewModel()

    private val args: ChildProfileFragmentArgs by navArgs()
    lateinit var token :String
    var role ="0"
    var selected=0
    var assignsize=0
    var finishsize=0
    var status="assign_chores"


    @SuppressLint("ResourceAsColor", "UseCompatLoadingForDrawables", "ResourceType")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, layout.fragment_profile, container, false)
        binding.lifecycleOwner = this

        Log.e("token", GlobalClass.prefs.getValueString("token").toString())

        loadingIndicator = getLoadingIndicator(requireContext())
        getassignedchores()


        binding.tvseemore.setOnClickListener(){

           if (status=="assign_chores"){

               GlobalClass.setTitle="AssignedChoreschild"
               findNavController().navigate(MoreChoresFragmentDirections.actionGlobalNavMorechores("assignchild","",args.childToken))

           }else if (status=="finished_chores"){
               GlobalClass.setTitle="FinishedChoreschild"
               findNavController().navigate(MoreChoresFragmentDirections.actionGlobalNavMorechores("finishchild","",args.childToken))

           }
        }



        binding.tvExisting.setOnClickListener(){
         status="assign_chores"
            if (assignsize==6){
                binding.tvseemore.visibility= View.VISIBLE
            }else{
                binding.tvseemore.visibility= View.GONE
            }


            selected=0


            binding.txttitle.text = "Assigned Chores"
            binding.rvFinishedChores.visibility=View.GONE
            binding.rvassignedChores.visibility=View.VISIBLE

            binding.tvExisting.setTextColor(resources.getColor(color.white))
            binding.tvExisting.setBackgroundResource(R.drawable.bgdark_border)
            binding.tvCompleted.setTextColor(resources.getColor(color.clr_text))
            binding.tvCompleted.setBackgroundResource(drawable.text_border)

        }
        binding.tvCompleted.setOnClickListener(){

            status="finished_chores"
            if (finishsize==6){
                binding.tvseemore.visibility= View.VISIBLE
            }else{
                binding.tvseemore.visibility= View.GONE
            }

           binding.txttitle.text = "Finished Chores"
            binding.rvassignedChores.visibility=View.GONE
            binding.rvFinishedChores.visibility=View.VISIBLE
            binding.tvCompleted.setTextColor(resources.getColor(color.white))
            binding.tvCompleted.setBackgroundResource(R.drawable.bgdark_border)
            binding.tvExisting.setTextColor(resources.getColor(color.clr_text))
            binding.tvExisting.setBackgroundResource(R.drawable.text_border)
        }

        binding.tvRewards.setOnClickListener(){
            findNavController().navigate(RewardFragmentDirections.actionGlobalReward(token,"profile"))
        }

        binding.tvClaims.setOnClickListener(){
            findNavController().navigate(ClaimListFragmentDirections.actionGlobalClaim(args.childToken,"profile",""))
        }


        return binding.root
    }



    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun getassignedchores(){
        if (isNetworkConnected(requireContext())) {
            val today = Date()
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val dateToStr = format.format(today)

            token=args.childToken
            role=args.role
            viewModel.getChildFamilyMemberDetail(args.childToken,dateToStr)
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {
                                    binding.viewModel = response
                                    binding.tvPoint.text=response.totalPoint.toString()+" Pts"


                                    Glide.with(this)
                                        .load(response.profileUrl)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .into(binding.imgProfile)

                                    assignsize=response.assignChore.size

                                    finishsize=response.finishChore.size

                                    setHasOptionsMenu(true)
                                    if (response.assignChore.size==6){
                                        binding.tvseemore.visibility=View.VISIBLE
                                    }else{
                                        binding.tvseemore.visibility=View.GONE
                                    }

                                    binding.imgmsg.setOnClickListener(){
                                        if (GlobalClass.prefs.getValueString("token")!=response.assignChore.get(0).token){
                                            findNavController().navigate(ChatFragmentDirections.actionGlobalNavChat(GlobalClass.prefs.getValueString("token").toString(),args.childId,response.totalPoint.toString(),""))
                                        }
                                        else{
                                            findNavController().navigate(ChatlistFragmentDirections.actionGlobalNavChatlist( GlobalClass.prefs.getValueString("token").toString(),""))
                                        }

                                    }

                                    binding.rvassignedChores.adapter =
                                        ChildAssignChoreAdapter(
                                            requireContext(),
                                            onItemClicked = {

                                                findNavController().navigate(
                                                    UpdateChoresFragmentDirections.actionGlobalDialogUpdateChore(
                                                        it.choreId.toString(),
                                                        it.iconUrl,
                                                        it.token,"",""
                                                        ,"","")
                                                )


                                            }, onDeleteItemClicked = {
                                                //call delete chore api id= it.choreId
                                                val dialog = AlertDialog.Builder(context)
                                                    .setTitle("Delete Chore")
                                                    .setTitle("Are you sure you want to delete this chore?")
                                                    .setPositiveButton(
                                                        "Ok"
                                                    ) { dialog, whichButton ->

                                                        viewModeldelete.deletechore(it.choreId.toString()).observe(viewLifecycleOwner) { resource ->
                                                            when (resource.status) {
                                                                Status.SUCCESS -> {
                                                                    loadingIndicator.dismiss()
                                                                    resource.data.let { response ->
                                                                        if (response != null) {
                                                                            if (response.status) {
                                                                                Toast.makeText(
                                                                                    requireContext(),
                                                                                    response.message,
                                                                                    Toast.LENGTH_SHORT
                                                                                ).show()

                                                                                setFragmentResult(
                                                                                    "refresh",
                                                                                    bundleOf("fromdeleteChore" to true)
                                                                                )
                                                                                findNavController().navigateUp()
                                                                            }
                                                                        }

                                                                    }

                                                                }
                                                                Status.LOADING -> {
                                                                    loadingIndicator.show()
                                                                }
                                                                Status.ERROR -> {
                                                                    loadingIndicator.dismiss()
                                                                }
                                                            }

                                                        }
                                                    }
                                                    .setNegativeButton("Cancel") { dialog, whichButton ->
                                                        dialog.dismiss()
                                                    }
                                                    .show()


                                            }).apply {
                                            choreList = response.assignChore
                                        }


                                    binding.rvFinishedChores.adapter =
                                        ChildFinishedChore("dashboard",
                                            requireContext(),
                                            onItemClicked = {}).apply {
                                            choreList = response.finishChore
                                        }




                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }


                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }
    }

    override fun onResume() {
        super.onResume()
        status="assign_chores"
    }

    override fun onDestroy() {
        super.onDestroy()
        GlobalClass.setTitle=""
        status="assign_chores"
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        var itemImage = menu.findItem(R.id.item_child_profile)

        itemImage.isVisible = true
        menu.findItem(R.id.item_add_family).isVisible = false
        menu.findItem(R.id.item_add_reward).isVisible=false
//        menu.findItem(R.id.item_profile).isVisible = false

      //  itemImage!!.icon=resources.getDrawable(R.drawable.ic_no_user)


        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {


        if(item.itemId == R.id.item_child_profile){
            if (role == "2" || role == "3"){
                findNavController().navigate(EditProfileFragmentDirections.actionGlobalNavEditProfile( args.childToken))
            }else{
                findNavController().navigate(
                    UpdateChildProfileFragmentDirections.actionGlobalNavUpdateProfile(
                        args.childToken
                    )
                )
            }
        }
        return super.onOptionsItemSelected(item)

    }
}
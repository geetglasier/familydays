package com.app.familydays.activities

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.app.familydays.R
import com.app.familydays.databinding.ActivityForgotPasswordBinding
import com.app.familydays.databinding.ActivityLoginBinding
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.viewmodels.ForgotPassVm
import com.google.android.material.snackbar.Snackbar
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.android.viewmodel.ext.android.viewModel


class ForgotPasswordActivity : AppCompatActivity() {

    private lateinit var binding: ActivityForgotPasswordBinding
    private val viewModel: ForgotPassVm by viewModel()
    private lateinit var loadingIndicator: KProgressHUD


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
        val validator = Validator(binding)
        loadingIndicator = getLoadingIndicator(this)

        binding.ivBack.setOnClickListener {
            finish()
        }

        binding.tvForgot.setOnClickListener() {
            if (validator.validate() && isNetworkConnected(baseContext)) {
                viewModel.forgotPassword(binding.edtEmail.text.toString())
                    .observe(
                        this
                    ) { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {
                                loadingIndicator.dismiss()
                                resource.data?.let { response ->
                                    Toast.makeText(
                                        baseContext,
                                        response.message,
                                        Toast.LENGTH_LONG
                                    ).show()
                                    finish()
                                }

                            }
                            Status.ERROR -> {
                                loadingIndicator.dismiss()
                            }
                            Status.LOADING -> {
                                loadingIndicator.show()
                            }
                        }


                    }

            }
        }
    }
}
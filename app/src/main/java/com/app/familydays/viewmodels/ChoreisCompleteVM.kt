package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class ChoreisCompleteVM(private val mainRepository: MainRepository): ViewModel() {
    fun chore_iscomplete(token:String,chore_id:String,chore_iscomplete:String,date_time:String)=
        liveData(Dispatchers.IO) {
            emit(Resource.loading(data = null))
            try {
                emit(Resource.success(data = mainRepository.chores_iscomplete(token,chore_id,chore_iscomplete,date_time)))
            } catch (exception: Exception) {
                exception.printStackTrace()
                emit(Resource.error(data = null, exception = exception))
            }



        }

}
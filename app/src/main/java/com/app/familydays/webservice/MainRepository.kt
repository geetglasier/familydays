package com.app.familydays.webservice

import okhttp3.MultipartBody
import okhttp3.RequestBody


class MainRepository(private val apiService: ApiService) {

    suspend fun signUp(params: Map<String, RequestBody>, file: MultipartBody.Part?) =
        apiService.signUp(params, file)

    suspend fun login(username: String, password: String, devicetoken: String,device_type:String) =
        apiService.login(username, password, devicetoken,device_type)

    suspend fun getchoresdetails(chore_id: String,notification_id:String) =
        apiService.choredetails(chore_id,notification_id)

    suspend fun getrewarddetails(reward_id: String,notification_id:String) =
        apiService.rewarddetails(reward_id,notification_id)

    suspend fun addchores(token:String,icon_name:String,title:String,chore_time:String,child_id:String,is_daily_chore:String,set_point:String,date_time: String)=
        apiService.addchores(token,icon_name,title,chore_time,child_id,is_daily_chore,set_point,date_time)


    suspend fun chores_iscomplete(token:String,chore_id:String,chore_iscomplete:String,date_time:String)=
            apiService.chores_is_complete(token,chore_id,chore_iscomplete,date_time)

    suspend fun chores_approve(token:String,chore_id:String)=
        apiService.chores_approve(token,chore_id)


    suspend fun addchoreschild(token:String,icon_name:String,title:String,chore_time:String,is_daily_chore:String,is_confirmation:String,cho_is_complete:String,set_point:String)=
        apiService.addchoreschild(token,icon_name,title,chore_time,is_daily_chore,is_confirmation,cho_is_complete,set_point)

    suspend fun addreward(token:String,category_id:String,rewards_name:String,brand_name:String,brand_icon:String,frame_date:String,point:String,child_id:String,date_time:String)=
        apiService.addreward(token,category_id,rewards_name,brand_name,brand_icon,frame_date,point,child_id,date_time)

    suspend fun updatechoreschild(token:String,chore_id:String,icon_name:String,title:String,chore_time:String,is_daily_chore:String,is_confirmation:String,cho_is_complete:String,set_point:String,date_time: String)=
        apiService.updatechoreschild(token,chore_id,icon_name,title,chore_time,is_daily_chore,is_confirmation,cho_is_complete,set_point,date_time)



    suspend fun updatereward(token:String,rewards_name: String,reward_id:Int,category_id:Int,category:String,category_icon:String,brand_name:String,brand_icon:String,frame_date:String,point:Int)=
        apiService.updatereward(token,rewards_name,reward_id,category_id,category,category_icon,brand_name,brand_icon,frame_date,point)

    suspend fun deletechore(chore_id:String) =
        apiService.deletechore(chore_id)

    suspend fun deleterewad(rewardid:String) =
        apiService.deletereward(rewardid)

    suspend fun confirm_reward(reward_id:String,is_confirm:Int) =
        apiService.confirm_reward(reward_id,is_confirm)

    suspend fun claim_reward(token:String,reward_id:Int,date_time:String) =
        apiService.claim_reward(token,reward_id,date_time)


    suspend fun forgotPassword(email: String) = apiService.forgotPassword(email)

    suspend fun contactAdmin(params: Map<String, RequestBody>, file: MultipartBody.Part?) =
        apiService.contactAdmin(params, file)

    suspend fun getUserDetail(token: String) = apiService.getUserDetail(token)


    suspend fun getFamilyMemberList(token: String) = apiService.getFamilyMemberList(token)

    suspend fun getFamilyMemberListmsg(token: String,notification_id: String) = apiService.getFamilyMemberListmsg(token,notification_id)


    suspend fun getcount(token: String) = apiService.getcount(token)

    suspend fun getmessage(token: String,user_id:String) = apiService.getmessages(token,user_id)

    suspend fun sendmessage(token: String,received_id:String,message:String,date_time:String) = apiService.sendmessage(token,received_id,message,date_time)



    suspend fun updateProfilePic(token: String, file: MultipartBody.Part?) =
        apiService.updateProfilePic(token, file)

    suspend fun updateUserDetail(token: String,name:String,email:String,phone_no:String,role:String,birth_date: String,password:String,is_admin:String) =
        apiService.updateUserDetail(token,name,email,phone_no,role,birth_date,password,is_admin)

    suspend fun updatechildDetail(token: String,username:RequestBody,child_id:RequestBody,fullname:RequestBody,email:RequestBody,password: RequestBody,birth_date:RequestBody,role:RequestBody,is_admin:RequestBody,phone_no:RequestBody,profilefile:MultipartBody.Part?) =
        apiService.updatechildDetail(token,username,child_id,fullname,email,password,birth_date,role,is_admin,phone_no,profilefile)


    suspend fun addFamilyMember(
        params: Map<String, RequestBody>,
        file: MultipartBody.Part?,
        token: String
    ) =
        apiService.addFamilyMember(token, params, file)

    suspend fun getChildMemberDetail(
        token: String,date_time:String
    ) =
        apiService.getChildMemberDetail(token,date_time)

    suspend fun changepassword(
        token: String,password:String
    ) =
        apiService.resetpwd(token,password)

    suspend fun childlist(
        token: String
    ) =
        apiService.childlist(token)

    suspend fun getAdminFinishedChoreList(
        token: String,load_more: String

    ) =
        apiService.getAdminFinishedChoreList(token,load_more)

    suspend fun getAdminAssignedChoreList(
        token: String,load_more:String,date_time: String
    ) =
        apiService.getAdminAssignedChoreList(token,load_more,date_time)

    suspend fun rewardlist(
        token: String,load_more:String,date_time: String
    ) =
        apiService.getrewardList(token,load_more,date_time)

    suspend fun rewardlistuserwise(
        token: String,load_more:String,date_time: String
    ) =
        apiService.getrewardListuserwise(token,load_more,date_time)

    suspend fun claimslist(
        token: String,load_more:String,date_time: String,notification_id:String
    ) =
        apiService.getclaimsList(token,load_more,date_time,notification_id)

    suspend fun claimslistuserwise(
        token: String,load_more:String,date_time: String
    ) =
        apiService.getclaimsListuserwise(token,load_more,date_time)



    suspend fun getChild_assigned_finishedChoreList(
        token: String,load_more:String,to_date: String,from_date:String,status:String,date_time: String
    ) =
        apiService.child_assign_finished_chores(token,load_more,to_date,from_date,status,date_time)


   suspend fun getPopularChoreList(
        token: String
    ) =
        apiService.getPopularChoreList(token)


    suspend fun getnotificationList(
        token: String
    ) =
        apiService.getnotificationList(token)

    suspend fun getrewardcategoryList(
        token: String
    ) =
        apiService.getrewardCategoryList(token)

    suspend fun getrewardBrand(
        cat_id: String
    ) =
        apiService.getrewardBrand(cat_id)


    suspend fun getFinishedchoreFilter(
        token: String,load_more:String,from_date: String,to_date: String,child_id:String

    ) =
        apiService.getfinishedchoresfilterlist(token,load_more,from_date,to_date,child_id)

    suspend fun getassignedchoreFilter(
        token: String,load_more:String,from_date: String,to_date: String,child_id:String

    ) =
        apiService.getassignedchoresfilterlist(token,load_more,from_date,to_date,child_id)

    suspend fun getrewardFilter(
        token: String,from_date: String,to_date: String,child_id:String

    ) =
        apiService.getrewardfilterlist(token,from_date,to_date,child_id)

    suspend fun getclaimFilter(
        token: String,from_date: String,to_date: String,child_id:String

    ) =
        apiService.getclaimfilterlist(token,from_date,to_date,child_id)



}


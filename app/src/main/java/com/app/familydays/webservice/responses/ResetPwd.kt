package com.app.familydays.webservice.responses

data class ResetPwd(
    val error: Int,
    val message: String,
    val status: Boolean
)
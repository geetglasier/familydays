package com.app.familydays.webservice.responses


import com.app.familydays.webservice.models.FamilyDaysUser
import com.squareup.moshi.Json

data class GetUserDetailResp(
    @Json(name = "data")
    val `data`: FamilyDaysUser,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean
)
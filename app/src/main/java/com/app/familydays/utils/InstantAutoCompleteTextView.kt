package com.app.familydays.utils

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.Filterable
import androidx.appcompat.widget.AppCompatAutoCompleteTextView


class InstantAutoComplete : AppCompatAutoCompleteTextView, View.OnClickListener {
    init {
        this.setOnClickListener(this)
    }

    constructor(context: Context?) : super(context!!)
    constructor(arg0: Context?, arg1: AttributeSet?) : super(arg0!!, arg1)
    constructor(arg0: Context?, arg1: AttributeSet?, arg2: Int) : super(arg0!!, arg1, arg2)

    override fun enoughToFilter(): Boolean {
        return true
    }

    override fun onClick(v: View?) {
        if (adapter != null) {
            performFiltering("", 0)
            showDropDown()
        }
    }

    override fun onFilterComplete(count: Int) {
        super.onFilterComplete(count)
        val mFilter = (adapter as Filterable).filter

        Log.i("filter", mFilter.toString())

    }
}
package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

data class ChildChoreResp(

    @Json(name="status") val status: Boolean,
    @Json(name="error") val error: Int,
    @Json(name="message") val message: String,
    @Json(name="child_name") val child_name: String,
    @Json(name="profile_url") val profile_url: String,
    @Json(name="total_point") val total_point: Int,
    @Json(name="assign_chore") val assign_chore: List<Assign_chore>,
    @Json(name="finish_chore") val finish_chore: List<Finish_chore>
                      ) {


    data class Assign_chore(

        @Json(name="chore_id") val chore_id : Int,
        @Json(name="title") val title : String,
        @Json(name="create_by") val create_by : String,
        @Json(name="point") val point : Int,
        @Json(name="is_daily") val is_daily : String,
        @Json(name="is_complete") val is_complete : Int,
        @Json(name="is_admin_complete") val is_admin_complete : Int,
        @Json(name="is_conform") val is_conform : Int,
        @Json(name="is_admin") val is_admin : Int,
        @Json(name="is_createby") val is_createby : Int,
        @Json(name="due_date") val due_date : String,
        @Json(name="child_id") val child_id : String,
        @Json(name="token") val token : String,
        @Json(name="left_days") val left_days : String,
        @Json(name="icon_url") val icon_url : String
    )


    data class Finish_chore(
        @Json(name="chore_id") val chore_id : Int,
        @Json(name="title") val title : String,
        @Json(name="create_by") val create_by : String,
        @Json(name="point") val point : Int,
        @Json(name="is_daily") val is_daily : String,
        @Json(name="is_complete") val is_complete : Int,
        @Json(name="is_admin_complete") val is_admin_complete : Int,
        @Json(name="is_conform") val is_conform : Int,
        @Json(name="is_admin") val is_admin : Int,
        @Json(name="is_createby") val is_createby : Int,
        @Json(name="cho_is_expired") val cho_is_expired : String,
        @Json(name="due_date") val due_date : String,
        @Json(name="token") val token : String,
        @Json(name="icon_url") val icon_url : String
    )





}
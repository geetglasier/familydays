package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class LoginVm(private val mainRepository: MainRepository) : ViewModel() {
    fun login(username: String, password: String,devicetoken:String,device_type:String) = liveData(Dispatchers.IO) {
            emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.login(username,password,devicetoken,device_type)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }





}
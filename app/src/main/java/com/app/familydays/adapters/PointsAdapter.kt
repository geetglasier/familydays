package com.app.familydays.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.fragments.*


class PointsAdapter(val context: Context, val pointslist: List<String>,val from:String) :
    RecyclerView.Adapter<PointsAdapter.ViewHolder>() {

    var points: Int = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_points, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.points.text = pointslist[position]

        holder.clMain.setOnClickListener {

            if (from == "Addnew"){
                AddNewChoreFragment.instance.editText!!.text = pointslist[position]
                AddNewChoreFragment.instance.bottomSheetDialog!!.dismiss()
            }else if(from == "fromchild"){
                AddchoreschildFragment.instance.editTextpoints!!.text = pointslist[position]
               AddchoreschildFragment.instance.bottomSheetDialog!!.dismiss()
            }else if (from == "fromupdate"){
                UpdateChoresFragment.instance.editTextpoints!!.text = pointslist[position]
                UpdateChoresFragment.instance.bottomSheetDialog!!.dismiss()
            }else if(from=="Addnewreward"){
                AddNewRewardFragment.instance.editTextpoints!!.text = pointslist[position]
                AddNewRewardFragment.instance.bottomSheetDialog!!.dismiss()
            }else if (from=="updatereward"){
                UpdateRewardFragment.instance.editTextpoints!!.text = pointslist[position]
                UpdateRewardFragment.instance.bottomSheetDialog!!.dismiss()
            }



        }

    }

    override fun getItemCount(): Int {
        return pointslist.size
        Log.d("points", pointslist.size.toString())
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val points: TextView = itemView.findViewById(R.id.tvpoints)
        val clMain: ConstraintLayout = itemView.findViewById(R.id.clMain)
    }
}
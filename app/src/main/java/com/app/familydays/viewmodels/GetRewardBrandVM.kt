package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class GetRewardBrandVM (private val mainRepository: MainRepository
) : ViewModel()  {

    fun getRewardBrand(cat_id: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getrewardBrand(cat_id)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }
}
package com.app.familydays.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.app.familydays.R
import com.app.familydays.adapters.DrawerMenuAdapter
import com.app.familydays.adapters.NotificationListAdapter
import com.app.familydays.databinding.ActivityDashboardBinding
import com.app.familydays.databinding.FragmentNotificationListBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.viewmodels.GetCountVM
import com.app.familydays.viewmodels.NotificationListVM
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel


class NotificationListFragment : Fragment() {

    private val notiication_list: NotificationListVM by viewModel()
    private lateinit var loadingIndicator: KProgressHUD
    lateinit var binding: FragmentNotificationListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification_list, container, false)
        loadingIndicator = getLoadingIndicator(requireContext())

        notificationlist_api()


        return binding.root
    }

    fun notificationlist_api(){
        notiication_list.getnotificationlist(
            GlobalClass.prefs.getValueString("token").toString()
        )
            .observe(
                viewLifecycleOwner
            ) { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        loadingIndicator.dismiss()
                        resource.data?.let { response ->
                        //  Log.d("XXX",response.message_count.toString()+" "+response.notification_count)
                            binding.rvnotification.adapter=NotificationListAdapter(requireContext(),onItemClicked = {


                                lifecycleScope.launch {
                                    delay(500)
                                    val type = it.type


                                    Log.e(
                                        "notificationdata",
                                        type.toString()
                                    )


                                    if (type == "send_message") {
                                        //move to message fragment

                                        findNavController().navigate(
                                            ChatFragmentDirections.actionGlobalNavChat(GlobalClass.prefs.getValueString("token").toString(),it.user_id,it.token,it.notification_id.toString())
                                        )

                                    } else if (type == "cliam_create_child") {
                                        //move to comment listing
                                        findNavController().navigate(
                                            ClaimListFragmentDirections.actionGlobalClaim("","",it.notification_id.toString()))

                                    } else if (type == "reward_create_by_child" || type == "reward_create_by_parent") {
                                        //move to forum detail

                                        findNavController().navigate(
                                            UpdateRewardFragmentDirections.actionGlobalDialogUpdateReward(
                                               "","",it.reward_id,"","",it.notification_id.toString()
                                            )
                                        )

                                    }
                                    else if (type == "chore_create_by_parent" || type == "chore_create_by_child") {
                                        //move to forum detail

                                       findNavController().navigate(
                                          UpdateChoresFragmentDirections.actionGlobalDialogUpdateChore(it.chores_id,"",it.token,"","","",it.notification_id.toString())
                                        )

                                    }
                                }

                            }).apply {
                                notificationlist = response.data
                            }

                        }

                    }
                    Status.ERROR -> loadingIndicator.dismiss()

                    Status.LOADING -> loadingIndicator.show()
                }
            }

    }


}
package com.app.familydays.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import com.app.familydays.webservice.requests.ContactAdminReq
import kotlinx.coroutines.Dispatchers
import okhttp3.MultipartBody


class ContactAdminVm(private val mainRepository: MainRepository) : ViewModel() {
    val contactAdminReq = MutableLiveData(ContactAdminReq())
    fun contactAdmin(filePart: MultipartBody.Part?) = liveData(
        Dispatchers.IO
    ) {
        emit(Resource.loading(data = null))
        try {
            contactAdminReq.value?.let {
                emit(
                    Resource.success(
                        data = mainRepository.contactAdmin(
                            it.toMap(),
                            filePart
                        )
                    )
                )
            } ?: emit(Resource.error(data = null, exception = Exception("Error Occurred in data!")))

        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

    fun setImageUri(uri: String) {
        contactAdminReq.value = contactAdminReq.value?.copy(media_file = uri)
    }


}
package com.app.familydays.utils

data class Resource<out T>(val status: Status, val data: T?, val exception: Exception?) {
    companion object {
        fun <T> success(data: T): Resource<T> = Resource(status = Status.SUCCESS, data = data, exception = null)

        fun <T> error(data: T?, exception: Exception): Resource<T> =
            Resource(status = Status.ERROR, data = data, exception = exception)

        fun <T> loading(data: T?): Resource<T> = Resource(status = Status.LOADING, data = data, exception = null)
    }
}
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
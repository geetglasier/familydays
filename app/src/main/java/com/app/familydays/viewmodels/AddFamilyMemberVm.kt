package com.app.familydays.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import com.app.familydays.webservice.models.AddFamilyUser
import com.app.familydays.webservice.models.FamilyDaysUser
import com.app.familydays.webservice.requests.AddFamilyReq
import com.app.familydays.webservice.requests.SignUpReq
import kotlinx.coroutines.Dispatchers
import okhttp3.MultipartBody

class AddFamilyMemberVm (
    private val mainRepository: MainRepository
) : ViewModel() {

    val addFamilyReq = MutableLiveData(AddFamilyReq())

        fun addFamilyMember(filePart: MultipartBody.Part?, token:String, role:String,isAdmin:String,email:String,password:String,birthdate:String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))

        try {
            addFamilyReq.value?.let {
                emit(
                    Resource.success(
                        data = mainRepository.addFamilyMember(
                            it.copy(role = role,is_admin = isAdmin,email = email,password = password,birth_date = birthdate).toMap(),
                             filePart,
                           token
                        )
                    )
                )
            } ?: emit(Resource.error(data = null, exception = Exception("Error Occurred in data!")))

        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

    fun setImageUri(uri: String) {
        addFamilyReq.value = addFamilyReq.value?.copy(profile_file = uri)
    }

    fun setBirthDay(day: String) {
        addFamilyReq.value = addFamilyReq.value?.copy(birth_date = day)
    }




}
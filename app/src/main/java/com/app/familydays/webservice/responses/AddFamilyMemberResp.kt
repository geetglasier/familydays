package com.app.familydays.webservice.responses


import com.squareup.moshi.Json

data class AddFamilyMemberResp(
    @Json(name = "data")
    val `data`: Data,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean
) {
    data class Data(
        @Json(name = "email")
        val email: String? = "",
        @Json(name = "full_name")
        val fullName: String? = "",
        @Json(name = "is_admin")
        val isAdmin: Int? = 0,
        @Json(name = "profile_url")
        val profileUrl: String? = "",
        @Json(name = "role")
        val role: Int? = 0,
        @Json(name = "token")
        val token: String? = "",
        @Json(name = "user_id")
        val userId: Int? = 0,
        @Json(name = "username")
        val username: String? = ""
    )
}
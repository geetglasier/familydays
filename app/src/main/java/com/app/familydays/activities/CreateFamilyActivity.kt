package com.app.familydays.activities

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.app.familydays.BuildConfig
import com.app.familydays.R
import com.app.familydays.databinding.ActivityCreateFamilyBinding
import com.app.familydays.global.AppConstant
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.*
import com.app.familydays.viewmodels.SignUpVm
import com.kaopiz.kprogresshud.KProgressHUD
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class CreateFamilyActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCreateFamilyBinding
    private val viewModel: SignUpVm by viewModel()
    private lateinit var loadingIndicator: KProgressHUD
    private lateinit var validator: Validator
    private var imageUri: Uri? = null
    var role = ""
    var msg = ""


    val myCalendar = Calendar.getInstance()
    private val takePhoto =
        registerForActivityResult(ActivityResultContracts.TakePicture())
        {
            if (it) {
                viewModel.setImageUri(imageUri.toString())
            }
        }
    private val pickImage = registerForActivityResult(ActivityResultContracts.GetContent()) {
        it?.let { uri ->
            imageUri = uri
            viewModel.setImageUri(imageUri.toString())
        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_family)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        validator = Validator(binding)
        loadingIndicator = getLoadingIndicator(this)

        binding.imgProfile.setImageResource(R.drawable.ic_user)

        binding.ivBack.setOnClickListener {
            finish()
        }

        val roleList = listOf("Father", "Mother")

        binding.txtRole.setAdapter(
            ArrayAdapter(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                roleList
            )
        )

        binding.imgProfile.setOnClickListener {
            showMediaPickerDialog(this, {
                imageUri = FileProvider.getUriForFile(
                    applicationContext, BuildConfig.APPLICATION_ID + ".provider",
                    File.createTempFile("image", ".jpg", filesDir)
                )
                takePhoto.launch(imageUri)
            }, {
                pickImage.launch("image/*")
            })
        }

        binding.edtBirthdate.setOnClickListener {


            val date = OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                onDateSet(view, year, monthOfYear, dayOfMonth)
            }

            val dateDialog = DatePickerDialog(
                this, R.style.datepickerstyle, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            )

//            dateDialog.datePicker.minDate = System.currentTimeMillis() - 1000

            dateDialog.show()
            dateDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE)
                .setTextColor(getColor(R.color.colorPrimary))
            dateDialog.getButton(DatePickerDialog.BUTTON_POSITIVE)
                .setTextColor(getColor(R.color.colorPrimary))
        }

        binding.tvSignup.setOnClickListener {


            validateinput()
        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun validateinput() {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

        if (binding.edtUsername.text.isEmpty()){
              Toast.makeText(applicationContext, "Please enter username", Toast.LENGTH_SHORT).show()
        }else if (binding.editFamilyName.text.isEmpty()){
              Toast.makeText(applicationContext, "Please enter family name", Toast.LENGTH_SHORT).show()
        }else if (binding.editName.text.isEmpty()){
              Toast.makeText(applicationContext, "Please enter name", Toast.LENGTH_SHORT).show()
        }else if (!binding.edtEmail.text.toString().trim().matches(emailPattern.toRegex())||binding.edtEmail.text.isEmpty()) {
            Toast.makeText(applicationContext, "Please enter valid e-mail", Toast.LENGTH_SHORT)
                .show()
        }else if (binding.edtPhNo.text.isEmpty()){
             Toast.makeText(applicationContext, "Please enter phone number", Toast.LENGTH_SHORT).show()
        }else if (binding.edtPassword.text.isEmpty()){
            Toast.makeText(applicationContext, "Please enter password", Toast.LENGTH_SHORT).show()
        }else if (binding.edtPassword.length()<6) {
            Toast.makeText(applicationContext, "Password must be 6 character", Toast.LENGTH_SHORT)
                .show()
        }else{
            if (binding.txtRole.text.toString() == "Father") {
                role = "2"
            } else if (binding.txtRole.text.toString() == "Mother") {
                role = "3"
            } else {
                role = ""
            }
            var bday = ""

            if (binding.edtBirthdate.text.toString() != "") {
                bday = finalDateTime(binding.edtBirthdate.text.toString()).toString()

            } else {
                bday = ""
            }

            Log.e("bday", bday)

            if (isNetworkConnected(baseContext)) {
                val filePart: MultipartBody.Part? = imageUri?.let {
                    application.contentResolver.openInputStream((it))?.use { inputStream ->
                        MultipartBody.Part.createFormData(
                            "profile_file",
                            "image.jpg",
                            inputStream.readBytes().toRequestBody("image/jpeg".toMediaType())
                        )
                    }
                }


                viewModel.signUp(
                    filePart,
                    GlobalClass.prefs.getValueString("devicetoken").toString(), role, bday!!,
                "2").observe(
                    this
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()

                            resource.data?.let { response ->
                                // msg=response.message
                                if (response.status) {

                                    loadingIndicator.dismiss()
                                    if (response.error == 200) {


                                        GlobalClass.prefs.SaveString(
                                            "token",
                                            response.data.token ?: ""
                                        )
                                        GlobalClass.prefs.SaveString(
                                            "id",
                                            response.data.userId.toString()
                                        )
                                        GlobalClass.prefs.SaveString(
                                            "username",
                                            response.data.fullName ?: ""
                                        )
                                        GlobalClass.prefs.SaveString(
                                            "email",
                                            response.data.email ?: ""
                                        )
                                        GlobalClass.prefs.SaveString(
                                            "profilePic",
                                            response.data.profileUrl ?: ""
                                        )
                                        when (response.data.role) {
                                            2 -> {
                                                GlobalClass.prefs.SaveString("role", "Father")
                                            }
                                            3 -> {
                                                GlobalClass.prefs.SaveString("role", "Mother")

                                            }
                                            else -> {
                                                GlobalClass.prefs.SaveString("role", "Child")
                                            }

                                        }
                                        GlobalClass.prefs.SaveString(
                                            "role_id",
                                            response.data.role.toString()
                                        )

                                        Toast.makeText(
                                            this,
                                            response.message,
                                            Toast.LENGTH_LONG
                                        ).show()

                                        startActivity(
                                            Intent(
                                                this,
                                                DashboardActivity::class.java
                                            )
                                        )
                                        finish()


                                    } else {


                                        Toast.makeText(
                                            this,
                                            response.message,
                                            Toast.LENGTH_SHORT
                                        ).show()


                                    }
                                }

                            }

                        }
                        Status.ERROR -> {
                            loadingIndicator.show()

                        }
                        Status.LOADING -> {
                            loadingIndicator.show()

                        }
                    }
                }

            }
        }
    }

    fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
        val userAge: Calendar = GregorianCalendar(year, month, day)
        val minAdultAge: Calendar = GregorianCalendar()
        minAdultAge.add(Calendar.YEAR, -18)
        if (minAdultAge.before(userAge)) {
            Toast.makeText(this, getString(R.string.txt_birthday_validation), Toast.LENGTH_SHORT)
                .show()
        } else {
//            val myFormat = "dd-MM-yyyy" //In which you need put here
            val myFormat = "MMM-dd-yyyy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            binding.edtBirthdate.setText(sdf.format(myCalendar.time))

        }
    }

    private fun validate(): Boolean {
        if (!validator.validate())
            return false

        if (role == "") {
            Toast.makeText(this, getString(R.string.txt_role_validation), Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    fun finalDateTime(inputDateTime: String?): String? {
        val inputPattern = "MMM-dd-yyyy"
        val outputPattern = "dd-MM-yyyy"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.US)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.US)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }


}
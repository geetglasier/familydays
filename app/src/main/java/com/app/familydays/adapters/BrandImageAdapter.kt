package com.app.familydays.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.ChildlistItemBinding
import com.app.familydays.databinding.ItemPopularChoreBinding
import com.app.familydays.databinding.ItemRewardImageBinding
import com.app.familydays.databinding.RewardItemBinding
import com.app.familydays.fragments.AddNewRewardFragment
import com.app.familydays.fragments.UpdateRewardFragment
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.GetChildListResp
import com.app.familydays.webservice.responses.RewardBrandResp
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class BrandImageAdapter(val context: Context,val from:String):
    RecyclerView.Adapter<BrandImageAdapter.ImageViewHolder>(){

    var brandList: List<RewardBrandResp.Data.Sub_Brands>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var selectedId:ArrayList<Int>? = ArrayList()

    inner class ImageViewHolder(val binding: ItemRewardImageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("ResourceAsColor")
        fun bind(position: Int) {
            binding.subbrand = brandList?.get(position)

            Glide.with(context)
                .load(brandList?.get(position)!!.brand_icon)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(binding.ivBrand)


                //add selection logic here

            if (GlobalClass.selectedimage_url == brandList!![position].brand_icon_name ){
                binding.clShadow.visibility=View.VISIBLE

            }else{
                binding.clShadow.visibility=View.GONE

            }


            binding.root.setOnClickListener {
//                onItemClicked(binding.subbrand!!)


                Log.d("selectedurl",brandList!![position].brand_icon_name)
                GlobalClass.selectedimage_url=brandList!![position].brand_icon_name


                if (from=="addnew"){

                    if (brandList!![position].brand_url.isNotEmpty()){


                        AddNewRewardFragment.instance.showwebviewdialog(brandList!![position].brand_url)
                        AddNewRewardFragment.instance.category_id=0
                        AddNewRewardFragment.instance.brand_icon=brandList!![position].brand_icon_name



                    }else {
                        // do nothing
                        // AddNewRewardFragment.instance.showwebviewdialog(brandList!![position].brand_url)
                        AddNewRewardFragment.instance.category_id=0
                        AddNewRewardFragment.instance.brand_icon=brandList!![position].brand_icon_name

                    }


                }else{

                    if (brandList!![position].brand_url.isNotEmpty()){


                        UpdateRewardFragment.instance.showwebviewdialog(brandList!![position].brand_url)
                        UpdateRewardFragment.instance.category_id=0
                        UpdateRewardFragment.instance.brand_icon=brandList!![position].brand_icon_name



                    }else {
                        // do nothing
                        // AddNewRewardFragment.instance.showwebviewdialog(brandList!![position].brand_url)
                        UpdateRewardFragment.instance.category_id=0
                        UpdateRewardFragment.instance.brand_icon=brandList!![position].brand_icon_name

                    }

                }






                notifyDataSetChanged()
                //binding.cardView2.setBackgroundColor(R.color.colorPrimary)


            }



            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {

     return   ImageViewHolder(
            ItemRewardImageBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    }

    override fun onBindViewHolder(holder:ImageViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
       return brandList!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

}
package com.app.familydays.activities

import android.content.Context
import android.graphics.*
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.app.familydays.R
import com.app.familydays.adapters.DrawerMenuAdapter
import com.app.familydays.databinding.ActivityDashboardBinding
import com.app.familydays.fragments.*
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.*
import com.app.familydays.viewmodels.EditProfileVm
import com.app.familydays.viewmodels.GetCountVM
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*


class DashboardActivity : AppCompatActivity() {

    lateinit var binding: ActivityDashboardBinding
    private lateinit var navController: NavController
    private val viewModel: GetCountVM by viewModel()
    private lateinit var appBarConfiguration: AppBarConfiguration
    var role: String? = null
     lateinit var loadingIndicator: KProgressHUD

    private val viewModelget: EditProfileVm by viewModel()


    var unreadmsg:Int=0
    var notificationcount=0

     lateinit var myBitmap: Bitmap

    private var showProfileMenu = false
    lateinit var roundedbitmap:Bitmap

    var itemImage: MenuItem? = null
    var itemAdd: MenuItem? = null
    var item_child_profile: MenuItem? = null
    var item_Reward: MenuItem? = null
    var item_filter:MenuItem?=null
    var item_clear:MenuItem?=null
    var text: TextView? = null



    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)

        loadingIndicator = getLoadingIndicator(this)


        binding.appBarMain.toolbar.overflowIcon =
            AppCompatResources.getDrawable(this, R.drawable.ic_menu)

        setSupportActionBar(binding.appBarMain.toolbar)
//        binding.appBarMain.toolbarTitle.text="Family Dashboard"
//        supportActionBar!!.setDisplayShowTitleEnabled(false)

//        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
//        StrictMode.setThreadPolicy(policy)
        navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(setOf(R.id.nav_dashboard), binding.drawer)
        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.navView.setupWithNavController(navController)




        val intent = intent
        val intent1 = intent.extras
        if (intent1 != null) {

            val type = intent.getStringExtra("type")

            if (type != null) {
                Log.e("notificationdataextra",type)
            }
        }
//               if (intent != null) {
//            Log.e("notificationdata", intent1.toString())
//        }


        if (intent1 != null) {
            lifecycleScope.launch {
                delay(500)
                val type = intent.getStringExtra("type")


                Log.e(
                    "notificationdata",
                    type.toString()
                )


                if (type == "send_message") {
                    //move to message fragment

                    navController.navigate(
                        ChatlistFragmentDirections.actionGlobalNavChatlist(GlobalClass.prefs.getValueString("token").toString(),"")
                    )





                } else if (type == "cliam_create_child") {
                    //move to comment
                    // .listing
                    navController.navigate(
                        ClaimListFragmentDirections.actionGlobalClaim("","",""))

                } else if (type == "reward_create_by_child" || type == "reward_create_by_parent") {
                    //move to forum detail

                    navController.navigate(
                        RewardFragmentDirections.actionGlobalReward(
                            "",""
                        )
                    )





                }
//                else if (type == "chore_create_by_parent" || type == "chore_create_by_child") {
//                    //move to forum detail
//
//                    navController.navigate(
//                        DashboardFragmentDirections.actionGlobalNavHome(
//
//                        )
//                    )
//
//                }
            }


        } else {
            Log.e("notificationdata", "intent null")

        }


        navController.addOnDestinationChangedListener { controller, destination, arguments ->



            val inflator =
                this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val v: View = inflator.inflate(R.layout.item_drawer_menu, null)



            text = v.findViewById(R.id.tvMsgCount)


            if (destination.id in setOf(R.id.nav_dashboard)) {
                supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu)
                binding.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                binding.appBarMain.ivProfile.visibility=View.VISIBLE
                binding.appBarMain.tvpointsab.visibility=View.GONE

            }/*else if (destination.id in setOf(R.id.nav_chatlist)){
                binding.appBarMain.tvpointsab.visibility=View.GONE
            }*/else{

                text!!.visibility = View.GONE


                if(destination.id in setOf(R.id.nav_chat)){
                    binding.appBarMain.tvpointsab.visibility=View.VISIBLE

                }else{
                    binding.appBarMain.tvpointsab.visibility=View.GONE
                }


                if(destination.id in setOf(R.id.nav_see_more)){
                    if(GlobalClass.setTitle == "Assigned Chores"||GlobalClass.setTitle == "AssignedChoreschild"){

                        binding.appBarMain.toolbar.title="Assigned Chores"

                    }else  if(GlobalClass.setTitle == "Finished Chores"||GlobalClass.setTitle == "FinishedChoreschild") {
                        binding.appBarMain.toolbar.title = "Finished Chores"

                    }
                    else if (GlobalClass.setTitle=="Reward"||GlobalClass.setTitle=="Rewards"){
                        binding.appBarMain.toolbar.title = "Reward"

                    }
                    else if (GlobalClass.setTitle=="Claims"){
                        binding.appBarMain.toolbar.title = "Claims"
                    }
                    else{
                        binding.appBarMain.toolbar.title=""
                    }
                }else if(destination.id in setOf(R.id.nav_get_popular_chore)) {
                    if (GlobalClass.setTitle == "Reward") {
                        binding.appBarMain.toolbar.title = "Reward Title"
                    } else {
                        binding.appBarMain.toolbar.title = "Chore Title"
                    }



                }





                supportActionBar?.setDisplayShowTitleEnabled(true)
                supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
                binding.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

                binding.appBarMain.ivProfile.visibility=View.GONE

            }

        }

        binding.image = GlobalClass.prefs.getValueString("profilePic")
        binding.username = GlobalClass.prefs.getValueString("username")


        binding.rvDrawerMenu.adapter = DrawerMenuAdapter(
            this, navController,
            onItemClicked = { dest, args ->
                dest?.let { d ->
                    binding.drawer.close()
                    navController.navigate(d, args)
                }

            }
        ) {
            confirmationDialog(this, getString(R.string.txt_logout_msg)) {
                logoutUser(this)
            }
        }


        binding.appBarMain.ivProfile.setOnClickListener {

            if (GlobalClass.prefs.getValueString("role").equals("Child")) {
                navController.navigate(
                    ChildProfileFragmentDirections.actionGlobalNavChildProfile(
                        GlobalClass.prefs.getValueString("token").toString(), "","",
                        GlobalClass.prefs.getValueString("role_id").toString()
                    )
                )
            } else {
                navController.navigate(
                    EditProfileFragmentDirections.actionGlobalNavEditProfile(
                        GlobalClass.prefs.getValueString("token").toString()

                    )
                )


            }




        }



        loadprofilepic()
        getcountapi()
        getuser()

    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.drawer_menu, menu)

        itemAdd = menu!!.findItem(R.id.item_add_family)
//        itemImage = menu.findItem(R.id.item_profile)
        item_child_profile = menu.findItem(R.id.item_child_profile)
        item_Reward = menu.findItem(R.id.item_add_reward)
        item_filter=menu.findItem(R.id.item_filter)
        item_clear=menu.findItem(R.id.item_clear)



        itemAdd!!.isVisible = false
        item_child_profile!!.isVisible = false
        item_Reward!!.isVisible= false

        item_filter!!.isVisible=false
        item_clear!!.isVisible=false



        val filter = SpannableString("Filter")
        val clear = SpannableString("Clear All")

        filter.setSpan(ForegroundColorSpan(Color.WHITE), 0, filter.length, 0)
        item_filter!!.title = filter
        clear.setSpan(ForegroundColorSpan(Color.WHITE), 0, clear.length, 0)
        item_clear!!.title = clear




        return true
    }

    override fun onResume() {
        loadprofilepic()

        super.onResume()
    }

    override fun onRestart() {
        loadprofilepic()
        super.onRestart()
    }




    fun loadprofilepic() {


        Thread {
            try {
                // Your implementation
                try {
                    val cornerRadius = 25
                    val url = URL(GlobalClass.prefs.getValueString("profilePic").toString())
                    val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
                    connection.doInput = true
                    connection.connect()
                    val input: InputStream = connection.inputStream
                    myBitmap = BitmapFactory.decodeStream(input)
                    roundedbitmap=getRoundedCornerBitmap(myBitmap, 20)

                } catch (e: IOException) {
                    e.printStackTrace()

                }


            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }.start()


        Glide.with(this)
                .load(GlobalClass.prefs.getValueString("profilePic").toString())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(binding.imgProfile)

        Glide.with(this)
                .load(GlobalClass.prefs.getValueString("profilePic"))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(binding.appBarMain.ivProfile)

    }




    override fun onBackPressed() {
        if (navController.currentBackStackEntry?.destination?.id == R.id.nav_dashboard){
            super.onBackPressed()
        }
        else{
            onSupportNavigateUp()
        }

    }
    fun getRoundedCornerBitmap(bitmap: Bitmap, pixels: Int): Bitmap {
        val output = Bitmap.createBitmap(
            bitmap.width, bitmap
                .height, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(output)
        val color = -0xbdbdbe
        val paint = Paint()
        val rect = Rect(0, 0, bitmap.width, bitmap.height)
        val rectF = RectF(rect)
        val roundPx = pixels.toFloat()
        paint.isAntiAlias = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = color
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint)
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(bitmap, rect, rect, paint)
        return output
    }



    @RequiresApi(Build.VERSION_CODES.M)
    private fun getcountapi() {
        val today = Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date", dateToStr)

        if (isNetworkConnected(applicationContext)) {
            viewModel.getcount(
                GlobalClass.prefs.getValueString("token").toString()
            ).observe(
                    this
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->

                                //  Log.d("XXX",response.message_count.toString()+" "+response.notification_count)

                                notificationcount = response.notification_count
                                unreadmsg = response.message_count

                                if (response.message_count == 0&&response.notification_count == 0) {
                                    text!!.visibility = View.GONE
                                } else {
                                    text!!.visibility = View.VISIBLE
                                    (binding.rvDrawerMenu.adapter as DrawerMenuAdapter).setMessageCount(
                                        response.message_count, response.notification_count

                                    )
                                }


                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.dismiss()
                    }
                }

        }
    }


    private fun getuser() {

        if (isNetworkConnected(applicationContext)) {
            viewModelget.getUserDetail(
               GlobalClass.prefs.getValueString("token").toString()
            )
                .observe(
                   this
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    if (response.error == 200) {

                                        GlobalClass.prefs.SaveString("phone",response.data.phoneNo.toString())
                                    } else {
                                        Toast.makeText(
                                            applicationContext,
                                            response.message,
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }

                                }

                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }

    }


}
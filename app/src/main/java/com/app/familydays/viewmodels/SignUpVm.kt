package com.app.familydays.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import com.app.familydays.webservice.requests.SignUpReq
import kotlinx.coroutines.Dispatchers
import okhttp3.MultipartBody

class SignUpVm(private val mainRepository: MainRepository) : ViewModel() {
     val signUpReq = MutableLiveData(SignUpReq())
    fun signUp(filePart: MultipartBody.Part?, devicetoken:String,role:String,birthday:String,device_type: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            signUpReq.value?.let {
                emit(
                    Resource.success(
                        data = mainRepository.signUp(
                            it.copy(device_token = devicetoken,role = role,birth_date = birthday,device_type=device_type).toMap(),
                            filePart
                        )
                    )
                )
            } ?: emit(Resource.error(data = null, exception = Exception("Error Occurred in data!")))

        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

    fun setImageUri(uri: String) {
        signUpReq.value = signUpReq.value?.copy(profile_file = uri)
    }

    fun setBirthDay(day: String) {
        signUpReq.value = signUpReq.value?.copy(birth_date = day)
    }



}
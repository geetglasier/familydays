package com.app.familydays.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.room.util.StringUtil
import com.app.familydays.R
import com.app.familydays.adapters.StringUtils.center
import com.app.familydays.databinding.ItemAssignChoreBinding
import com.app.familydays.databinding.ItemAssignedChoreBinding
import com.app.familydays.databinding.ItemFinishedChoreBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.viewmodels.DeleteChoreVM
import com.app.familydays.viewmodels.LoginVm
import com.app.familydays.webservice.responses.GetAssignedChoreResp
import com.app.familydays.webservice.responses.GetFinishedChoreResp
import org.koin.android.viewmodel.ext.android.viewModel
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*

class AssignedChoreAdapter(
    val context: Context,
    val onItemClicked: (GetAssignedChoreResp.Data) -> Unit,
    val onDeleteItemClicked:(GetAssignedChoreResp.Data)-> Unit,
    val onlongItemClicked:(GetAssignedChoreResp.Data)-> Unit
) :
    RecyclerView.Adapter<AssignedChoreAdapter.AssignedChoreViewHolder>() {

    //private val viewmodel:DeleteChoreVM by viewmodel()

    var choreRespList: List<GetAssignedChoreResp.Data>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }



    inner class AssignedChoreViewHolder(val binding: ItemAssignedChoreBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(position: Int) {

            binding.assignedchore = choreRespList?.get(position)



            if (choreRespList?.get(position)!!.isAdmin == 1 || GlobalClass.prefs.getValueString("role_id").toString() == "2" || GlobalClass.prefs.getValueString("role_id").toString() == "3" ) {
                if(choreRespList?.get(position)!!.isChecked){
//                binding.clmain.setBackgroundColor(Color.LTGRAY)
                    binding.clSelected.visibility=View.VISIBLE

                    if(GlobalClass.alSelectedPositionsreward.contains(choreRespList?.get(position)!!.choreId)){

                    }else{
                        GlobalClass.alSelectedPositionsreward.add(choreRespList?.get(position)!!.choreId)
                    }


                }else{
//                binding.clmain.setBackgroundColor(Color.TRANSPARENT)
                    binding.clSelected.visibility=View.GONE

                    GlobalClass.alSelectedPositionsreward.remove(choreRespList?.get(position)!!.choreId)

                }
                binding.ivDelete.visibility = View.VISIBLE
            } else {
                binding.ivDelete.visibility = View.GONE
            }

            if (choreRespList!![position].isConform == 0) {
                binding.tvTitle.setTextColor(context.resources.getColor(R.color.black))
                binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.black))

            } else {
                binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))

            }


            val due_dte= dueDateFormate(
                binding.assignedchore!!.dueDate).substring(0,dueDateFormate(
                binding.assignedchore!!.dueDate).length-9)


            val duetime: String = dueDateFormate(
                binding.assignedchore!!.dueDate).substring(dueDateFormate(
                binding.assignedchore!!.dueDate).length-9)
            Log.d("xxx", "Date:$due_dte\ntime:$duetime")



            if(binding.assignedchore?.isCreateby == 0){
                binding.tvDateTime.text =
                        " Created for " + binding.assignedchore!!.createBy +"\n "+"Due by " + due_dte+"\n"+StringUtils.center(duetime,duetime.length)

            }else{
                binding.tvDateTime.text =
                        " Created by " + binding.assignedchore!!.createBy +"\n "+"Due by " + due_dte+"\n"+ StringUtils.center(duetime,duetime.length)
            }



            if (choreRespList?.size!! > 1) {
                if (position == choreRespList?.size!! - 1) {
                    binding.vwLine.visibility = View.GONE
                }
            }

            binding.root.setOnClickListener {
                onItemClicked(binding.assignedchore!!)
            }
            binding.ivDelete.setOnClickListener {
                onDeleteItemClicked(binding.assignedchore!!)
            }

            binding.root.setOnLongClickListener(){
                onlongItemClicked(binding.assignedchore!!)
                true
            }
            binding.executePendingBindings()
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        AssignedChoreViewHolder(
                ItemAssignedChoreBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    override fun onBindViewHolder(
        holder: AssignedChoreAdapter.AssignedChoreViewHolder,
        position: Int
    ) {
        holder.bind(position)

        if (choreRespList!![position].isConform == 0) {
            holder.binding.tvTitle.setTextColor(Color.BLACK)
        }
    }

    override fun getItemCount() = choreRespList?.size ?: 0

    fun dueDateFormate(dueDate: String?): String {
        var spf = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val newDate: Date = spf.parse(dueDate)
        spf = SimpleDateFormat("MMM-dd-yyyy hh:mm aa")
        val newDateString: String = spf.format(newDate)
        Log.i("datetime", newDateString)
        return newDateString

    }
}


internal object StringUtils {
    @JvmOverloads
    fun center(s: String?, size: Int, pad: Char = ' '): String? {
        if (s == null || size <= s.length) return s
        val sb = StringBuilder(size)
        for (i in 0 until (size - s.length) / 2) {
            sb.append(pad)
        }
        sb.append(s)
        while (sb.length < size) {
            sb.append(pad)
        }
        return sb.toString()
    }
}
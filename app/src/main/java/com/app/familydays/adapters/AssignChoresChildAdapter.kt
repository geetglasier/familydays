package com.app.familydays.adapters

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.ItemAssignchorechildBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.ChildChoreResp
import java.text.SimpleDateFormat
import java.util.*

class AssignChoresChildAdapter(val context: Context,
                               val onItemClicked: (ChildChoreResp.Assign_chore) -> Unit):
RecyclerView.Adapter<AssignChoresChildAdapter.assignchoreChildViewHolder>()  {

    var choreRespList: List<ChildChoreResp.Assign_chore>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class assignchoreChildViewHolder(val binding: ItemAssignchorechildBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {

            binding.assignchore = choreRespList?.get(position)

            if (choreRespList?.get(position)!!.is_admin == 1 || GlobalClass.prefs.getValueString("role_id").toString() == "2" || GlobalClass.prefs.getValueString("role_id").toString() == "3" ) {
                binding.ivDelete.visibility = View.VISIBLE
            } else {
                binding.ivDelete.visibility = View.GONE
            }

            if (choreRespList!![position].is_conform == 0) {
                binding.tvTitle.setTextColor(context.resources.getColor(R.color.black))
                binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.black))

            } else {
                binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))

            }

            val due_dte= dueDateFormate(
                binding.assignchore!!.due_date).substring(0,dueDateFormate(
                binding.assignchore!!.due_date).length-9)


            val duetime: String = dueDateFormate(
                binding.assignchore!!.due_date).substring(dueDateFormate(
                binding.assignchore!!.due_date).length-9)
            Log.d("xxx", "Date:$due_dte\ntime:$duetime")



            if(binding.assignchore?.is_createby == 0){
                binding.tvDateTime.text =
                    " Created for " + binding.assignchore!!.create_by +"\n "+"Due by " + due_dte+"\n"+StringUtils.center(duetime,duetime.length)

            }else{
                binding.tvDateTime.text =
                    " Created by " + binding.assignchore!!.create_by +"\n "+"Due by " + due_dte+"\n"+ StringUtils.center(duetime,duetime.length)
            }





            if (choreRespList?.size!! > 1) {
                if (position == choreRespList?.size!! - 1) {
                    binding.vwLine.visibility = View.GONE
                }
            }

            binding.root.setOnClickListener {
                onItemClicked(binding.assignchore!!)
            }

            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
    assignchoreChildViewHolder(
    ItemAssignchorechildBinding
            .inflate(LayoutInflater.from(parent.context),parent, false
        )
    )

    override fun onBindViewHolder(
        holder: AssignChoresChildAdapter.assignchoreChildViewHolder,
        position: Int
    ) {
        holder.bind(position)

        if (choreRespList!![position].is_conform== 0) {
            holder.binding.tvTitle.setTextColor(Color.BLACK)
        }
    }

    override fun getItemCount() = choreRespList?.size ?: 0

    fun dueDateFormate(dueDate: String?): String {
        var spf = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val newDate: Date = spf.parse(dueDate)
        spf = SimpleDateFormat("MMM-dd-yyyy hh:mm aa")
        val newDateString: String = spf.format(newDate)
        Log.i("datetime", newDateString)
        return newDateString

    }


}
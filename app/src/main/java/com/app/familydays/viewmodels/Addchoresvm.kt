package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers


class Addchoresvm(private val mainRepository: MainRepository):ViewModel() {
    fun addchores(token:String,icon_name:String,title:String,chore_time:String,child_id:String,is_daily_chore:String,set_point:String,date_time:String)=
        liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
            try {
                emit(Resource.success(data = mainRepository.addchores(token,icon_name,title,chore_time,child_id,is_daily_chore,set_point,date_time)))
            } catch (exception: Exception) {
                exception.printStackTrace()
                emit(Resource.error(data = null, exception = exception))
            }



    }

}
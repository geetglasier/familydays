package com.app.familydays.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
   import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.ItemChoreListBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.GetAssignedChoreResp
import com.app.familydays.webservice.responses.GetChildMemberDetailResp
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*

class ChildAssignChoreAdapter(
    val context: Context,
    val onItemClicked: (GetChildMemberDetailResp.AssignChore) -> Unit,
    val onDeleteItemClicked:(GetChildMemberDetailResp.AssignChore)-> Unit
) :
    RecyclerView.Adapter<ChildAssignChoreAdapter.CategoriesViewHolder>() {
    var choreList: List<GetChildMemberDetailResp.AssignChore>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class CategoriesViewHolder(val binding: ItemChoreListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(position: Int) {
            binding.assignedchore =  choreList?.get(position)

            if ( choreList?.get(position)!!.isAdmin == 1 || GlobalClass.prefs.getValueString("role_id").toString() == "2" || GlobalClass.prefs.getValueString("role_id").toString() == "3" ) {
                binding.ivDelete.visibility = View.VISIBLE
            } else {
                binding.ivDelete.visibility = View.GONE
            }

            if ( choreList!![position].isConform == 0) {
                binding.tvTitle.setTextColor(context.resources.getColor(R.color.black))
                binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.black))

            } else {
                binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))

            }


            val due_dte= dueDateFormate(
                binding.assignedchore!!.dueDate).substring(0,dueDateFormate(
                binding.assignedchore!!.dueDate).length-9)


            val duetime: String = dueDateFormate(
                binding.assignedchore!!.dueDate).substring(dueDateFormate(
                binding.assignedchore!!.dueDate).length-9)
            Log.d("xxx", "Date:$due_dte\ntime:$duetime")



            if(binding.assignedchore?.isCreateby == 0){
                binding.tvDateTime.text =
                    " Created for " + binding.assignedchore!!.createBy +"\n "+"Due by " + due_dte+"\n"+StringUtils.center(duetime,duetime.length)

            }else{
                binding.tvDateTime.text =
                    " Created by " + binding.assignedchore!!.createBy +"\n "+"Due by " + due_dte+"\n"+ StringUtils.center(duetime,duetime.length)
            }



            if ( choreList?.size!! > 1) {
                if (position ==  choreList?.size!! - 1) {
                    binding.vwLine.visibility = View.GONE
                }
            }

            binding.root.setOnClickListener {
                onItemClicked(binding.assignedchore!!)
            }
            binding.ivDelete.setOnClickListener {
                onDeleteItemClicked(binding.assignedchore!!)
            }
            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CategoriesViewHolder(
            ItemChoreListBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount() = choreList?.size ?: 0


    fun dueDateFormate(dueDate: String?): String {
        var spf = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val newDate: Date = spf.parse(dueDate)
        spf = SimpleDateFormat("MMM-dd-yyyy hh:mm aa")
        val newDateString: String = spf.format(newDate)
        Log.i("datetime", newDateString)
        return newDateString

    }
    internal object StringUtils {
        @JvmOverloads
        fun center(s: String?, size: Int, pad: Char = ' '): String? {
            if (s == null || size <= s.length) return s
            val sb = StringBuilder(size)
            for (i in 0 until (size - s.length) / 2) {
                sb.append(pad)
            }
            sb.append(s)
            while (sb.length < size) {
                sb.append(pad)
            }
            return sb.toString()
        }

    }



}




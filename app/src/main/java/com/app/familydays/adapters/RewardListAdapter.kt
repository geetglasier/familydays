package com.app.familydays.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.get
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.RewardItemBinding
import com.app.familydays.fragments.RewardFragment
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status

import com.app.familydays.webservice.responses.GetRewardResp
import java.text.SimpleDateFormat
import java.util.*

class RewardListAdapter(
    val context: Context,
    val onItemClicked: (GetRewardResp.Data) -> Unit,
    val onclaimnowClicked: (GetRewardResp.Data) -> Unit,
    val onconfirmClicked: (GetRewardResp.Data) -> Unit,
    val onDeleteItemClicked: (GetRewardResp.Data) -> Unit,
    val from: String,
    val onlongItemClicked: (GetRewardResp.Data) -> Unit

) :
    RecyclerView.Adapter<RewardListAdapter.RewardListViewHolder>() {


    var rewardRespList:List<GetRewardResp.Data>? = null
        set(value) {
            field = value
            notifyDataSetChanged()

        }

    var isconfirm = 0
    var isLongPressed = false


    inner class RewardListViewHolder(val binding: RewardItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n", "ResourceAsColor")
        fun bind(position: Int) {

            binding.rewardlist = rewardRespList?.get(position)

            if (rewardRespList?.get(position)!!.isAdmin == 1 || GlobalClass.prefs.getValueString("role_id")
                    .toString() == "2" || GlobalClass.prefs.getValueString("role_id")
                    .toString() == "3"
            ) {

                binding.llConfirm.isEnabled = true

                if(rewardRespList?.get(position)!!.isChecked){
//                binding.clmain.setBackgroundColor(Color.LTGRAY)
                    binding.clSelected.visibility=View.VISIBLE

                    if(GlobalClass.alSelectedPositionsreward.contains(rewardRespList?.get(position)!!.rewardId)){

                    }else{
                        GlobalClass.alSelectedPositionsreward.add(rewardRespList?.get(position)!!.rewardId)
                    }


                }else{
//                binding.clmain.setBackgroundColor(Color.TRANSPARENT)
                    binding.clSelected.visibility=View.GONE

                    GlobalClass.alSelectedPositionsreward.remove(rewardRespList?.get(position)!!.rewardId)

                }

                binding.ivDelete.visibility = View.VISIBLE
            } else {
                binding.ivDelete.visibility = View.GONE
//                binding.llConfirm.visibility = View.GONE
                binding.llConfirm.isEnabled = false

            }

            if (binding.rewardlist!!.token == GlobalClass.prefs.getValueString(("token")) && binding.rewardlist!!.isConform == 0) {
                binding.claimreward.visibility = View.VISIBLE
                binding.llConfirm.visibility = View.GONE

            } else {
                binding.claimreward.visibility = View.GONE
                binding.llConfirm.visibility = View.VISIBLE
            }

            if (from == "profile") {
                binding.claimreward.visibility = View.GONE
                binding.ivDelete.visibility = View.GONE
//                binding.llConfirm.visibility = View.GONE

            }

            if (binding.rewardlist!!.isConform == 0) {
                binding.chkconfirm.setImageResource(R.drawable.ic_rec_checked)


            } else {
                binding.chkconfirm.setImageResource(R.drawable.ic_rec_unchecked)
                binding.tvTitle.setTextColor(R.color.colorPrimary)
                binding.tvcreated.setTextColor(R.color.clr_gray_light)
                binding.tvdue.setTextColor(R.color.clr_gray_light)
                binding.tvPoint.setTextColor(R.color.clr_gray_light)
                binding.tvrewardconfirm.setTextColor(R.color.clr_gray_light)
//                binding.llConfirm.isEnabled = true

            }

            if (binding.rewardlist?.is_createby == "P") {
                binding.tvcreated.text =
                    "Created for " + binding.rewardlist!!.create_by
            } else {
                binding.tvcreated.text =
                    "Created by " + binding.rewardlist!!.create_by
            }
            binding.tvdue.text = "Claim by " + dueDateFormate(binding.rewardlist!!.dueDate)
            binding.tvPoint.text = binding.rewardlist!!.point.toString() + " " + "pts"


            if (rewardRespList?.size!! > 1) {
                if (position == rewardRespList?.size!! - 1) {
                    binding.vwLine.visibility = View.GONE
                }
            }


            binding.llConfirm.setOnClickListener {
                onconfirmClicked(binding.rewardlist!!)
            }
            binding.ivDelete.setOnClickListener {
                onDeleteItemClicked(binding.rewardlist!!)
            }

            binding.claimreward.setOnClickListener {
                onclaimnowClicked(binding.rewardlist!!)
            }

            binding.root.setOnClickListener(){
                onItemClicked(binding.rewardlist!!)
            }
            binding.root.setOnLongClickListener(){
                onlongItemClicked(binding.rewardlist!!)
                true
            }


            binding.executePendingBindings()
        }


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        RewardListViewHolder(
            RewardItemBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    override fun onBindViewHolder(
        holder: RewardListViewHolder,
        position: Int
    ) {
        holder.bind(position)

        /*   if (rewardRespList!![position].isConform == 0) {
               holder.binding.tvTitle.setTextColor(Color.BLACK)
           }*/
    }

    override fun getItemCount() = rewardRespList?.size ?: 0


    fun dueDateFormate(dueDate: String?): String {
        var spf = SimpleDateFormat("dd-MM-yyyy")
        val newDate: Date = spf.parse(dueDate)
        spf = SimpleDateFormat("MMM-dd-yyyy")
        val newDateString: String = spf.format(newDate)
        Log.i("datetime", newDateString)
        return newDateString

    }


}
package com.app.familydays.webservice.requests

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody

data class AddFamilyReq (
    var username: String = "",
    var family_name: String = "",
    var fullname: String = "",
    var email: String = "",
    var phone_no: String = "",
    var password: String = "",
    var birth_date: String = "",
    var rolename: String = "",
    var role: String = "",
    var is_admin: String = "",
    var device_token: String = "",
    var profile_file: String = "",
) {
    fun toMap() =
        ((Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(AddFamilyReq::class.java)
            .toJsonValue(this) as Map<String, *>).mapValues
        { (it.value as String).toRequestBody("text/plain".toMediaType()) })
}
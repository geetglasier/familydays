package com.app.familydays.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.app.familydays.R
import com.app.familydays.webservice.responses.RewardBrandResp
import kotlinx.android.synthetic.main.item_brandname.view.*

class BrandArrayAdapter(ctx: Context,
                        moods: List<RewardBrandResp.Data>) :
    ArrayAdapter<RewardBrandResp.Data>(ctx, 0, moods) {

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {

        val mood = getItem(position)

        val view = recycledView ?: LayoutInflater.from(context).inflate(
            R.layout.item_brandname,
            parent,
            false
        )

//        view.moodImage.setImageResource(mood.image)
        view.tvBrandname.text=mood!!.brand_name

        return view
    }
}
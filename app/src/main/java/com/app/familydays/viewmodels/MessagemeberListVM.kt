package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import com.app.familydays.webservice.responses.AddFamilyMemberResp
import com.squareup.moshi.Json
import kotlinx.coroutines.Dispatchers

class MessagemeberListVM(
    private val mainRepository: MainRepository
) : ViewModel() {

    fun getFamilyMemberlistmsg(token: String,notification_id:String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getFamilyMemberListmsg(token,notification_id)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

    fun getMessages(token: String,user_id:String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getmessage(token,user_id)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

}


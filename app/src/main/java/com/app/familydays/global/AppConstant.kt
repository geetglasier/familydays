package com.app.familydays.global

object AppConstant {

  val CODE_SUCCESS=200
  val DATANOTFOUND=402
  val UNAUTHORISED=401
  val BAD_REQUEST=400
  val TOO_MANY_REQUEST=429
  val TOO_VALIDATION=101


}
package com.app.familydays.global

import android.app.Application
import com.app.familydays.webservice.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class GlobalClass:Application() {

    companion object{
        lateinit var prefs: SharePrefranceGlobal

        var setTitle=""

      var  chore_type = "0"
        var selectepos=-1
        var isselected=0
        var isimgselected=0
        var selectedchild: ArrayList<String> = ArrayList()
        var selectedChildId: ArrayList<Int> = ArrayList()
        var selectedimage_url:String=""
        var url:String=""
        var start_date=""
        var end_date=""
        var clear=""
        var alSelectedPositionsreward: ArrayList<Int> = ArrayList()
        var index: Int? = null
        var isclicked=false
        var page =0
        var clearfilter=0   //0=flase 1=true


    }

    override fun onCreate() {
        prefs = SharePrefranceGlobal(applicationContext)
        super.onCreate()

        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@GlobalClass)
            modules(mainModule)
        }
    }
}
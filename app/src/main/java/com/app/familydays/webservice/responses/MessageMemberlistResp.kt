package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

class MessageMemberlistResp (
    @Json(name = "data")
    val `data`: List<Data>,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean,
    @Json(name = "user_info")
    val user_info: User_info)
{


    data class User_info(
        @Json(name = "user_id")
        val user_id: Int,
        @Json(name = "username")
        val username: String,
        @Json(name = "email")
        val email: String,
        @Json(name = "token")
        val token: String,
        @Json(name = "role")
        val role: Int,
        @Json(name = "role_type")
        val role_type: String,
        @Json(name = "total_point")
        val total_point: Int,
        @Json(name = "profile_url")
        val profile_url: String)



    data class Data(
        @Json(name = "user_id")
        val user_id: Int,
        @Json(name = "full_name")
        val full_name: String,
        @Json(name = "email")
        val email: String,
        @Json(name = "token")
        val token: String,
        @Json(name = "role")
        val role: Int,
        @Json(name = "role_type")
        val role_type: String,
        @Json(name = "total_point")
        val total_point: Int,
        @Json(name = "is_read")
        val is_read: Int,
        @Json(name = "message_count")
        val message_count:Int,
        @Json(name = "profile_url")
        val profile_url: String)











}
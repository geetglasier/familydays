package com.app.familydays.activities

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.app.familydays.R
import com.app.familydays.databinding.ActivityLoginBinding
import com.app.familydays.databinding.ActivityMainBinding
import com.app.familydays.global.AppConstant
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.utils.unAuthorisedUserDialog
import com.app.familydays.viewmodels.LoginVm
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.regex.Pattern

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private val viewModel: LoginVm by viewModel()
    private lateinit var loadingIndicator: KProgressHUD

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        val validator = Validator(binding)
        loadingIndicator = getLoadingIndicator(this)

        binding.ivBack.setOnClickListener {
            finish()
        }

        binding.btnLogin.setOnClickListener {

           if(binding.edtEmail.text.isEmpty()){
               Toast.makeText(applicationContext, "Please enter valid E-mail", Toast.LENGTH_SHORT).show()
           }else if(binding.edtPassword.text.isEmpty()){
               Toast.makeText(applicationContext, "Please enter password", Toast.LENGTH_SHORT).show()
           }else if(binding.edtPassword.text.length<6){
            Toast.makeText(applicationContext, "Password must be 6 character", Toast.LENGTH_SHORT).show()
           }else{

                   if (isNetworkConnected(baseContext))
                       viewModel.login(
                           binding.edtEmail.text.toString(),
                           binding.edtPassword.text.toString(),
                           GlobalClass.prefs.getValueString("devicetoken").toString(),
                           "2"
                       ).observe(
                           this
                       ) { resource ->
                           when (resource.status) {
                               Status.SUCCESS -> {
                                   loadingIndicator.dismiss()
                                   resource.data?.let { response ->
                                       if (response.status) {
                                           GlobalClass.prefs.SaveString("token", response.data!!.token)
                                           GlobalClass.prefs.SaveString(
                                               "id",
                                               response.data.userId.toString()
                                           )
                                           GlobalClass.prefs.SaveString(
                                               "username",
                                               response.data.fullName
                                           )
                                           GlobalClass.prefs.SaveString("email", response.data.email)
                                           GlobalClass.prefs.SaveString(
                                               "profilePic",
                                               response.data.profileUrl
                                           )
                                           GlobalClass.prefs.SaveString("isadmin",response.data.isAdmin.toString())
                                           if (response.data.role == 2) {
                                               GlobalClass.prefs.SaveString("role", "Father")

                                           } else if (response.data.role == 3) {
                                               GlobalClass.prefs.SaveString("role", "Mother")

                                           } else if (response.data.role==4) {
                                               GlobalClass.prefs.SaveString("role", "Child")
                                           }else if (response.data.role==5) {
                                               GlobalClass.prefs.SaveString("role", "Child")
                                           }
                                           GlobalClass.prefs.SaveString("role_id",response.data.role.toString())
                                           GlobalClass.prefs.SaveString("userid", response.data.userId.toString())

                                           if (response.data.isReset==1){
                                               startActivity(
                                                   Intent(
                                                       baseContext,
                                                       ChnagePasswordActivity::class.java
                                                   ).addFlags(
                                                       Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                   )
                                               )
                                               finish()
                                           }else{
                                               startActivity(
                                                   Intent(
                                                       baseContext,
                                                       DashboardActivity::class.java
                                                   ).addFlags(
                                                       Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                   )
                                               )
                                               finish()
                                           }


                                       } else {

                                           Toast.makeText(
                                               this,
                                               response.message,
                                               Toast.LENGTH_LONG
                                           ).show()

                                       }


                                   }

                               }
                               Status.ERROR -> {
                                   loadingIndicator.dismiss()

                               }
                               Status.LOADING -> {
                                   loadingIndicator.show()
                               }
                           }


                       }



           }
        }
    }

    fun forgotPassword(view: View) {
        val intent = Intent(this, ForgotPasswordActivity::class.java)
        startActivity(intent)
    }

    fun createFamily(view: View) {
        val intent = Intent(this, CreateFamilyActivity::class.java)
        startActivity(intent)
    }

    fun openContactUs(view: View) {
        val intent = Intent(this, ContactActivity::class.java)
        startActivity(intent)
    }
}

package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

class ChoresCompletedResp(

    @Json(name = "status")
    val status: Boolean,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "is_complete")
    val is_complete: String,)  {
}
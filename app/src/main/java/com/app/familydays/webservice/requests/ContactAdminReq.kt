package com.app.familydays.webservice.requests

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody

data class ContactAdminReq (
    var full_name: String = "",
    var email: String = "",
    var phone: String = "",
    var subject: String = "",
    var message: String = "",
    var media_file: String = "",
) {
    fun toMap() =
        ((Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(ContactAdminReq::class.java)
            .toJsonValue(this) as Map<String, *>).mapValues
        { (it.value as String).toRequestBody("text/plain".toMediaType()) })
}
package com.app.familydays.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.FragmentAddNewRewardBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.viewmodels.*
import com.app.familydays.webservice.responses.RewardBrandResp
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.bottomsheet_layout.view.*
import kotlinx.android.synthetic.main.fragment_add_new_chore.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import com.app.familydays.utils.getLoadingIndicator
import android.widget.AdapterView
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.app.familydays.adapters.*
import kotlinx.android.synthetic.main.webviewdialog.view.*
import kotlin.collections.ArrayList
import android.graphics.Bitmap
import android.view.KeyEvent
import android.webkit.WebResourceRequest

import android.webkit.WebViewClient
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import kotlinx.android.synthetic.main.fragment_add_new_chore.txtpoints
import kotlinx.android.synthetic.main.fragment_add_new_chore.txttimeframe
import kotlinx.android.synthetic.main.fragment_add_new_reward.*


class AddNewRewardFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    private lateinit var binding: FragmentAddNewRewardBinding
    private val viewModel: AddRewardVM by viewModel()
    private val viewModelchild: Family_Child_ListVm by viewModel()
    private val viewModel2: PopularChoreListVm by viewModel()
    private val viewModelsubbrand: GetRewardBrandVM by viewModel()


    private val args: AddNewRewardFragmentArgs by navArgs()
    private lateinit var loadingIndicator: KProgressHUD
    var day = 0
    var month: Int = 0
    var year: Int = 0

    var myday = 0
    var myMonth: Int = 0
    var myYear: Int = 0

    lateinit var time: String
    var date = Calendar.getInstance()


    var listbrand: ArrayList<RewardBrandResp.Data>? = null
    var choreIcon = ""

    var bottomSheetDialog: BottomSheetDialog? = null
    var editText: TextView? = null
    var editTextpoints: TextView? = null

    var txttitle: TextView? = null
    var webview: WebView? = null

    var btnclose: TextView? = null

    val selectedChild = StringBuilder()

    var selectedDate: LocalDate? = null
    var currentDate: LocalDate? = null
    var max: Int = 0
    var min: Int = 0
    var category_id = 0//reward type custom or list
    var brand_name = ""
    var frame_date = ""
    var url = ""
    var title = ""

    var brand_icon = ""
    var cat_id = 0


    companion object {

        lateinit var instance: AddNewRewardFragment

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    @SuppressLint("ResourceAsColor")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_add_new_reward, container, false)
        binding.args = args
        loadingIndicator = getLoadingIndicator(requireContext())

        isCancelable = false
        listbrand = ArrayList()

        familychildlist()

        binding.pointsll.setOnClickListener() {
            showcustomdialog()
        }
        getrewardlist()

        instance = this
        // category_id=args.rewardId.toInt()

        binding.etchoretitle.isEnabled=false


        if (args.selectedreward.isNotEmpty()&&category_id!=1) {
            Log.e("xxx", args.rewardId)
            cat_id = args.rewardId.toInt()
            GlobalClass.selectedimage_url=""
            binding.chkreward.setImageResource(R.drawable.ic_rec_checked)
            binding.chkcustom.setImageResource(R.drawable.ic_rec_unchecked)

            binding.etchoretitle.isEnabled = false



        }



        DrawableCompat.setTint(
            binding.etcustomtitle.getBackground(),
            ContextCompat.getColor(requireContext(), R.color.white)
        )

        binding.llCustomerreward.setOnClickListener() {

            category_id = 1

            cat_id = 1

            binding.chkreward.setImageResource(R.drawable.ic_rec_unchecked)
            binding.chkcustom.setImageResource(R.drawable.ic_rec_checked)

            binding.spnrbrandname.visibility = View.GONE

            binding.etcustomtitle.visibility = View.VISIBLE
            binding.rvimgitem.visibility = View.INVISIBLE
            binding.etchoretitle.isEnabled = true

            binding.etchoretitle.setText("")
            binding.etchoretitle.hint = "Please enter reward name"
            binding.etcustomtitle.hint = "Please enter brand name"

            brand_icon = ""

        }

        val today = Date()
        val format = SimpleDateFormat("MMM-dd-yyyy ")
        var dateToStr = format.format(today)
        binding.txttimeframe.setText(dateToStr)

        binding.imgClose.setOnClickListener {
            dialog?.dismiss()
            GlobalClass.selectepos = -1
            GlobalClass.isselected = 0
            GlobalClass.selectedChildId = java.util.ArrayList()
            GlobalClass.selectedimage_url = ""
            GlobalClass.setTitle = ""
            GlobalClass.selectedchild= java.util.ArrayList()


        }

        binding.txtsave.setOnClickListener() {


            Log.d("xxx",category_id.toString())
            Log.d("xxx",brand_icon)

            if (brand_icon == "" && category_id == 1) {

                brand_icon = "Other"
                brand_name=binding.etcustomtitle.text.toString()
            } else {
                brand_icon = GlobalClass.selectedimage_url
                Log.e("xxx", GlobalClass.selectedimage_url)
            }



            if (binding.etchoretitle.text.isEmpty()) {
                Toast.makeText(
                    requireContext(),
                    "Please enter reward name",
                    Toast.LENGTH_SHORT
                ).show()

            } else if (brand_name == "") {
                Toast.makeText(
                    requireContext(),
                    "Please select brand name",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (category_id == 0 && brand_icon == "") {
                Toast.makeText(
                    requireContext(),
                    "Please select product",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (GlobalClass.selectedChildId.size == 0) {
                Toast.makeText(
                    requireContext(),
                    "Please select atleast one child",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                if (category_id == 1) {
                    brand_name = etcustomtitle.text.toString()
                }

                addrewardapi()

            }


        }

        binding.txttimeframe.setOnClickListener() {
            //Toast.makeText(requireContext(), "Date Picker", Toast.LENGTH_SHORT).show()
            datepickerdialog()
//            showDateTimePicker()
        }

        if (GlobalClass.prefs.getValueString("role_id") == "4" || GlobalClass.prefs.getValueString("role_id") == "5") {
            binding.rvChildMember.visibility = View.GONE
            binding.txtsetchild.visibility = View.GONE
            GlobalClass.selectedChildId.add(GlobalClass.prefs.getValueString("userid")!!.toInt())

        }

        binding.etchoretitle.setOnClickListener() {
            binding.chkreward.setImageResource(R.drawable.ic_rec_unchecked)
            binding.chkcustom.setImageResource(R.drawable.ic_rec_checked)
        }

        binding.spnrbrandname.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                // your code here
                val imageurls = parentView!!.getItemAtPosition(position)

                val sub = imageurls as RewardBrandResp.Data


                Log.e("selectname", imageurls.toString())

                brand_name = imageurls.brand_name

                GlobalClass.selectedimage_url=""





                binding.rvimgitem.adapter = BrandImageAdapter(requireContext(),"addnew").apply {
                    brandList = imageurls.sub_brands
                }


            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here

            }


        }
        if (args.selectedreward == "Other") {
            binding.etchoretitle.hint = "Please enter reward title"
            binding.chkreward.setImageResource(R.drawable.ic_rec_unchecked)
            binding.chkcustom.setImageResource(R.drawable.ic_rec_checked)
        } else {
            binding.etchoretitle.setText(args.selectedreward)
            Log.d("XXX", args.rewardId)
            if (args.rewardId.isNotEmpty()) {
                getrewardbrand()
            }

        }
        binding.llreward.setOnClickListener {

            binding.chkreward.setImageResource(R.drawable.ic_rec_checked)
            binding.chkcustom.setImageResource(R.drawable.ic_rec_unchecked)

            binding.spnrbrandname.visibility = View.VISIBLE

            binding.etcustomtitle.visibility = View.GONE
            binding.rvimgitem.visibility = View.VISIBLE

            cat_id = 0
            category_id = 0
            GlobalClass.setTitle = "Reward"
            findNavController().navigate(
                PopularChoreListFragmentDirections.actionGlobalNavPopularChore(
                    "reward",
                    "",
                    ""
                )
            )


        }

        return binding.root
    }

    private fun datepickerdialog() {
        val calendar = Calendar.getInstance()

        year = calendar[Calendar.YEAR]
        month = calendar[Calendar.MONTH]
        day = calendar[Calendar.DAY_OF_MONTH]
        val datePickerDialog =
            DatePickerDialog(requireActivity(), R.style.DialogTheme, this, year, month, day)
        datePickerDialog.datePicker.minDate = calendar.timeInMillis
        datePickerDialog.show()
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )
        )
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )
        )
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000

    }


    fun familychildlist() {

        viewModelchild.getChildlist(GlobalClass.prefs.getValueString("token").toString())
            .observe(this) { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        loadingIndicator.dismiss()

                        resource.data?.let { response ->
                            if (response.status) {
                                binding.rvChildMember.adapter = ChildListAdapter(
                                ).apply {
                                    childList = response.data
                                }

                            }
                        }

                    }
                    Status.ERROR -> loadingIndicator.dismiss()

                    Status.LOADING -> loadingIndicator.show()
                }

            }

    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun getrewardbrand() {
        if (isNetworkConnected(requireContext())) {
            viewModelsubbrand.getRewardBrand(
                args.rewardId
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {


//                                    min=response.minimum_point
//                                    max=response.maximum_point
                                    val subBrand: ArrayList<String> = ArrayList()
                                    for (i in response.data.indices) {
                                        subBrand.add(response.data[i].brand_name)

                                    }



                                    listbrand!!.clear()
                                    listbrand!!.addAll(response.data)

                                    val adapter =
                                        BrandArrayAdapter(requireActivity(), response.data)
                                    binding.spnrbrandname.adapter = adapter


                                }


                            }
                        }

                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()

                    }

                }

        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getrewardlist() {

        if (isNetworkConnected(requireContext())) {
            viewModel2.getRewardcategoryList(
                GlobalClass.prefs.getValueString("token").toString()
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    max = response.maximumPoint
                                    min = response.minimumPoint
                                    binding.minimumpts.text = min.toString()
                                    binding.maximumpts.text = max.toString()
                                    binding.txtpoints.text = min.toString()
                                }


                            }
                        }

                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()

                    }

                }

        }

    }


    private fun showcustomdialog() {

        val viewGroup: ViewGroup? = requireView().findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(requireContext())
            .inflate(R.layout.bottomsheet_layout, viewGroup, false)
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())


        //setting the view of the builder to our custom view that we already inflated

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it

        //finally creating the alert dialog and displaying it
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
        editTextpoints = dialogView.findViewById<TextView>(R.id.etpoints)
        // editText.filters = arrayOf<InputFilter>(MinMaxFilter("1", "500"))
        editTextpoints!!.setText(binding.txtpoints.text.toString())



        dialogView.imgback.setOnClickListener() {
            alertDialog.dismiss()
            GlobalClass.selectedimage_url = ""

        }
        val pointslist: MutableList<String> = ArrayList()
        for (i in min..max) {
            pointslist.add(i.toString())
            // Log.d("xxx",pointslist.size.toString())
        }

        dialogView.etpoints.setOnClickListener() {

            bottomSheetDialog = BottomSheetDialog(requireContext())
            val btnsheet = layoutInflater.inflate(R.layout.bottomsheet, null)


            val rec = btnsheet.findViewById<RecyclerView>(R.id.rvpoints)

            rec.layoutManager = LinearLayoutManager(requireContext())
            val adapter = PointsAdapter(requireContext(), pointslist, "Addnewreward")
            rec.adapter = adapter

            bottomSheetDialog!!.setContentView(btnsheet)

//            val bottomSheetBehavior = BottomSheetBehavior.from(btnsheet.getParent() as View)
//            bottomSheetBehavior.peekHeight = 500

            bottomSheetDialog!!.show()
        }
        dialogView.btnsave.setOnClickListener() {
            txtpoints.text = editTextpoints!!.text.toString()

            alertDialog.dismiss()


        }


    }


    fun showwebviewdialog(url: String) {

        val viewGroup: ViewGroup? = requireView().findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(requireContext())
            .inflate(R.layout.webviewdialog, viewGroup, false)
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())


        //setting the view of the builder to our custom view that we already inflated

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it

        //finally creating the alert dialog and displaying it
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
        txttitle = dialogView.findViewById<TextView>(R.id.textviewtitle)
        btnclose = dialogView.findViewById(R.id.textviewclose)
        webview = dialogView.findViewById(R.id.webview)


        // editText.filters = arrayOf<InputFilter>(MinMaxFilter("1", "500"))
        // editTextpoints!!.setText(binding.txtpoints.text.toString())


        val webViewClient: WebViewClient = object : WebViewClient() {

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(request?.url.toString())
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {

                super.onPageStarted(view, url, favicon)


            }

            override fun onPageFinished(view: WebView?, url: String?) {

                txttitle!!.text = view?.title
                loadingIndicator.dismiss()
                super.onPageFinished(view, url)
            }
        }
        webview!!.webViewClient = webViewClient
//             binding.webview.settings.defaultTextEncodingName = "utf-8"
        webview!!.settings.javaScriptEnabled = true
        webview!!.settings.javaScriptCanOpenWindowsAutomatically = true
        webview!!.settings.domStorageEnabled = true

        webview!!.loadUrl(url)



        webview!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action === KeyEvent.ACTION_DOWN) {
                    val webView = v as WebView
                    when (keyCode) {
                        KeyEvent.KEYCODE_BACK -> if (webView.canGoBack()) {
                            webView.goBack()
                            return true
                        }
                    }
                }
                return false
            }
        })

        dialogView.textviewclose.setOnClickListener() {
            alertDialog.dismiss()
        }


    }


    private fun setupWebView() {


    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {

        myYear = year
        myday = dayOfMonth
        myMonth = month + 1

        val c = Calendar.getInstance()




        selectedDate = LocalDate.of(myYear, myMonth, myday)
        currentDate =
            LocalDate.of(c[Calendar.YEAR], c[Calendar.MONTH] + 1, c[Calendar.DAY_OF_MONTH])

        Log.e("selecteddate", selectedDate.toString() + "=======" + currentDate.toString())

//set date here
        txttimeframe.setText(parseDateTime("$myYear-$myMonth-$myday"))

    }


    fun parseDateTime(inputDateTime: String?): String? {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "MMM-dd-yyyy"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.US)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.US)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun addrewardapi() {

        var prefix = ""
        for (str in GlobalClass.selectedChildId.distinct()) {
            selectedChild.append(prefix)
            prefix = ","
            selectedChild.append(str)
        }
        frame_date = binding.txttimeframe.text.toString()


        val today = Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val dateToStr = format.format(today)

        Log.e("date", dateToStr)


        if (!isNetworkConnected(requireContext())) {
            Toast.makeText(
                requireContext(),
                "please check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        } else {


            viewModel.AddReward(
                GlobalClass.prefs.getValueString("token").toString(),
                cat_id.toString(),
                binding.etchoretitle.text.toString(),
                brand_name,
                brand_icon,
                finalDateTime(frame_date).toString(),
                binding.txtpoints.text.toString(),
                selectedChild.toString(), dateToStr
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {
                                    Toast.makeText(
                                        requireContext(),
                                        "Reward created successfully!",
                                        Toast.LENGTH_SHORT
                                    ).show()

                                    GlobalClass.selectepos = -1
                                    GlobalClass.isselected = 0
                                    GlobalClass.selectedChildId = ArrayList()
                                    GlobalClass.selectedimage_url = ""
                                    GlobalClass.selectedchild=ArrayList()
                                    GlobalClass.setTitle=""


                                    setFragmentResult(
                                        "refresh",
                                        bundleOf("fromAddreward" to true)
                                    )
                                    findNavController().navigateUp()
                                    GlobalClass.selectedChildId = ArrayList()

                                } else {
                                    Toast.makeText(
                                        requireContext(),
                                        response.message,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    GlobalClass.setTitle=""

                                }


                            }
                        }

                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()

                    }

                }
        }

    }

    fun finalDateTime(inputDateTime: String?): String? {
        val inputPattern = "MMM-dd-yyyy"
        val outputPattern = "dd-MM-yyyy"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.US)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.US)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

}
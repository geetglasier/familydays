package com.app.familydays.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import com.app.familydays.webservice.responses.GetRewardResp
import kotlinx.coroutines.Dispatchers

class RewardlistVM(
    private val mainRepository: MainRepository
) : ViewModel() {
    fun getRewardList(token: String, load_more: String, date_time: String) = liveData(
        Dispatchers.IO
    ) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.rewardlist(token, load_more, date_time)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

    fun getRewardListuserwise(token: String, load_more: String, date_time: String) = liveData(
        Dispatchers.IO
    ) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = mainRepository.rewardlistuserwise(
                        token,
                        load_more,
                        date_time
                    )
                )
            )
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }
}
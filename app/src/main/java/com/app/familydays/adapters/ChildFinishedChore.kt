package com.app.familydays.adapters

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.ItemChoreListBinding
import com.app.familydays.databinding.ItemFinishedchorelistBinding
import com.app.familydays.fragments.DashboardFragment
import com.app.familydays.fragments.MoreChoresFragment
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.GetChildMemberDetailResp
import com.app.familydays.webservice.responses.GetFinishedChoreResp
import java.text.SimpleDateFormat
import java.util.*

class ChildFinishedChore( val from:String,
                          val context: Context,
                          val onItemClicked: (GetChildMemberDetailResp.FinishChore) -> Unit
) :
    RecyclerView.Adapter<ChildFinishedChore.CategoriesViewHolder>() {
    var choreList: List<GetChildMemberDetailResp.FinishChore>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    inner class CategoriesViewHolder(val binding: ItemFinishedchorelistBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @RequiresApi(Build.VERSION_CODES.M)
        fun bind(position: Int) {
            binding.finishedchore =  choreList?.get(position)

            if (GlobalClass.prefs.getValueString("role").equals("Father") ||
                GlobalClass.prefs.getValueString("role").equals("Mother") ||
                 choreList?.get(position)!!.isAdmin == 1
            ) {

                binding.ll.visibility = View.VISIBLE

                if ( choreList?.get(position)!!.isAdminComplete == 0) {


                    binding.ivCompleted.setImageDrawable(context.resources.getDrawable(R.drawable.ic_rec_checked))
                    binding.tvCompleted.setTextColor(context.resources.getColor(R.color.colorPrimary))


                } else if ( choreList?.get(position)!!.isAdminComplete == 1) {

                    binding.ivInCompleted.setImageDrawable(context.resources.getDrawable(R.drawable.ic_rec_checked))
                    binding.tvCompleted.setTextColor(context.resources.getColor(R.color.colorPrimary))


                } else {

                    binding.ivCompleted.setImageDrawable(context.resources.getDrawable(R.drawable.ic_rec_unchecked))
                    binding.tvCompleted.setTextColor(context.resources.getColor(R.color.clr_gray_text))

                }


            } else {
                binding.ll.visibility = View.GONE

            }



            binding.llCompleted.setOnClickListener {

                if ( choreList?.get(position)!!.isAdminComplete == 0) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else if ( choreList?.get(position)!!.isAdminComplete == 1) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else {
                    binding.ivCompleted.isEnabled = true
                    binding.ivInCompleted.isEnabled = true


                    if(from == "dashboard"){
                        DashboardFragment.instance.iscompletedApiCall(
                            "0",
                             choreList?.get(position)!!.choreId.toString()
                        )
                    }else if(from == "morechore"){
                        MoreChoresFragment.instance.iscompletedApiCall(
                            "0",
                             choreList?.get(position)!!.choreId.toString()
                        )
                    }



                }


            }

            binding.llInCompleted.setOnClickListener {

                if ( choreList?.get(position)!!.isAdminComplete == 0) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else if ( choreList?.get(position)!!.isAdminComplete == 1) {

                    binding.ivCompleted.isEnabled = false
                    binding.ivInCompleted.isEnabled = false

                } else {
                    binding.ivCompleted.isEnabled = true
                    binding.ivInCompleted.isEnabled = true

                    if(from == "dashboard"){
                        DashboardFragment.instance.iscompletedApiCall(
                            "1",
                             choreList?.get(position)!!.choreId.toString()
                        )
                    }else if(from == "morechore"){
                        MoreChoresFragment.instance.iscompletedApiCall(
                            "1",
                             choreList?.get(position)!!.choreId.toString()
                        )
                    }



                }

            }

            if ( choreList?.size!! > 1) {
                if (position ==  choreList?.size!! - 1) {
                    binding.vwLine.visibility = View.GONE
                }
            }

            if (GlobalClass.prefs.getValueString("role").equals("Father") ||
                GlobalClass.prefs.getValueString("role").equals("Mother") ||
                 choreList?.get(position)!!.isAdmin == 1
            ) {
                if ( choreList?.get(position)!!.isAdminComplete == 0) {
                    //green
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))
                } else if ( choreList?.get(position)!!.isAdminComplete == 1) {
                    //green
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))
                } else if ( choreList?.get(position)!!.isAdminComplete == 2) {
                    //black
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.black))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.black))
                }


            } else {
                if ( choreList?.get(position)!!.isAdminComplete == 0) {
                    //green
                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.clr_green))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.clr_green))
                } else if ( choreList?.get(position)!!.isAdminComplete == 2) {
                    //black

                    binding.tvTitle.setTextColor(context.resources.getColor(R.color.black))
                    binding.tvIsDaily.setTextColor(context.resources.getColor(R.color.black))
                }

            }





            val due_dte= dueDateFormate(
                binding.finishedchore!!.dueDate).substring(0,dueDateFormate(
                binding.finishedchore!!.dueDate).length-9)


            val duetime: String = dueDateFormate(
                binding.finishedchore!!.dueDate).substring(dueDateFormate(
                binding.finishedchore!!.dueDate).length-9)
            Log.d("xxx", "Date:$due_dte\ntime:$duetime")


            if (binding.finishedchore?.isCreateby == 0) {
                binding.tvDateTime.text =
                    "Created for " + binding.finishedchore?.createBy + " " + " on " + dueDateFormate(
                        binding.finishedchore?.dueDate
                    )
            } else {
                binding.tvDateTime.text =
                    "Created by " + binding.finishedchore?.createBy + " "  + " on " + dueDateFormate(
                        binding.finishedchore?.dueDate
                    )
            }


            binding.root.setOnClickListener {
                onItemClicked(binding.finishedchore!!)
            }
            binding.executePendingBindings()
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CategoriesViewHolder(
            ItemFinishedchorelistBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ChildFinishedChore.CategoriesViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount() = choreList?.size ?: 0

    fun dueDateFormate(dueDate: String?): String {
        var spf = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val newDate: Date = spf.parse(dueDate)
        spf = SimpleDateFormat("MMM-dd-yyyy hh:mm aa")
        val newDateString: String = spf.format(newDate)
        Log.i("datetime", newDateString)
        return newDateString

    }

}
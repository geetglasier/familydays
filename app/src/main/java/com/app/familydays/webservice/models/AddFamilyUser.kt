package com.app.familydays.webservice.models

import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

data class AddFamilyUser(
    @Json(name = "birth_date")
    var birth_date: String?="",

    @Json(name = "email")
    var email: String?="",

    @Json(name = "password")
    var password: String?="",

    @Json(name = "full_name")
    var full_name: String?="",

    @Json(name = "is_admin")
    var is_admin: Int?=1,

    @Json(name = "phone_no")
    var phone_no: String?="",

    @Json(name = "profile_file")
    var profile_file: String?="",

    @Json(name = "role")
    var role: Int?=0,

    @Json(name = "rolename")
    var rolename: String?="",

    @Json(name = "token")
    var token: String?="",

    @Json(name = "username")
    var username: String?=""
) {
    fun toMap() =
        ((Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            .adapter(AddFamilyUser::class.java)
            .toJsonValue(this) as Map<String, String>))

}
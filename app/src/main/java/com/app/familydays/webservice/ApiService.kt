package com.app.familydays.webservice

import com.app.familydays.viewmodels.ChildMemberDetailVm
import com.app.familydays.webservice.responses.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

@JvmSuppressWildcards
interface ApiService {

    @Multipart
    @POST("api/signup")
      suspend fun signUp(
        @PartMap params: Map<String, RequestBody>,
        @Part file: MultipartBody.Part?,

    ): SignUpResp


    @FormUrlEncoded
    @POST("api/new-chores")
    suspend fun addchores(
        @Header("token") token: String,
        @Field("icon_name")icon_name: String,
        @Field("title")title: String,
        @Field("chore_time")chore_time: String,
        @Field("child_id")child_id: String,
        @Field("is_daily_chore")is_daily_chores: String,
        @Field("set_point")set_points: String,
        @Field("date_time")date_time: String,

        ):AddChoresResp


    @FormUrlEncoded
    @POST("api/delete-chores")
    suspend fun deletechore(
        @Field("chore_id") chore_id: String,


        ):AddChoresResp



    @FormUrlEncoded
    @POST("api/reward-is-conformation")
    suspend fun confirm_reward(
        @Field("reward_id") reward_id: String,
        @Field("is_conform") is_conform: Int


        ):AddChoresResp


    @FormUrlEncoded
    @POST("api/create-claim")
    suspend fun claim_reward(
        @Header("token")token: String,
        @Field("reward_id") reward_id: Int,
        @Field("date_time") date_time:String


    ):AddChoresResp



    @FormUrlEncoded
    @POST("api/delete-reward")
    suspend fun deletereward(
        @Field("reward_id") reward_id: String,


        ):AddChoresResp


    @FormUrlEncoded
        @POST("api/new-chores-child")
    suspend fun addchoreschild(
        @Header("token")token: String,
        @Field("icon_name")icon_name: String,
        @Field("title")title: String,
        @Field("chore_time")chore_time: String,
        @Field("is_daily_chore")is_daily_chores: String,
        @Field("is_confirmation")is_confirmation: String,
        @Field("cho_is_complete")cho_is_complete: String,
        @Field("set_point") set_point: String,

        ): AddChoresResp



    @FormUrlEncoded
    @POST("api/create-reward")
    suspend fun addreward(
        @Header("token")token: String,
        @Field("category_id")category_id: String,
        @Field("rewards_name")rewards_name: String,
        @Field("brand_name")brand_name: String,
        @Field("brand_icon")brand_icon: String,
        @Field("frame_date")frame_date: String,
        @Field("point")point: String,
        @Field("child_id")child_id: String,
        @Field("date_time")date_time: String,

        ): AddRewardResp


    @FormUrlEncoded
    @POST("api/chores_is_complete")
    suspend fun chores_is_complete(
        @Header("token")token: String,
        @Field("chore_id")chore_id: String,
        @Field("is_complete")is_complete: String,
        @Field("date_time")date_time: String
        ): ChoresCompletedResp



    @FormUrlEncoded
    @POST("api/approve-chores")
    suspend fun chores_approve(
        @Header("token")token: String,
        @Field("chore_id")chore_id: String
    ): GeneralResp



    @FormUrlEncoded
    @POST("api/update-reward")
    suspend fun updatereward(
        @Header("token")token: String,
        @Field("rewards_name")rewards_name: String,
        @Field("reward_id")reward_id:Int,
        @Field("category_id")category_id: Int,
        @Field("category")category: String,
        @Field("category_icon")category_icon: String,
        @Field("brand_name")brand_name: String,
        @Field("brand_icon")is_confirmation: String,
        @Field("frame_date") frame_date: String,
        @Field("point")point: Int,

        ):GeneralResp


    @FormUrlEncoded
    @POST("api/update_chores")
    suspend fun updatechoreschild(
        @Header("token")token: String,
        @Field("chore_id")chore_id: String,
        @Field("icon_name")icon_name: String,
        @Field("title")title: String,
        @Field("is_daily_chore")is_daily_chores: String,
        @Field("is_complete")is_complete: String,
        @Field("is_confirmation")is_confirmation: String,
        @Field("set_point") set_point: String,
        @Field("chore_time")chore_time: String,
        @Field("date_time")date_time: String

    ):GeneralResp


    @FormUrlEncoded
    @POST("api/login")
    suspend fun login(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("device_token") device_token: String,
        @Field("device_type") device_type: String
    ): LoginResp

    @FormUrlEncoded
    @POST("api/chores_id_wise_details")
    suspend fun choredetails(
        @Field("chore_id") chore_id: String,
        @Field("notification_id") notification_id: String,
    ):ChoreDetailsResp

    @FormUrlEncoded
    @POST("api/reward-details")
    suspend fun rewarddetails(
        @Field("reward_id")reward_id: String,
        @Field("notification_id") notification_id: String
    ):RewardDetailsResp

    @FormUrlEncoded
    @POST("api/forget_password")
    suspend fun forgotPassword(
        @Field("email") email: String
    ): ForgotPasswordResp

    @Multipart
    @POST("api/contact-admin")
    suspend fun contactAdmin(
        @PartMap params: Map<String, RequestBody>,
        @Part file: MultipartBody.Part?
    ): ContactAdminResp

    @POST("api/user-details")
    suspend fun getUserDetail(@Header("token") token: String): GetUserDetailResp

    @Multipart
    @POST("api/edit_profile_image")
    suspend fun updateProfilePic(
        @Header("token") token: String,
        @Part file: MultipartBody.Part?
    ): UploadImageProfileResp


    @FormUrlEncoded
    @POST("api/edit_profile")
    suspend fun updateUserDetail(
        @Header("token") token: String,
       @Field("fullname")fullname:String,
        @Field("email")email:String,
        @Field("phone_no")phone_no:String,
        @Field("role")role:String,
        @Field("birth_date")birth_date:String,
        @Field("password")password: String,
        @Field("is_admin")is_admin:String,
    ): GeneralResp


    @Multipart
    @POST("api/admin_update_child_profile")
    suspend fun updatechildDetail(
        @Header("token") token: String,
        @Part("username")username:RequestBody,
        @Part("child_id")child_id:RequestBody,
        @Part("fullname")fullname:RequestBody,
        @Part("email")email:RequestBody,
        @Part("password")password: RequestBody,
        @Part("birth_date")birth_date:RequestBody,
        @Part("role")role:RequestBody,
        @Part("is_admin")is_admin:RequestBody,
        @Part("phone_no")phone_no:RequestBody,
        @Part profilefile:MultipartBody.Part?
    ): GeneralResp


    @Multipart
    @POST("api/add_family_member")
    suspend fun addFamilyMember(
        @Header("token") token: String,
        @PartMap params: Map<String, RequestBody>,
        @Part file: MultipartBody.Part?
    ): AddFamilyMemberResp

    @POST("api/family_member_list")
    suspend fun getFamilyMemberList(@Header("token") token: String): FamilyMemberListResp

    @FormUrlEncoded
    @POST("api/message-family-member-list")
    suspend fun getFamilyMemberListmsg(@Header("token") token: String,@Field("notification_id") notification_id: String):MessageMemberlistResp

    @FormUrlEncoded
    @POST("api/member-wise-message-list")
    suspend fun getmessages(
        @Header("token") token: String,
        @Field("user_id")user_id:String):GetMessagesResp

    @FormUrlEncoded
    @POST("api/send-message")
    suspend fun sendmessage(
        @Header("token") token: String,
        @Field("received_id")received_id: String,
        @Field("message")message: String,
        @Field("date_time")date_time: String):GeneralResp


    @POST("api/family_child_list")
    suspend fun childlist(@Header("token") token: String): GetChildListResp

    @FormUrlEncoded
    @POST("api/child-assign-finished-chores")
    suspend fun getChildMemberDetail(@Header("token") token: String,@Field("date_time") date_time:String): GetChildMemberDetailResp


    @FormUrlEncoded
    @POST("api/change_password")
    suspend fun resetpwd(

        @Field("token") reward_id: String,
        @Field("password") date_time:String

    ): ResetPwd

    @FormUrlEncoded
    @POST("api/admin_finished_chores_list")
    suspend fun getAdminFinishedChoreList(@Header("token") token: String,@Field("load_more") load_more:String): GetFinishedChoreResp

    @FormUrlEncoded
    @POST("api/admin_assigned_chores_list")
        suspend fun getAdminAssignedChoreList(@Header("token") token: String,@Field("load_more") load_more:String,@Field("date_time") date_time:String): GetAssignedChoreResp
    @FormUrlEncoded
    @POST("api/reward-list")
    suspend fun getrewardList(@Header("token") token: String,@Field("load_more") load_more:String,@Field("date_time") date_time:String): GetRewardResp

    @FormUrlEncoded
    @POST("api/user-wise-reward-list")
    suspend fun getrewardListuserwise(@Header("token") token: String,@Field("load_more") load_more:String,@Field("date_time") date_time:String): GetRewardResp

    @FormUrlEncoded
    @POST("api/claim-list")
    suspend fun getclaimsList(@Header("token") token: String,@Field("load_more") load_more:String,@Field("date_time") date_time:String,@Field("notification_id") notification_id: String): ClaimListResp



    @FormUrlEncoded
    @POST("api/user-wise-claim-list")
    suspend fun getclaimsListuserwise(@Header("token") token: String,@Field("load_more") load_more:String,@Field("date_time") date_time:String): ClaimListResp



    @FormUrlEncoded
    @POST("api/child-assign-finished-chores")
    suspend fun child_assign_finished_chores(@Header("token") token: String,
                                          @Field("load_more") load_more:String,
                                          @Field("to_date") to_date:String,
                                          @Field("from_date") from_date:String,
                                          @Field("status") status:String,
                                          @Field("date_time") date_time: String
    ):ChildChoreResp





    @GET("api/preset-chores-list")
    suspend fun getPopularChoreList(@Header("token") token: String): PopularChoreResp


    @POST("api/notification-list")
    suspend fun getnotificationList(@Header("token") token: String): GetNotificationListResp

    @GET("api/reward-category-list")
    suspend fun getrewardCategoryList(@Header("token") token: String): RewardCategoryResp

    @FormUrlEncoded
    @POST("api/reward-brands-list")
    suspend fun getrewardBrand(@Field("caterory_id") caterory_id: String): RewardBrandResp

    @POST("api/unread-notification-count")
    suspend fun getcount(@Header("token") token: String): GetCountResp

    @FormUrlEncoded
    @POST("api/finished_chores_filter")
    suspend fun getfinishedchoresfilterlist(
        @Header("token") token: String,
        @Field("load_more") load_more:String,
        @Field("from_date") from_date: String,
        @Field("to_date") to_date:String,
        @Field("child_id") child_id: String
    ): GetFinishedChoreResp


    @FormUrlEncoded
    @POST("api/assigned_chores_filter")
    suspend fun getassignedchoresfilterlist(
        @Header("token") token: String,
        @Field("load_more") load_more:String,
        @Field("from_date") from_date: String,
        @Field("to_date") to_date:String,
        @Field("child_id") child_id: String
    ): GetAssignedChoreResp



    @FormUrlEncoded
    @POST("api/filter-reward-list")
    suspend fun getrewardfilterlist(
        @Header("token") token: String,

        @Field("from_date") from_date: String,
        @Field("to_date") to_date:String,
        @Field("child_id") child_id: String
    ): GetRewardResp

    @FormUrlEncoded
    @POST("api/filter-claim-list")
    suspend fun getclaimfilterlist(
        @Header("token") token: String,

        @Field("from_date") from_date: String,
        @Field("to_date") to_date:String,
        @Field("child_id") child_id: String
    ):ClaimListResp


}








package com.app.familydays.webservice.responses


import com.squareup.moshi.Json

data class FamilyMemberListResp(
    @Json(name = "data")
    val `data`: List<Data>,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean
) {
    data class Data(
        @Json(name = "child_id")
        val childId: Int,
        @Json(name = "email")
        val email: String,
        @Json(name = "full_name")
        val fullName: String,
        @Json(name = "is_admin")
        val isAdmin: String,
        @Json(name = "profile_url")
        val profileUrl: String,
        @Json(name = "reward_point")
        val rewardPoint: Int,
        @Json(name = "role")
        val role: Int,
        @Json(name = "role_type")
        val roleType: String,
        @Json(name = "token")
        val token: String,
        @Json(name = "username")
        val username: String
    )
}
package com.app.familydays.webservice.responses


import com.squareup.moshi.Json

data class PopularChoreResp(
    @Json(name = "data")
    val `data`: List<Data>,
    @Json(name = "error")
    val error: Int,
    @Json(name = "maximum_point")
    val maximumPoint: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "minimum_point")
    val minimumPoint: Int,
    @Json(name = "status")
    val status: Boolean
) {
    data class Data(
        @Json(name = "icon")
        val icon: String,
        @Json(name = "icon_name")
        val iconName: String,
        @Json(name = "id")
        val id: Int,
        @Json(name = "title")
        val title: String
    )
}
package com.app.familydays.fragments

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.DatePicker
import android.widget.TextView
import android.widget.TimePicker
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.adapters.ChildListAdapter
import com.app.familydays.adapters.PointsAdapter
import com.app.familydays.adapters.PopularChoreAdaper
import com.app.familydays.databinding.FragmentAddNewChoreBinding
import com.app.familydays.global.AppConstant
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.utils.unAuthorisedUserDialog
import com.app.familydays.viewmodels.Addchoreschildvm
import com.app.familydays.viewmodels.Addchoresvm
import com.app.familydays.viewmodels.Family_Child_ListVm
import com.app.familydays.viewmodels.PopularChoreListVm
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.bottomsheet_layout.view.*
import kotlinx.android.synthetic.main.fragment_add_new_chore.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*


class AddNewChoreFragment : DialogFragment(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {

    private lateinit var binding: FragmentAddNewChoreBinding
    private val viewModel: Addchoresvm by viewModel()
    private val viewModelchildchores: Addchoreschildvm by viewModel()
    private val viewModelchild: Family_Child_ListVm by viewModel()
    private val viewModel2: PopularChoreListVm by viewModel()

    private val args: AddNewChoreFragmentArgs by navArgs()
    private lateinit var loadingIndicator: KProgressHUD
    var day = 0
    var month: Int = 0
    var year: Int = 0
    var hour: Int = 0
    var minute: Int = 0
    var myday = 0
    var myMonth: Int = 0
    var myYear: Int = 0
    var myHour: Int = 0
    var myMinute: Int = 0
    var mySecond: Int = 0
    lateinit var time: String


    var is_daily_chores = "0"
    var chore_type = "0"
    lateinit var chore_title: String
    var date = Calendar.getInstance()

    var choreIcon = ""

    var bottomSheetDialog: BottomSheetDialog? = null
    var editText: TextView? = null

    var selectedDate: LocalDate? = null
    var currentDate: LocalDate? = null
    var max:Int =0
    var min:Int=0

    companion object {

        lateinit var instance: AddNewChoreFragment

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.let {
            it.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            it.requestFeature(Window.FEATURE_NO_TITLE)
        }
        familychildlist()
        isCancelable = false
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_add_new_chore,
            container,
            false
        )

        instance = this

        loadingIndicator = getLoadingIndicator(requireContext())
        binding.args = args

        choreIcon = args.selectedicon

        binding.imgClose.setOnClickListener {
            dialog?.dismiss()
            GlobalClass.selectepos = -1
            GlobalClass.isselected = 0
            GlobalClass.selectedChildId = ArrayList()

        }
        getPopularChoreList()
        if (args.selectedchore == "Other") {
            binding.etchoretitle.hint = "Please enter chore title"
        } else {
            binding.etchoretitle.setText(args.selectedchore)
        }
        binding.txttimeframe.setOnClickListener() {
            //Toast.makeText(requireContext(), "Date Picker", Toast.LENGTH_SHORT).show()
            datepickerdialog()
//            showDateTimePicker()
        }
        binding.llisDaily.setOnClickListener() {
            if (is_daily_chores == "0") {
                binding.imgchk.setImageResource(R.drawable.ic_rec_checked)
                is_daily_chores = "1"

            } else {
                binding.imgchk.setImageResource(R.drawable.ic_rec_unchecked)
                is_daily_chores = "0"
            }
        }

        if (args.selectedchore.isNotBlank()) {
            binding.chkpopular.setImageResource(R.drawable.ic_rec_checked)
            chore_type = "1"
        } else {
            binding.chkpopular.setImageResource(R.drawable.ic_rec_unchecked)
            chore_type = "0"
        }

        if (args.selectedicon == "other-icon.png") {
            binding.chkcustom.visibility = View.GONE
            binding.chkcustomselected.visibility = View.VISIBLE
            chore_type = "1"

            binding.chkpopular.setImageResource(R.drawable.ic_rec_unchecked)

        }

        binding.txtpoints.text = "1"

        val today = Date()
        val format = SimpleDateFormat("MMM-dd-yyyy hh:mm a")
        var dateToStr = format.format(today)
        binding.txttimeframe.setText(dateToStr)

        binding.llPopularChore.setOnClickListener {
            GlobalClass.selectepos = -1
            chore_type = "0"
            binding.chkcustom.visibility = View.VISIBLE
            binding.chkcustomselected.visibility = View.GONE

            findNavController().navigate(
                PopularChoreListFragmentDirections.actionGlobalNavPopularChore(
                    "addnew",
                    "",
                    ""
                )
            )
        }
        binding.pointsll.setOnClickListener() {
            showcustomdialog()
        }
        binding.imgchk.setOnClickListener() {

        }

        binding.llCustomerChore.setOnClickListener() {

            GlobalClass.selectepos = -1

            binding.etchoretitle.setText("")
            choreIcon = ""
//            Toast.makeText(requireContext(),"CLICKED",Toast.LENGTH_SHORT).show()

            if (binding.chkcustom.isVisible) {
                binding.etchoretitle.setText("")
                chore_title = ""

                binding.chkcustom.visibility = View.GONE
                binding.chkcustomselected.visibility = View.VISIBLE
                chore_type = "1"
                binding.chkpopular.setImageResource(R.drawable.ic_rec_unchecked)

            }

            /*if (chore_type == "0") {
                binding.chkpopular.setImageResource(R.drawable.ic_rec_unchecked)
                binding.chkcustom.setImageResource(R.drawable.ic_rec_checked)
                chore_type = "1"
                binding.etchoretitle.isEnabled = true

            } else {
                binding.chkcustom.setImageResource(R.drawable.ic_rec_unchecked)

                chore_type = "0"
                binding.etchoretitle.isEnabled = false
            }*/
        }


        binding.txtsave.setOnClickListener() {

            for (i in 0 until GlobalClass.selectedChildId?.size) {
                Log.i("finalarray", GlobalClass.selectedChildId[i].toString())
            }
            var displaydate: Date? = null
            var current: Date? = null

            val displayformat = SimpleDateFormat("MMM-dd-yyyy hh:mm a")
            try {
                displaydate = displayformat.parse(binding.txttimeframe.text.toString())
                System.out.println(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            val today = Date()
            val format = SimpleDateFormat("MMM-dd-yyyy hh:mm a")
            val dateToStr = format.format(today)

            try {
                current = format.parse(dateToStr)
                System.out.println(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            //check current time karvanu nd display time sathe compare karvanu


            if (displaydate!!.equals(current)) {
                Log.i("apidatetime", "same")
                Toast.makeText(
                    context,
                    "Time expaired. Please reset chore timeframe!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (GlobalClass.selectedChildId.size==0) {
                Toast.makeText(context, "Please select atleast one child", Toast.LENGTH_SHORT).show()
            } else {
                if (binding.etchoretitle.text.isEmpty()) {
                    Toast.makeText(
                        context,
                        "Please enter and select chore name",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {

                    callparentapi()
                }
            }

        }

        dialog?.show()
        return binding.root
    }

    fun isTimeAfter(startTime: Date?, endTime: Date): Boolean {
        return !endTime.before(startTime)
    }

    private fun callparentapi() {

        val selectedChild = StringBuilder()
        var prefix = ""
        for (str in GlobalClass.selectedChildId.distinct()) {
            selectedChild.append(prefix)
            prefix = ","
            selectedChild.append(str)
        }

        println("WithCommas $selectedChild")

        Log.d("Selected Child",selectedChild.toString())

        Log.v("senddate", finalDateTime(binding.txttimeframe.text.toString()).toString())

        val today = Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val dateToStr = format.format(today)

        Log.e("date", dateToStr)

        var chore_time = ""

        if (binding.txttimeframe.text.toString() != "") {
            chore_time = finalDateTime(binding.txttimeframe.text.toString()).toString()
        } else {
            chore_time = ""
        }
        Log.d("datefinal", chore_time)


        if (choreIcon == "") {
            choreIcon = "other-icon.png"
        }

        viewModel.addchores(
            GlobalClass.prefs.getValueString("token").toString(),
            choreIcon,
            binding.etchoretitle.text.toString(),
            chore_time,
            selectedChild.toString(),
            is_daily_chores,
            binding.txtpoints.text.toString(),
            dateToStr


        ).observe(this) { resources ->
            when (resources.status) {
                Status.SUCCESS -> {
                    loadingIndicator.dismiss()
                    resources.data.let { response ->
                        if (response != null) {
                            if (response.status) {

                                GlobalClass.selectepos = -1
                                GlobalClass.isselected = 0
                                GlobalClass.selectedChildId = ArrayList()


                                Toast.makeText(
                                    requireContext(),
                                    response.message,
                                    Toast.LENGTH_SHORT
                                ).show()

                                setFragmentResult(
                                    "refresh",
                                    bundleOf("fromAddChore" to true)
                                )

                                findNavController().navigateUp()
                            }
                        }

                    }

                }

                Status.ERROR -> {
                    loadingIndicator.dismiss()

                }
                Status.LOADING -> {
                    loadingIndicator.show()
                }
            }

        }
    }

    fun familychildlist() {

        viewModelchild.getChildlist(GlobalClass.prefs.getValueString("token").toString())
            .observe(this) { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        loadingIndicator.dismiss()

                        resource.data?.let { response ->
                            if (response.status) {
                                binding.rvChildMember.adapter = ChildListAdapter(
                                ).apply {
                                    childList = response.data
                                }

                            }
                        }

                    }
                    Status.ERROR -> loadingIndicator.dismiss()

                    Status.LOADING -> loadingIndicator.show()
                }

            }

    }

    private fun datepickerdialog() {

        val calendar = Calendar.getInstance()

        year = calendar[Calendar.YEAR]
        month = calendar[Calendar.MONTH]
        day = calendar[Calendar.DAY_OF_MONTH]
        val datePickerDialog =
            DatePickerDialog(requireActivity(), R.style.DialogTheme, this, year, month, day)
        datePickerDialog.datePicker.minDate = calendar.timeInMillis
        datePickerDialog.show()
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )
        )
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )
        )
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000


    }

    private fun showcustomdialog() {

        val viewGroup: ViewGroup? = requireView().findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(requireContext())
            .inflate(R.layout.bottomsheet_layout, viewGroup, false)
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())


        //setting the view of the builder to our custom view that we already inflated

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it

        //finally creating the alert dialog and displaying it
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()

        editText = dialogView.findViewById<TextView>(R.id.etpoints)
        // editText.filters = arrayOf<InputFilter>(MinMaxFilter("1", "500"))
        editText!!.setText(binding.txtpoints.text.toString())


        dialogView.imgback.setOnClickListener() {
            alertDialog.dismiss()
        }
        val pointslist: MutableList<String> = ArrayList()
        for (i in min..max) {
            pointslist.add(i.toString())
            // Log.d("xxx",pointslist.size.toString())
        }

        dialogView.etpoints.setOnClickListener() {

            bottomSheetDialog = BottomSheetDialog(requireContext())
            val btnsheet = layoutInflater.inflate(R.layout.bottomsheet, null)


            val rec = btnsheet.findViewById<RecyclerView>(R.id.rvpoints)

            rec.layoutManager = LinearLayoutManager(requireContext())
            val adapter = PointsAdapter(requireContext(), pointslist, "Addnew")
            rec.adapter = adapter

            bottomSheetDialog!!.setContentView(btnsheet)

//            val bottomSheetBehavior = BottomSheetBehavior.from(btnsheet.getParent() as View)
//            bottomSheetBehavior.peekHeight = 500

            bottomSheetDialog!!.show()
        }
        dialogView.btnsave.setOnClickListener() {
            binding.txtpoints.text = editText!!.text.toString()
            alertDialog.dismiss()
        }


    }


    override fun onDestroy() {
        super.onDestroy()
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {

        myYear = year
        myday = dayOfMonth
        myMonth = month + 1

        val c = Calendar.getInstance()

        hour = c[Calendar.HOUR_OF_DAY]
        minute = c[Calendar.MINUTE]
        mySecond = c[Calendar.SECOND]

        selectedDate = LocalDate.of(myYear, myMonth, myday)
        currentDate =
            LocalDate.of(c[Calendar.YEAR], c[Calendar.MONTH] + 1, c[Calendar.DAY_OF_MONTH])

        Log.e("selecteddate", selectedDate.toString() + "=======" + currentDate.toString())

        val timePickerDialog = TimePickerDialog(
            requireContext(), R.style.DialogTheme,
            this,
            hour,
            minute, false
        )

        timePickerDialog.show()

        timePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )
        )

        timePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )
        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {

        if (selectedDate!!.isAfter(currentDate)) {

            myHour = hourOfDay
            myMinute = minute

            txttimeframe.setText(parseDateTime("$myYear-$myMonth-$myday" + " " + "$myHour:$myMinute"))
            time = "$myYear-$myMonth-$myday " +
                    "$myHour:$myMinute"
        } else {
            val c = Calendar.getInstance()

            if ((hourOfDay <= (c.get(Calendar.HOUR_OF_DAY))) &&
                (minute <= (c.get(Calendar.MINUTE)))
            ) {

                Toast.makeText(requireContext(), "You can't pick past time", Toast.LENGTH_SHORT)
                    .show()
                binding.txtsave.isEnabled=false

            } else {
                binding.txtsave.isEnabled=true
                myHour = hourOfDay
                myMinute = minute

                txttimeframe.setText(parseDateTime("$myYear-$myMonth-$myday" + " " + "$myHour:$myMinute"))
                time = "$myYear-$myMonth-$myday " +
                        "$myHour:$myMinute"

            }

        }

        /*if (hour >= c.get(Calendar.HOUR_OF_DAY) && minute >= c.get(Calendar.MINUTE) ){
            myHour = hourOfDay
            myMinute = minute

            txttimeframe.setText(parseDateTime("$myYear-$myMonth-$myday" + " " + "$myHour:$myMinute"))
            binding.imgtimechk.setImageResource(R.drawable.ic_rec_checked)
        } else{


            Toast.makeText(requireContext(), "You can't pick past time", Toast.LENGTH_SHORT).show()

        }*/

    }

    fun displayTime(inputDateTime: String?): String? {
        val inputPattern = "MMM-dd-yyyy hh:mm a"
        val outputPattern = "hh:mm a"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.US)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.US)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }


    fun parseDateTime(inputDateTime: String?): String? {
        val inputPattern = "yyyy-MM-dd HH:mm"
        val outputPattern = "MMM-dd-yyyy hh:mm a"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.US)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.US)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }


    fun finalDateTime(inputDateTime: String?): String? {
        val inputPattern = "MMM-dd-yyyy hh:mm a"
        val outputPattern = "yyyy-MM-dd HH:mm:ss"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.US)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.US)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun getPopularChoreList() {

        if (isNetworkConnected(requireContext())) {
            viewModel2.getPopularChoreList(
                GlobalClass.prefs.getValueString("token").toString()
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    max=response.maximumPoint
                                    min=response.minimumPoint
                                    binding.minimumpts.text=min.toString()
                                    binding.maximumpts.text=max.toString()
                                }


                            }
                        }

                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()

                    }

                }

        }

    }
}
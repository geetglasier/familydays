package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class GetRewardDetailsVM(val mainRepository: MainRepository): ViewModel() {

    fun getrewarddetails(reward_id:String,notification_id:String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getrewarddetails(reward_id,notification_id)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }
}
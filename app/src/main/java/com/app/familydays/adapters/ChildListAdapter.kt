package com.app.familydays.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.ChildlistItemBinding
import com.app.familydays.fragments.AddNewChoreFragment
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.GetChildListResp

class ChildListAdapter(
):
    RecyclerView.Adapter<ChildListAdapter.BlockUserViewHolder>() {

    var selectedId:ArrayList<Int>? = ArrayList()


    var childList: List<GetChildListResp.Data>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class BlockUserViewHolder(val binding: ChildlistItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.child = childList?.get(position)

            if (GlobalClass.selectedChildId.size > 0) {
                      for (i in GlobalClass.selectedChildId.indices) {
                    if (GlobalClass.selectedChildId[i] == childList?.get(position)!!.childId) {
                        binding.ivUnChecked.setImageResource(R.drawable.ic_rec_checked)
                        break
//                        GlobalClass.isselected=1
//                        GlobalClass.selectedChildId.add(childList?.get(position)?.childId!!)
//                        GlobalClass.selectedchild.add(childList?.get(position)?.fullName!!)
//                    } else {
//                        binding.ivUnChecked.setImageResource(R.drawable.ic_rec_unchecked)
//                        GlobalClass.isselected=0
////                        GlobalClass.selectedChildId.remove(childList?.get(position)?.childId!!)
////                        GlobalClass.selectedchild.remove(childList?.get(position)?.fullName!!)
//                    }
                    }
                }
            }

            binding.llchildlist.setOnClickListener {
                if (GlobalClass.isselected == 0) {
                    binding.ivUnChecked.setImageResource(R.drawable.ic_rec_checked)
                    GlobalClass.isselected = 1
                    GlobalClass.selectedChildId.add(childList?.get(position)?.childId!!)
                    GlobalClass.selectedchild.add(childList?.get(position)?.fullName!!)

                } else {
                    binding.ivUnChecked.setImageResource(R.drawable.ic_rec_unchecked)
                    GlobalClass.isselected = 0
                    GlobalClass.selectedChildId.remove(childList?.get(position)?.childId!!)
                    GlobalClass.selectedchild.remove(childList?.get(position)?.fullName!!)


                }

                notifyDataSetChanged()

                /*  for (i in 0 until GlobalClass.selectedChildId?.size!!){

                      if (GlobalClass.isselected==0){
                          binding.ivUnChecked.setImageResource(R.drawable.ic_rec_checked)
                          GlobalClass.isselected=1
                      }else{
                          binding.ivUnChecked.setImageResource(R.drawable.ic_rec_unchecked)
                          GlobalClass.isselected=0


                      }

                  }*/


            }



            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        BlockUserViewHolder(
            ChildlistItemBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )
    override fun onBindViewHolder(holder: BlockUserViewHolder, position: Int) {
        holder.bind(position)
    }



    override fun getItemCount(): Int {
        return childList?.size ?: 0
    }



}
package com.app.familydays.fragments

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.app.familydays.R
import com.app.familydays.adapters.FinishedChoreAdapter
import com.app.familydays.adapters.PopularChoreAdaper
import com.app.familydays.adapters.RewardCategoryAdapter
import com.app.familydays.databinding.FragmentDashboardBinding
import com.app.familydays.databinding.FragmentPopularChoreListBinding
import com.app.familydays.global.AppConstant
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.utils.unAuthorisedUserDialog
import com.app.familydays.viewmodels.AdminFinishedChoreListVm
import com.app.familydays.viewmodels.PopularChoreListVm
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.android.viewmodel.ext.android.viewModel


class PopularChoreListFragment : DialogFragment() {
    private lateinit var binding: FragmentPopularChoreListBinding
    private lateinit var loadingIndicator: KProgressHUD
    private val viewModel: PopularChoreListVm by viewModel()
    private val args: PopularChoreListFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_popular_chore_list, container, false)
        loadingIndicator = getLoadingIndicator(requireContext())

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onPrepareOptionsMenu(menu: Menu) {

//        menu.findItem(R.id.item_profile).isVisible = false
        menu.findItem(R.id.item_add_family).isVisible = false
        menu.findItem(R.id.item_child_profile).isVisible = false

        super.onPrepareOptionsMenu(menu)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

       if (args.fromwhich=="addnew"||args.fromwhich=="update"||args.fromwhich=="addnewchild"){
           getPopularChoreList()
       } else if (args.fromwhich=="reward"||args.fromwhich=="updatereward"){
            getrewardList()
       }

    }
    @RequiresApi(Build.VERSION_CODES.M)
    private fun getPopularChoreList() {

        if (isNetworkConnected(requireContext())) {
            viewModel.getPopularChoreList(
                GlobalClass.prefs.getValueString("token").toString()
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    binding.rvChoreList.adapter =
                                        PopularChoreAdaper(
                                            requireContext(),
                                            onItemClicked = {



                                                if(args.fromwhich == "addnew"){
                                                    if(GlobalClass.prefs.getValueString("role").equals("Father")||GlobalClass.prefs.getValueString("role").equals("Mother")){
                                                        findNavController().navigateUp()

                                                        findNavController().navigate(AddNewChoreFragmentDirections.actionGlobalDialogNewChore(it.title,it.iconName ))
                                                    }else{
                                                        findNavController().navigateUp()

                                                        findNavController().navigate(AddchoreschildFragmentDirections.actionGlobalDialogNewChorechild(it.title,it.iconName))
                                                    }

                                                }else if (args.fromwhich == "addnewchild"){
                                                    if(GlobalClass.prefs.getValueString("role").equals("Father")||GlobalClass.prefs.getValueString("role").equals("Mother")){
                                                        findNavController().navigateUp()

                                                        findNavController().navigate(AddNewChoreFragmentDirections.actionGlobalDialogNewChore(it.title,it.iconName ))
                                                    }else{
                                                        findNavController().navigateUp()

                                                        findNavController().navigate(AddchoreschildFragmentDirections.actionGlobalDialogNewChorechild(it.title,it.iconName))
                                                    }

                                                }else if (args.fromwhich == "update"){
//                                                    setFragmentResult(
//                                                        "refresh",
//                                                        bundleOf("fromPopularchore" to true)
//                                                    )
                                                    findNavController().navigateUp()

                                                    findNavController().navigate(
                                                        UpdateChoresFragmentDirections.actionGlobalDialogUpdateChore(args.choreId,
                                                           it.iconName,
                                                            args.childToken,it.title,"fromPopular"
                                                        ,"","")
                                                    )


                                                }





                                            }).apply {
                                            popularChoreRespList = response.data
                                        }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getrewardList() {

        if (isNetworkConnected(requireContext())) {
            viewModel.getRewardcategoryList(
                GlobalClass.prefs.getValueString("token").toString()
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {

                                    binding.rvChoreList.adapter =
                                        RewardCategoryAdapter(
                                            requireContext(),
                                            onItemClicked = {



                                                if(args.fromwhich == "reward"){
                                                    if(GlobalClass.prefs.getValueString("role").equals("Father")||GlobalClass.prefs.getValueString("role").equals("Mother")){
                                                        findNavController().navigateUp()

                                                        findNavController().navigate(AddNewRewardFragmentDirections.actionGlobalDialogNewReward(it.title,it.iconName,it.cat_id ))
                                                    }else{
                                                        findNavController().navigateUp()

                                                        findNavController().navigate(AddNewRewardFragmentDirections.actionGlobalDialogNewReward(it.title,it.iconName,it.cat_id ))
                                                    }

                                                }else if (args.fromwhich == "updatereward"){
                                                    if(GlobalClass.prefs.getValueString("role").equals("Father")||GlobalClass.prefs.getValueString("role").equals("Mother")){
                                                        findNavController().navigateUp()

                                                        findNavController().navigate(UpdateRewardFragmentDirections.actionGlobalDialogUpdateReward(it.title,it.iconName,args.choreId,it.cat_id,"fromReward",""))
                                                    }else{
                                                        findNavController().navigateUp()

                                                        findNavController().navigate(UpdateRewardFragmentDirections.actionGlobalDialogUpdateReward(it.title,it.iconName,args.choreId,it.cat_id,"fromReward",""))
                                                    }

                                                }




                                            }).apply {
                                            rewardcategoryRespList = response.data
                                        }
                                } else {

                                    if (response.error == AppConstant.UNAUTHORISED) {
                                        unAuthorisedUserDialog(requireActivity(), "")

                                    } else {
                                        Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                            .show()

                                    }

                                }
                            }

                        }
                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()
                    }
                }

        }

    }
}
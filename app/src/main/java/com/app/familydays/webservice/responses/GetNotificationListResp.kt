package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

class GetNotificationListResp (
            @Json(name = "data")
            val `data`: List<Data>,
            @Json(name = "error")
            val error: Int,
            @Json(name = "message")
            val message: String,
            @Json(name = "status")
            val status: Boolean
            ) {

     data class Data(

        @Json(name = "notification_id")
        val notification_id: Int,
        @Json(name = "type")
        val type: String,
        @Json(name = "message")
        val message: String,
        @Json(name = "content")
        val content: String,
        @Json(name = "chores_id")
        val chores_id: String,
        @Json(name = "reward_id")
        val reward_id: String,
        @Json(name = "user_id")
        val user_id: String,
        @Json(name = "token")
        val token: String,
        @Json(name = "full_name")
        val full_name: String,
        @Json(name = "username")
        val username: String,
        @Json(name = "profile_url")
        val profile_url: String,
        @Json(name = "send_time")
        val send_time: String,
        @Json(name = "is_read")
        val is_read: String







    )
}
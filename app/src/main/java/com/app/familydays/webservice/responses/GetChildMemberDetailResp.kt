package com.app.familydays.webservice.responses


import com.squareup.moshi.Json

data class GetChildMemberDetailResp(
    @Json(name = "assign_chore")
    val assignChore: List<AssignChore>,
    @Json(name = "child_name")
    val childName: String,
    @Json(name = "error")
    val error: Int,
    @Json(name = "finish_chore")
    val finishChore: List<FinishChore>,
    @Json(name = "message")
    val message: String,
    @Json(name = "profile_url")
    val profileUrl: String,
    @Json(name = "status")
    val status: Boolean,
    @Json(name = "total_point")
    val totalPoint: Int

) {
    data class AssignChore(
     /*   @Json(name = "child_id")
        val childId: Int=0,*/
        @Json(name = "chore_id")
        val choreId: Int,
        @Json(name = "create_by")
        val createBy: String,
        @Json(name = "due_date")
        val dueDate: String,
        @Json(name = "icon_url")
        val iconUrl: String,
        @Json(name = "is_admin")
        val isAdmin: Int,
        @Json(name = "is_admin_complete")
        val isAdminComplete: Int,
        @Json(name = "is_complete")
        val isComplete: Int,
        @Json(name = "is_conform")
        val isConform: Int,
        @Json(name = "is_createby")
        val isCreateby: Int,
        @Json(name = "is_daily")
        val isDaily: String,
        @Json(name = "point")
        val point: Int,
        @Json(name = "title")
        val title: String,
        @Json(name = "token")
        val token: String,
        @Json(name = "left_days")
        val left_days: String
    )

    data class FinishChore(
       /* @Json(name = "child_id")
        val childId: Int,*/
        @Json(name = "chore_id")
        val choreId: Int,
        @Json(name = "create_by")
        val createBy: String,
        @Json(name = "due_date")
        val dueDate: String,
        @Json(name = "icon_url")
        val iconUrl: String,
        @Json(name = "is_admin")
        val isAdmin: Int,
        @Json(name = "is_admin_complete")
        val isAdminComplete: Int,
        @Json(name = "is_complete")
        val isComplete: Int,
        @Json(name = "is_conform")
        val isConform: Int,
        @Json(name = "is_createby")
        val isCreateby: Int,
        @Json(name = "is_daily")
        val isDaily: String,
        @Json(name = "point")
        val point: Int,
        @Json(name = "title")
        val title: String,
        @Json(name = "token")
        val token: String
    )
}
package com.app.familydays.fragments

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.adapters.PointsAdapter
import com.app.familydays.databinding.FragmentAddchoreschildBinding
import com.app.familydays.databinding.FragmentUpdateChoresBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.utils.MinMaxFilter
import com.app.familydays.utils.Status
import com.app.familydays.utils.getLoadingIndicator
import com.app.familydays.utils.isNetworkConnected
import com.app.familydays.viewmodels.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.bottomsheet_layout.view.*
import kotlinx.android.synthetic.main.fragment_add_new_chore.*
import kotlinx.android.synthetic.main.fragment_add_new_chore.txtpoints
import kotlinx.android.synthetic.main.fragment_add_new_chore.txttimeframe
import kotlinx.android.synthetic.main.fragment_update_chores.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class UpdateChoresFragment : DialogFragment(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {

    private lateinit var binding: FragmentUpdateChoresBinding
    lateinit var editText: EditText
    private val viewModel: GetChoresDetailVM by viewModel()
    private val viewModelupdate: UpdateChoresVM by viewModel()
    private val args: UpdateChoresFragmentArgs by navArgs()
    private val viewModel2: PopularChoreListVm by viewModel()

    var enable=false

    private lateinit var loadingIndicator: KProgressHUD
    var day = 0
    var month: Int = 0
    var year: Int = 0
    var hour: Int = 0
    var minute: Int = 0
    var myday = 0
    var myMonth: Int = 0
    var myYear: Int = 0
    var myHour: Int = 0
    var myMinute: Int = 0
    var mySecond: Int = 0
    lateinit var time: String

    var is_daily_chores = "0"

    //    var chore_type = "0"
    var is_confirmation = "0"
    var is_complete = "0"
    lateinit var chore_title: String
    var chore_point: String? = null
    var chore_id: String? = null
    var choreicon = ""
    var bottomSheetDialog: BottomSheetDialog? = null
    var editTextpoints: TextView? = null
    var role = ""

    var selectedDate: LocalDate? = null
    var currentDate: LocalDate? = null
    var max:Int =0
    var min:Int=0

    companion object {

        lateinit var instance: UpdateChoresFragment

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.let {
            it.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            it.requestFeature(Window.FEATURE_NO_TITLE)
        }

        isCancelable = false
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_update_chores,
            container,
            false
        )
        loadingIndicator = getLoadingIndicator(requireContext())
        instance = this
        binding.args = args
        chore_id = args.selectedchore
        choreicon = args.selectedicon

        binding.imgClose.setOnClickListener {
            dialog?.dismiss()
            GlobalClass.chore_type = "0"
          /*  setFragmentResult(
                "refresh",
                bundleOf("fromUpdateChore" to true)
            )*/

        }

        binding.txttimeframe.setOnClickListener() {
            //Toast.makeText(requireContext(), "Date Picker", Toast.LENGTH_SHORT).show()
            datepickerdialog()
        }

        if (args.choreid.isNotEmpty()){
            chore_id=args.choreid
        }

        getPopularChoreList()


        binding.llisDaily.setOnClickListener() {
            if (is_daily_chores == "0") {
                binding.imgchk.setImageResource(R.drawable.ic_rec_checked)
                is_daily_chores = "1"

            } else {
                binding.imgchk.setImageResource(R.drawable.ic_rec_unchecked)
                is_daily_chores = "0"
            }
        }

        //       Toast.makeText(context,args.childToken, Toast.LENGTH_SHORT).show()


        /*if (args.selectedchore.isNotBlank()) {
            //binding.chkpopular.setImageResource(R.drawable.ic_rec_checked)
            chore_type = "1"
        } else {
            //  binding.chkpopular.setImageResource(R.drawable.ic_rec_unchecked)
            chore_type = "0"
        }*/
        if (args.selectedchore=="Other"||args.populartitle=="Other"){
            binding.etchoretitle.hint = "Please enter chore title"
            binding.chkcustom.setImageResource(R.drawable.ic_rec_checked)
            binding.chkpopular.setImageResource(R.drawable.ic_rec_unchecked)
            binding.imgchk.isEnabled=true


        }else if (args.populartitle.isNotEmpty()){
            binding.etchoretitle.setText(args.populartitle)
            binding.chkpopular.setImageResource(R.drawable.ic_rec_checked)
            binding.chkcustom.setImageResource(R.drawable.ic_rec_unchecked)
            binding.imgchk.isEnabled=true
        }
        else{
            callchoresdetailapi()
        }

        binding.llPopularChore.setOnClickListener {
            binding.llCustomerChore.isEnabled = true

            GlobalClass.chore_type = "0"
            binding.chkcustom.visibility = View.VISIBLE
            binding.chkcustomselected.visibility = View.GONE

            findNavController().navigate(
                PopularChoreListFragmentDirections.actionGlobalNavPopularChore(
                    "update",
                    args.childToken, chore_id!!
                )
            )
        }

        binding.pointsll.setOnClickListener() {
            showcustomdialog()
        }

        binding.llCustomerChore.setOnClickListener {
//            Toast.makeText(requireContext(), "CLICKED", Toast.LENGTH_SHORT).show()

            choreicon = ""

            if (binding.chkcustom.isVisible) {
                binding.llCustomerChore.isEnabled = false
                binding.etchoretitle.setText("")
                chore_title = ""

                binding.chkcustom.visibility = View.GONE
                binding.chkcustomselected.visibility = View.VISIBLE
                GlobalClass.chore_type = "1"
                binding.chkpopular.setImageResource(R.drawable.ic_rec_unchecked)

            }
            /*else if (binding.chkcustomselected.visibility == View.VISIBLE) {
                chore_type = "0"
                binding.chkcustom.visibility = View.VISIBLE
                binding.chkcustomselected.visibility = View.GONE
            }*//*
*/

            /*      if (chore_type == "0") {
                      binding.chkpopular.setImageResource(R.drawable.ic_rec_unchecked)

                      binding.chkcustom.setImageResource(R.drawable.ic_rec_checked)
                      chore_type = "1"


                  } else {
                      binding.chkcustom.setImageResource(R.drawable.ic_rec_unchecked)

                      chore_type = "0"
                  }*/

        }

        binding.txtsave.setOnClickListener() {


                chore_title = binding.etchoretitle.text.toString()

                if (chore_title == "") {
                    Toast.makeText(context, "Please enter and select chore name", Toast.LENGTH_SHORT)
                        .show()

                } else {
                    callchildapi()
                }





        }

        binding.llisCompleted.setOnClickListener() {

            if (is_complete == "0") {
                binding.imgmark.setImageResource(R.drawable.ic_rec_checked)
                is_complete = "1"
            } else {
                binding.imgmark.setImageResource(R.drawable.ic_rec_unchecked)
                is_complete = "0"
            }

        }

        binding.llisApproval.setOnClickListener() {

            if (is_confirmation == "0") {
                binding.imgisparent.setImageResource(R.drawable.ic_rec_checked)
                is_confirmation = "1"

            } else {
                binding.imgisparent.setImageResource(R.drawable.ic_rec_unchecked)
                is_confirmation = "0"
            }


        }

        dialog?.show()
        return binding.root
    }

    private fun  callchoresdetailapi() {
        viewModel.getchoredetails(
            args.selectedchore,args.notificationId
        ).observe(this) { resources ->
            when (resources.status) {
                Status.SUCCESS -> {
                    loadingIndicator.dismiss()
                    resources.data.let { response ->
                        if (response != null) {
                            if (response.status) {
                                binding.llisDaily.isEnabled=false

                                binding.etchoretitle.setText(response.data.title)
                                binding.txtpoints.setText(response.data.point.toString())

                                //2021-08-13 06:50:00


                                if(args.fromPopular == ""){
                                    if (response.data.icon_name == "other-icon.png") {

                                        binding.chkpopular.setImageResource(R.drawable.ic_rec_unchecked)
                                        binding.chkcustom.visibility = View.GONE
                                        binding.chkcustomselected.visibility = View.VISIBLE
                                        binding.llCustomerChore.isEnabled = false

                                    } else {

                                        binding.chkpopular.setImageResource(R.drawable.ic_rec_checked)
                                        binding.chkcustom.visibility = View.VISIBLE
                                        binding.chkcustomselected.visibility = View.GONE
                                        binding.llCustomerChore.isEnabled = true
                                    }
                                }else{
                                    binding.chkpopular.setImageResource(R.drawable.ic_rec_checked)
                                    binding.chkcustom.visibility = View.VISIBLE
                                    binding.chkcustomselected.visibility = View.GONE
                                    binding.llCustomerChore.isEnabled = true
                                }







                                if (args.childToken != GlobalClass.prefs.getValueString("token").toString()
                                    && GlobalClass.prefs.getValueString("isadmin") == "0"
                                ) {

                                    binding.etchoretitle.isEnabled = false
                                    binding.chkcustom.isEnabled = false
                                    binding.chkpopular.isEnabled = false
                                    binding.lldatepicker.isEnabled = false
                                    binding.llisApproval.isEnabled = false
                                    binding.llisCompleted.isEnabled = false
                                    binding.llPopularChore.isEnabled = false
                                    binding.llCustomerChore.isEnabled = false
                                    binding.txtsave.isEnabled = false
                                    binding.txttimeframe.isEnabled = false
                                    binding.pointsll.isEnabled = false

                                } else if (args.childToken == GlobalClass.prefs.getValueString("token")
                                        .toString()
                                ) {

                                    binding.etchoretitle.isEnabled = false
                                    binding.chkcustom.isEnabled = false
                                    binding.chkpopular.isEnabled = false
                                    binding.lldatepicker.isEnabled = false
                                    binding.llisApproval.isEnabled = false
                                    binding.llPopularChore.isEnabled = false
                                    binding.llCustomerChore.isEnabled = false
                                }


                                if (response.data.is_confirmation == 0 && GlobalClass.prefs.getValueString(
                                        "isadmin"
                                    ) == "0"
                                ) {
                                    binding.llisCompleted.isEnabled = false
                                }
                                if (response.data.is_daily==1){
                                    binding.llisDaily.isEnabled=false
                                }


                                binding.txttimeframe.setText(getDateTime(response.data.set_time))
                                binding.txtpoints.text = response.data.point.toString()

                                if (args.populartitle == "") {
                                    choreicon = response.data.icon_name
                                } else {
                                    choreicon = args.selectedicon
                                    binding.etchoretitle.setText(args.populartitle)
                                }

                                if (response.data.is_confirmation == 1) {
                                    binding.imgmark.isEnabled = true

                                    binding.imgisparent.setImageResource(R.drawable.ic_rec_checked)
                                } else {
                                    binding.imgmark.isEnabled = false
                                }

                                if (response.data.is_daily == 1) {
                                    binding.imgchk.setImageResource(R.drawable.ic_rec_checked)
                                    is_daily_chores = response.data.is_daily.toString()

                                }

                                chore_point = response.data.point.toString()




                            }
                        }

                    }

                }

                Status.ERROR -> {
                    loadingIndicator.dismiss()

                }
                Status.LOADING -> {
                    loadingIndicator.show()
                }
            }

        }
    }

    override fun onResume() {
        super.onResume()

    }

    private fun callchildapi() {

        var msg: String? = null

        var finaldate = finalDateTime(binding.txttimeframe.text.toString())


        val today = Date()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateToStr = format.format(today)

        Log.e("date", dateToStr)
        var chore_time = ""


        if (binding.txttimeframe.text.toString() != "") {
            chore_time = finalDateTime(binding.txttimeframe.text.toString()).toString()
        } else {
            chore_time = ""
        }
        Log.d("datefinal", chore_time)
        if (choreicon == "") {
            choreicon = "other-icon.png"
        }

        viewModelupdate.updatechores(
            GlobalClass.prefs.getValueString("token").toString(),
            chore_id!!,
            choreicon,
            chore_title,
            is_daily_chores,
            is_complete,
            is_confirmation,
            binding.txtpoints.text.toString(),
            chore_time, dateToStr
        ).observe(this) { resources ->
            when (resources.status) {
                Status.SUCCESS -> {
                    loadingIndicator.dismiss()
                    resources.data.let { response ->
                        msg = response?.message


                        if (response != null) {

                            if (response.status) {

                                GlobalClass.chore_type = "0"
                                setFragmentResult(
                                    "refresh",
                                    bundleOf("fromUpdateChore" to true)
                                )
                                Toast.makeText(
                                    requireContext(),
                                    response.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                                findNavController().navigateUp()
                            }
                        }

                    }

                }

                Status.ERROR -> {
                    loadingIndicator.dismiss()
                }
                Status.LOADING -> {
                    loadingIndicator.show()

                }


            }
        }
    }

    private fun datepickerdialog() {

        val calendar = Calendar.getInstance()
        year = calendar[Calendar.YEAR]
        month = calendar[Calendar.MONTH]
        day = calendar[Calendar.DAY_OF_MONTH]
        val datePickerDialog =
            DatePickerDialog(requireActivity(), R.style.DialogTheme, this, year, month, day)
        datePickerDialog.datePicker.minDate = calendar.timeInMillis
        datePickerDialog.show()
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )
        )
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )
        )
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000


    }

    private fun showcustomdialog() {

        val viewGroup: ViewGroup? = requireView().findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(requireContext())
            .inflate(R.layout.bottomsheet_layout, viewGroup, false)
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())


        //setting the view of the builder to our custom view that we already inflated

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it

        //finally creating the alert dialog and displaying it
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()

        editTextpoints = dialogView.findViewById<TextView>(R.id.etpoints)
        // editText.filters = arrayOf<InputFilter>(MinMaxFilter("1", "500"))
        editTextpoints!!.setText(binding.txtpoints.text.toString())

        dialogView.imgback.setOnClickListener() {
            alertDialog.dismiss()
        }
        val pointslist: MutableList<String> = ArrayList()
        for (i in min..max) {
            pointslist.add(i.toString())
            // Log.d("xxx",pointslist.size.toString())
        }

        dialogView.etpoints.setOnClickListener() {

            bottomSheetDialog = BottomSheetDialog(requireContext())
            val btnsheet = layoutInflater.inflate(R.layout.bottomsheet, null)


            val rec = btnsheet.findViewById<RecyclerView>(R.id.rvpoints)

            rec.layoutManager = LinearLayoutManager(requireContext())
            val adapter = PointsAdapter(requireContext(), pointslist, "fromupdate")
            rec.adapter = adapter

            bottomSheetDialog!!.setContentView(btnsheet)

//            val bottomSheetBehavior = BottomSheetBehavior.from(btnsheet.getParent() as View)
//            bottomSheetBehavior.peekHeight = 500

            bottomSheetDialog!!.show()
        }
        dialogView.btnsave.setOnClickListener() {
            txtpoints.text = editTextpoints!!.text.toString()
            alertDialog.dismiss()
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        GlobalClass.selectedChildId.clear()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        myYear = year
        myday = dayOfMonth
        myMonth = month + 1
        val c = Calendar.getInstance()
        hour = c[Calendar.HOUR_OF_DAY]
        minute = c[Calendar.MINUTE]
        mySecond = c[Calendar.SECOND]

        selectedDate = LocalDate.of(myYear, myMonth, myday)
        currentDate =
            LocalDate.of(c[Calendar.YEAR], c[Calendar.MONTH] + 1, c[Calendar.DAY_OF_MONTH])

        Log.e("selecteddate", selectedDate.toString() + "=======" + currentDate.toString())

        val timePickerDialog = TimePickerDialog(
            requireContext(), R.style.DialogTheme,
            this,
            hour,
            minute, false
        )


        timePickerDialog.show()
        timePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )
        )
        timePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(
            resources.getColor(
                R.color.colorPrimary
            )

        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {


        if (selectedDate!!.isAfter(currentDate)) {



            myHour = hourOfDay
            myMinute = minute



            txttimeframe.setText(parseDateTime("$myYear-$myMonth-$myday" + " " + "$myHour:$myMinute"))
            time = "$myYear-$myMonth-$myday " +
                    "$myHour:$myMinute"
        } else {
            val c = Calendar.getInstance()



            if ((hourOfDay <= (c.get(Calendar.HOUR_OF_DAY))) &&
                (minute <= (c.get(Calendar.MINUTE)))
            ) {
                Toast.makeText(requireContext(), "You can't pick past time", Toast.LENGTH_SHORT)
                    .show()
                binding.txtsave.isEnabled=false

            } else {
                binding.txtsave.isEnabled=true
                myHour = hourOfDay
                myMinute = minute

                txttimeframe.setText(parseDateTime("$myYear-$myMonth-$myday" + " " + "$myHour:$myMinute"))
                time = "$myYear-$myMonth-$myday " +
                        "$myHour:$myMinute"

            }

        }


        /* val c = Calendar.getInstance()

         if((hourOfDay <= (c.get(Calendar.HOUR_OF_DAY)))&&
                 (minute <= (c.get(Calendar.MINUTE)))){
             Toast.makeText(requireContext(), "You can't pick past time", Toast.LENGTH_SHORT).show()

         }else{
             myHour = hourOfDay
             myMinute = minute
             txttimeframe.setText(parseDateTime("$myYear-$myMonth-$myday" + " " + "$myHour:$myMinute"))
             binding.imgtimechk.setImageResource(R.drawable.ic_rec_checked)
             // time="$myYear$myMonth$myday$myHour$myMinute$mySecond"
         }
 */
    }

    fun parseDateTime(inputDateTime: String?): String? {
        val inputPattern = "yyyy-MM-dd HH:mm"
        val outputPattern = "MMM-dd-yyyy hh:mm a"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    fun getDateTime(inputDateTime: String?): String? {
        val inputPattern = "yyyy-MM-dd HH:mm:ss"
        val outputPattern = "MMM-dd-yyyy hh:mm a"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    fun finalDateTime(inputDateTime: String?): String? {
        val inputPattern = "MMM-dd-yyyy hh:mm a"
        val outputPattern = "yyyy-MM-dd HH:mm:ss"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(inputDateTime)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getPopularChoreList() {

        if (isNetworkConnected(requireContext())) {
            viewModel2.getPopularChoreList(
                GlobalClass.prefs.getValueString("token").toString()
            )
                .observe(
                    viewLifecycleOwner
                ) { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            loadingIndicator.dismiss()
                            resource.data?.let { response ->
                                if (response.status) {


                                    max=response.maximumPoint
                                    min=response.minimumPoint
                                    binding.minimumpts.text=min.toString()
                                    binding.maximumpts.text=max.toString()

                                }


                            }
                        }

                        Status.ERROR -> loadingIndicator.dismiss()

                        Status.LOADING -> loadingIndicator.show()

                    }

                }

        }

    }




}
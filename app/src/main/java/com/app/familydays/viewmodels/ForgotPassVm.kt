package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class ForgotPassVm(private val mainRepository: MainRepository) : ViewModel() {

    fun forgotPassword(email: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.forgotPassword(email)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }


}
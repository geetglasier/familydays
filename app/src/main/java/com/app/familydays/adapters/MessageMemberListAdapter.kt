package com.app.familydays.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.ItemFamilyMemberBinding
import com.app.familydays.databinding.ItemMessageMemberlistBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.FamilyMemberListResp
import com.app.familydays.webservice.responses.MessageMemberlistResp
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class MessageMemberListAdapter(
    val onItemClicked: (MessageMemberlistResp.Data) -> Unit
) :
    RecyclerView.Adapter<MessageMemberListAdapter.BlockUserViewHolder>() {
    var familyMemberList: List<MessageMemberlistResp.Data>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class BlockUserViewHolder(val binding: ItemMessageMemberlistBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.member = familyMemberList?.get(position)
            binding.root.setOnClickListener {
                onItemClicked(binding.member!!)
            }

            if (familyMemberList!![position].role == 2) {
                GlobalClass.prefs.SaveString("roletypeF", "Father")
                binding.tvpoints.visibility=View.GONE

            }
            if (familyMemberList!![position].role == 3) {
                GlobalClass.prefs.SaveString("roletypeM", "Mother")
                binding.tvpoints.visibility=View.GONE
            }

            if (familyMemberList!![position].message_count>0){
               binding.imgchat.setImageResource(R.drawable.ic_msg_unread)
                binding.txtcount.visibility=View.VISIBLE
                binding.txtcount.text=familyMemberList!![position].message_count.toString()
            }else{
                binding.txtcount.visibility=View.GONE
            }




            Glide.with(itemView)
                .load(familyMemberList!![position]!!.profile_url)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(binding.imgProfile)


            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        BlockUserViewHolder(
            ItemMessageMemberlistBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )

    override fun onBindViewHolder(
        holder: MessageMemberListAdapter.BlockUserViewHolder,
        position: Int
    ) {
        holder.bind(position)


    }

    override fun getItemCount() = familyMemberList?.size ?: 0
}
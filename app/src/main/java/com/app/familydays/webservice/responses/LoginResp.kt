package com.app.familydays.webservice.responses


import com.squareup.moshi.Json

data class LoginResp(
    @Json(name = "data")
    val `data`: Data?,
    @Json(name = "error")
    val error: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "status")
    val status: Boolean
) {
    data class Data(
        @Json(name = "email")
        val email: String,
        @Json(name = "full_name")
        val fullName: String,
        @Json(name = "is_admin")
        val isAdmin: Int,
        @Json(name = "is_reset")
        val isReset: Int,
        @Json(name = "profile_url")
        val profileUrl: String,
        @Json(name = "role")
        val role: Int,
        @Json(name = "token")
        val token: String,
        @Json(name = "user_id")
        val userId: Int,
        @Json(name = "username")
        val username: String
    )
}
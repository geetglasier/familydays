package com.app.familydays.webservice

import com.app.familydays.viewmodels.*
import com.app.familydays.webservice.responses.GetNotificationListResp
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit


//const val BASE_URL = "https://glasier.in/"
const val BASE_URL = "https://choreup.org/"
val mainModule = module {

    single { MainRepository(get()) }

    single { createWebService() }

    viewModel { SignUpVm(get()) }
    viewModel { LoginVm(get()) }
    viewModel { ForgotPassVm(get()) }
    viewModel { ContactAdminVm(get()) }
    viewModel { EditProfileVm(get()) }
    viewModel { FamilyMemberVm(get()) }
    viewModel { AddFamilyMemberVm(get()) }
    viewModel { ChildMemberDetailVm(get()) }
    viewModel { AdminFinishedChoreListVm(get()) }
    viewModel { AdminAssignedChoreListVm(get()) }
    viewModel { PopularChoreListVm(get()) }
    viewModel { Family_Child_ListVm(get()) }
    viewModel { Addchoresvm(get()) }
    viewModel { Addchoreschildvm(get()) }
    viewModel { UpdateChoresVM(get()) }
    viewModel { GetChoresDetailVM(get()) }
    viewModel { ChoreisCompleteVM(get()) }
    viewModel { DeleteChoreVM(get()) }
    viewModel { GetMoreChoresVM(get()) }
    viewModel { GetRewardBrandVM(get()) }
    viewModel { AddRewardVM(get()) }
    viewModel { RewardlistVM(get()) }
    viewModel { ConfirmRewardVM(get()) }
    viewModel { ClaimRewardVM(get()) }
    viewModel { GetRewardDetailsVM(get()) }
    viewModel {UpdateRewardVM(get()) }
    viewModel {FilterVM(get()) }
    viewModel {ClaimListVM(get()) }
    viewModel {MessagemeberListVM(get()) }
    viewModel {SendMessageVM(get()) }
    viewModel {GetCountVM(get()) }
    viewModel {ApproveChoresVM(get()) }
    viewModel {NotificationListVM(get()) }
    viewModel {ChangePasswordVM(get()) }









}

fun createWebService(): ApiService {
    val retrofit = Retrofit.Builder()
        .addConverterFactory(
            MoshiConverterFactory.create(
                Moshi.Builder()
                    .add(KotlinJsonAdapterFactory())
                    .build()
            )
        )
        .client(
            OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) })
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .build()
        )
        .baseUrl(BASE_URL)
        .build()

    return retrofit.create(ApiService::class.java)
}
package com.app.familydays.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.app.familydays.R
import com.app.familydays.databinding.ItemFinishedChoreBinding
import com.app.familydays.databinding.ItemPopularChoreBinding
import com.app.familydays.global.GlobalClass
import com.app.familydays.webservice.responses.GetFinishedChoreResp
import com.app.familydays.webservice.responses.PopularChoreResp

class PopularChoreAdaper(
    val context: Context,
    val onItemClicked: (PopularChoreResp.Data) -> Unit
) :
    RecyclerView.Adapter<PopularChoreAdaper.PopularChoreViewHolder>() {
    var popularChoreRespList: List<PopularChoreResp.Data>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class PopularChoreViewHolder(val binding: ItemPopularChoreBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.popularChore = popularChoreRespList?.get(position)

            if (GlobalClass.selectepos == position){
                binding.ivSelected.visibility=View.VISIBLE
            }else{
                binding.ivSelected.visibility=View.INVISIBLE
            }


            binding.root.setOnClickListener {
                GlobalClass.selectepos=position
                onItemClicked(popularChoreRespList?.get(position)!!)
            }



            binding.executePendingBindings()

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PopularChoreViewHolder(
            ItemPopularChoreBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
        )


    override fun onBindViewHolder(holder: PopularChoreViewHolder, position: Int) {
        holder.bind(position)

    }

    override fun getItemCount() = popularChoreRespList?.size ?: 0

}
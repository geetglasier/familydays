package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

data class GeneralResp(
    @Json(name = "error")
    val error: Int = 0,
    @Json(name = "message")
    val message: String = "",
    @Json(name = "status")
    val status: Boolean = false
)
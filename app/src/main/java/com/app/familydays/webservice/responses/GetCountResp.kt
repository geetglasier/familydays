package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

data class GetCountResp(
    @Json(name = "status") val status : Boolean,
    @Json(name = "error") val error : Int,
    @Json(name = "message") val message : String,
    @Json(name = "notification_count") val notification_count : Int,
    @Json(name = "message_count") val message_count : Int
){

}
package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class FilterVM (private val mainRepository: MainRepository
) : ViewModel() {
    fun getfinishedfilterlist(token: String,load_more:String,from_date: String,to_date: String,child_id:String) = liveData(
        Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getFinishedchoreFilter(token,load_more,from_date,to_date,child_id)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

    fun getassignedfilterlist(token: String,load_more:String,from_date: String,to_date: String,child_id:String) = liveData(
        Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getassignedchoreFilter(token,load_more,from_date,to_date,child_id)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

    fun getrewardfilterlist(token: String,from_date: String,to_date: String,child_id:String) = liveData(
        Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getrewardFilter(token,from_date,to_date,child_id)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

    fun getckaimfilterlist(token: String,from_date: String,to_date: String,child_id:String) = liveData(
        Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getclaimFilter(token,from_date,to_date,child_id)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }


}
package com.app.familydays.webservice.responses

import com.squareup.moshi.Json

class RewardDetailsResp(

    @Json(name ="status")
    val status : Boolean,
    @Json(name ="error")
    val error : Int,
    @Json(name ="message")
    val message : String,
    @Json(name ="data")
    val data : Data
) {
    data class Data (

        @Json(name="reward_id") val reward_id : Int,
        @Json(name="category") val category : String,
        @Json(name="category_icon_name") val category_icon_name : String,
        @Json(name="category_icon") val category_icon :String,
        @Json(name="brand_name") val brand_name : String,
        @Json(name="brand_icon_name") val brand_icon_name : String,
        @Json(name="brand_icon") val brand_icon: String,
        @Json(name="reward_name") val reward_name : String,
        @Json(name="point") val point: Int,
        @Json(name="is_conform") val is_conform : Int,
        @Json(name="due_date") val due_date : String,
        @Json(name="token") val token : String,
        @Json(name="create_by") val create_by : String,
        @Json(name="is_createby") val is_createby : String,
        @Json(name="minimum_point") val minimum_point : Int,
        @Json(name="maximum_point") val maximum_point : Int,
    )

}
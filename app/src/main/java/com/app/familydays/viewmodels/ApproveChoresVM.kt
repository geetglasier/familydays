package com.app.familydays.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import kotlinx.coroutines.Dispatchers

class ApproveChoresVM(private val mainRepository: MainRepository): ViewModel() {
    fun chore_approve(token:String,chore_id:String)=
        liveData(Dispatchers.IO) {
            emit(Resource.loading(data = null))
            try {
                emit(Resource.success(data = mainRepository.chores_approve(token,chore_id)))
            } catch (exception: Exception) {
                exception.printStackTrace()
                emit(Resource.error(data = null, exception = exception))
            }



        }

}
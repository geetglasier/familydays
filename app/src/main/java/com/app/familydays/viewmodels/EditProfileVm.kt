package com.app.familydays.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.app.familydays.utils.Resource
import com.app.familydays.webservice.MainRepository
import com.app.familydays.webservice.models.FamilyDaysUser
import kotlinx.coroutines.Dispatchers
import okhttp3.MultipartBody
import okhttp3.RequestBody

class EditProfileVm (
    private val mainRepository: MainRepository
) : ViewModel() {
    var userLive = MutableLiveData(FamilyDaysUser())

    fun getUserDetail(token: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getUserDetail(token)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

    fun setUserDetail(userDetail: FamilyDaysUser) {
        userLive.value = userDetail
    }

    fun submitProfilePic(token: String, filePart: MultipartBody.Part?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.updateProfilePic(token, filePart)))
//            mainRepository.updateProfilePic(token, filePart)
//            emit(Resource.success(data = mainRepository.getUserDetail(token)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }

    fun updateUserDetail(token: String,name:String,email:String,phone_no:String,role:String,birth_date: String,password:String,is_admin:String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
//            emit(Resource.success(data = mainRepository.submitProfilePic(token, filePart)))
            mainRepository.updateUserDetail(token,name,email,phone_no,role,birth_date,password,is_admin)
            emit(Resource.success(data = mainRepository.updateUserDetail(token,name,email,phone_no,role,birth_date,password,is_admin)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }
    fun updateChildDetail(token: String, username: RequestBody, child_id: RequestBody, fullname: RequestBody, email: RequestBody, password: RequestBody, birth_date: RequestBody, role: RequestBody, is_admin: RequestBody, phone_no: RequestBody, profilefile:MultipartBody.Part?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
//            emit(Resource.success(data = mainRepository.submitProfilePic(token, filePart)))
            mainRepository.updatechildDetail(token,username,child_id,fullname,email,password,birth_date,role,is_admin,phone_no,profilefile)
            emit(Resource.success(data = mainRepository.updatechildDetail(token,username,child_id,fullname,email,password,birth_date,role,is_admin,phone_no,profilefile)))
        } catch (exception: Exception) {
            exception.printStackTrace()
            emit(Resource.error(data = null, exception = exception))
        }
    }




}
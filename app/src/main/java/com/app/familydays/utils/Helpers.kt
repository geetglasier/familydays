package com.app.familydays.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences

import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.BindingAdapter
import coil.load
import coil.transform.CircleCropTransformation
import com.app.familydays.R
import com.app.familydays.activities.LoginActivity
import com.app.familydays.activities.MainActivity
import com.app.familydays.global.GlobalClass
import com.kaopiz.kprogresshud.KProgressHUD

class ApiException(message: String) : Exception(message)

fun getLoadingIndicator(context: Context): KProgressHUD {
    return KProgressHUD.create(context)
        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
        .setLabel(context.getString(R.string.txt_loading))
        .setCancellable(false)
        .setAnimationSpeed(2)
        .setDimAmount(0.5f)
}



@BindingAdapter("circleImageUrl")
fun loadCircularImage(view: ImageView, url: String?) {
    url?.let {
        view.load(it) {
            crossfade(true)
            placeholder(R.drawable.ic_placeholder)
            error(R.drawable.ic_placeholder)
            transformations(CircleCropTransformation())
        }
    }
}

@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, url: String?) {
    url?.let {
        view.load(it) {
            crossfade(true)
            placeholder(R.drawable.ic_placeholder)
            error(R.drawable.ic_placeholder)
        }
    }
}

fun confirmationDialog(context: Context, message: String, onYes: () -> Unit) {
    androidx.appcompat.app.AlertDialog.Builder(context)
        .setMessage(message).setCancelable(false)
        .setPositiveButton(context.getString(R.string.txt_yes)) { _: DialogInterface, _: Int ->
            onYes()
        }.setNegativeButton(context.getString(R.string.txt_no), null).show()
}

fun logoutUser(activity: Activity?, fromLogout: Boolean = true) {



    GlobalClass.prefs.removeValue("token")
   GlobalClass.prefs.removeValue("id")
   GlobalClass.prefs.removeValue("username")
    GlobalClass.prefs.removeValue("profilePic")
    GlobalClass.prefs.removeValue("role")

    GlobalClass.prefs.clearSharedPreference()

//    sp.clear().commit()
    if (!fromLogout) Toast.makeText(activity, activity!!.getString(R.string.txt_user_token_expired), Toast.LENGTH_LONG).show()
    activity?.startActivity(
        Intent(activity, MainActivity::class.java).addFlags(
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        )
    )
    activity?.finish()
}

fun showMediaPickerDialog(
    context: Context,
    onCamera: () -> Unit,
    onGallery: () -> Unit,
    isImage: Boolean = true
) {
    val options = arrayOf<CharSequence>(
        context.getString(R.string.txt_from_camera),
        context.getString(R.string.txt_choose_from_gallery),
        context.getString(R.string.txt_cancel)
    )


    val alertDialogbuilder: AlertDialog.Builder = AlertDialog.Builder(context)
    alertDialogbuilder.setTitle(context.getString(R.string.txt_add_photo))

    alertDialogbuilder.setItems(options) { dialogInterface, i ->
        when (i) {
            0 -> onCamera()
            1 -> onGallery()
            else -> dialogInterface.dismiss()
        }


    }
    val alertDialog: AlertDialog = alertDialogbuilder.create()
    // Set other dialog properties
    alertDialog.setCancelable(false)
    alertDialog.show()

}

fun unAuthorisedUserDialog(
    activity: Activity?,
    message: String
) {
    androidx.appcompat.app.AlertDialog.Builder(activity!!)
        .setMessage(activity.getString(R.string.txt_unauthorised_msg)).setCancelable(false)
        .setPositiveButton("Ok") { _: DialogInterface, _: Int ->

            GlobalClass.prefs.removeValue("token")
            GlobalClass.prefs.removeValue("id")
            GlobalClass.prefs.removeValue("username")
            GlobalClass.prefs.removeValue("profilePic")
            GlobalClass.prefs.removeValue("roletype")


            activity?.startActivity(
                Intent(activity, MainActivity::class.java).addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                )
            )
            activity?.finish()

        }.setNegativeButton("", null).show()
}


@RequiresApi(Build.VERSION_CODES.M)
fun isNetworkConnected(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = connectivityManager.activeNetwork
    val networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)

    return if (networkCapabilities != null &&
        networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
    )
        true
    else {
        Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_LONG)
            .show()
        false
    }


}



